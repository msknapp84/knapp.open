package knapp.open.iterators.util;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import static knapp.common.util.ByteUtils.*;
import static org.junit.Assert.*;
import static knapp.open.iterators.util.AccIterUtil.*;

public class AccIterUtilTest {
	
	@Test
	public void testRowToValues() throws IOException {
		MockSortedKeyValueIterator mock = new MockSortedKeyValueIterator();
		mock.add("a","f","q",intToBytes(3));
		mock.add("a","f","x",intToBytes(2));
		mock.add("a","f","z",intToBytes(9));
		mock.add("a","c","q",intToBytes(36));
		mock.add("a","f","k",intToBytes(-13));
		mock.done();
		
		List<String> required = Arrays.asList("f.q","c.q");
		
		Map<String,byte[]> vals = rowToValues(required, mock);
		
		assertEquals(2,vals.size());
		assertEquals(3,bytesToInt(vals.get("f.q")));
		assertEquals(36,bytesToInt(vals.get("c.q")));
	}
	
	@Test
	public void testAreSame() {
		assertTrue(areSame("false",boolToBytes(false),"boolean"));
		assertTrue(areSame("true",boolToBytes(true),"boolean"));
		assertFalse(areSame("false",boolToBytes(true),"boolean"));
		assertFalse(areSame("true",boolToBytes(false),"boolean"));
		assertTrue(areSame("13",shortToBytes((short)13),"short"));
		assertTrue(areSame("84",intToBytes(84),"int"));
		assertTrue(areSame("84",longToBytes(84),"long"));
		assertFalse(areSame("83",longToBytes(84),"long"));
		assertFalse(areSame("84",longToBytes(84),"string"));
		assertTrue(areSame("13.245",floatToBytes((float)13.245),"float"));
		assertTrue(areSame("952.31534",doubleToBytes(952.31534),"double"));
		String f = "2013-10-25T04:31:18";
		LocalDateTime d = LocalDateTime.from(DateTimeFormatter.ISO_DATE_TIME.parse(f));
		assertTrue(areSame(f,dateTimeToBytes(d),"datetime"));
	}
}
