package knapp.open.iterators;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import knapp.common.util.ByteUtils;
import knapp.open.iterators.util.MockSortedKeyValueIterator;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.junit.Assert;
import org.junit.Test;

public class NthRowFilterTest {

	@Test
	public void withZero() throws IOException {
		MockSortedKeyValueIterator source = new MockSortedKeyValueIterator();
		source.done();
		NthRowFilter counter = new NthRowFilter();
		counter.init(source, Collections.EMPTY_MAP, null);
		counter.seek(new Range(), Collections.EMPTY_LIST, true);
		
		Assert.assertFalse(counter.hasTop());
	}

	@Test
	public void withZero_askingForTwo() throws IOException {
		MockSortedKeyValueIterator source = new MockSortedKeyValueIterator();
		source.done();
		NthRowFilter counter = new NthRowFilter();
		Map<String,String> options = new HashMap<>();
		options.put(NthRowFilter.DESIRED_ROW_PROPERTY,"1");
		counter.init(source, options, null);
		counter.seek(new Range(), Collections.EMPTY_LIST, true);
		
		Assert.assertFalse(counter.hasTop());
	}
	
	@Test
	public void withOne() throws IOException {
		MockSortedKeyValueIterator source = new MockSortedKeyValueIterator();
		source.add("a", "f", "q", "europe".getBytes());
		source.done();
		NthRowFilter counter = new NthRowFilter();
		counter.init(source, Collections.EMPTY_MAP, null);
		counter.seek(new Range(), Collections.EMPTY_LIST, true);
		
		Assert.assertTrue(counter.hasTop());
		Key key = counter.getTopKey();
		Value value = counter.getTopValue();
		Assert.assertEquals("a",key.getRow().toString());
		Assert.assertEquals("f",key.getColumnFamily().toString());
		Assert.assertEquals("q",key.getColumnQualifier().toString());
		Assert.assertEquals("europe",new String(value.get()));
		counter.next();
		Assert.assertFalse(counter.hasTop());
	}

	@Test
	public void withSome() throws IOException {
		MockSortedKeyValueIterator source = new MockSortedKeyValueIterator();
		source.add("a", "f", "q", "europe".getBytes());
		source.add("a", "f", "r", "locket".getBytes());
		source.add("b", "f", "t", "bloke".getBytes());
		source.add("c", "f", "x", "fan".getBytes());
		source.done();
		NthRowFilter counter = new NthRowFilter();
		counter.init(source, Collections.EMPTY_MAP, null);
		counter.seek(new Range(), Collections.EMPTY_LIST, true);
		
		Assert.assertTrue(counter.hasTop());
		Key key = counter.getTopKey();
		Value value = counter.getTopValue();
		Assert.assertEquals("a",key.getRow().toString());
		Assert.assertEquals("f",key.getColumnFamily().toString());
		Assert.assertEquals("q",key.getColumnQualifier().toString());
		Assert.assertEquals("europe",new String(value.get()));
		counter.next();
		Assert.assertTrue(counter.hasTop());
		key = counter.getTopKey();
		value = counter.getTopValue();
		Assert.assertEquals("a",key.getRow().toString());
		Assert.assertEquals("f",key.getColumnFamily().toString());
		Assert.assertEquals("r",key.getColumnQualifier().toString());
		Assert.assertEquals("locket",new String(value.get()));
		counter.next();
		Assert.assertFalse(counter.hasTop());
	}

	@Test
	public void chooseSecondRow() throws IOException {
		MockSortedKeyValueIterator source = new MockSortedKeyValueIterator();
		source.add("a", "f", "q", "europe".getBytes());
		source.add("a", "f", "r", "locket".getBytes());
		source.add("b", "f", "t", "bloke".getBytes());
		source.add("c", "f", "x", "fan".getBytes());
		source.done();
		NthRowFilter counter = new NthRowFilter();
		Map<String,String> options = new HashMap<>();
		options.put(NthRowFilter.DESIRED_ROW_PROPERTY,"1");
		counter.init(source, options, null);
		counter.seek(new Range(), Collections.EMPTY_LIST, true);
		
		Assert.assertTrue(counter.hasTop());
		Key key = counter.getTopKey();
		Value value = counter.getTopValue();
		Assert.assertEquals("b",key.getRow().toString());
		Assert.assertEquals("f",key.getColumnFamily().toString());
		Assert.assertEquals("t",key.getColumnQualifier().toString());
		Assert.assertEquals("bloke",new String(value.get()));
		counter.next();
		Assert.assertFalse(counter.hasTop());
	}
}