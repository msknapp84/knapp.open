package knapp.open.iterators.util;

import java.util.List;

import org.junit.Test;

import static org.junit.Assert.*;

public class IsQuotedTest {
	
	@Test
	public void testSplit() {
		String original = "foo, \"bar, zop\", googoo; 'rag'";
		List<IsQuoted> result = IsQuoted.splitQuoted(original);
		assertEquals(4,result.size());
		assertEquals("foo, ",result.get(0).getValue());
		assertEquals("bar, zop",result.get(1).getValue());
		assertEquals(", googoo; ",result.get(2).getValue());
		assertEquals("rag",result.get(3).getValue());
		
		assertEquals(", googoo; ",result.get(1).getRight().getValue());
		assertEquals("foo, ",result.get(1).getLeft().getValue());
		assertNull(result.get(0).getLeft());
		assertNull(result.get(3).getRight());
		assertEquals(6,result.get(1).getStart());
		assertEquals(14,result.get(1).getStop());
		
		assertEquals(original,IsQuoted.reconstruct(result));
		
		assertTrue(IsQuoted.splitQuoted(null).isEmpty());
	}

}
