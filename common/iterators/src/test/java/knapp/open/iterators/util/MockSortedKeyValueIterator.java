package knapp.open.iterators.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.accumulo.core.data.ByteSequence;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;

public class MockSortedKeyValueIterator implements
		SortedKeyValueIterator<Key, Value> {

	private SortedMap<Key, Value> realSource = new TreeMap<>();
	private boolean done = false;
	private List<Key> sorted;

	private int index = 0;
	private int maxIndex = 0;

	public MockSortedKeyValueIterator add(String rowId, String cf, String cq,
			String value) {
		return add(rowId, cf, cq, value.getBytes());
	}

	public MockSortedKeyValueIterator add(String rowId, String cf, String cq,
			byte[] value) {
		if (done) {
			throw new IllegalStateException(
					"You already marked this mock as done, you can't add to it now.");
		}
		Key key = new Key(rowId, cf, cq);
		Value val = new Value(value);
		realSource.put(key, val);
		return this;
	}

	public void done() {
		if (done) {
			throw new IllegalStateException(
					"You already marked this mock as done, you can't mark it done again.");
		}
		done = true;
		sorted = new ArrayList<>(realSource.keySet());
		Collections.sort(sorted);
		maxIndex = sorted.size();
	}

	@Override
	public void init(SortedKeyValueIterator<Key, Value> source,
			Map<String, String> options, IteratorEnvironment env)
			throws IOException {

	}

	@Override
	public boolean hasTop() {
		if (!done) {
			throw new IllegalStateException(
					"You must mark this mock as 'done' before you can use it.");
		}
		if (index >= maxIndex) {
			return false;
		}
		return sorted.get(index)!=null;
	}

	@Override
	public void next() throws IOException {
		if (!done) {
			throw new IllegalStateException(
					"You must mark this mock as 'done' before you can use it.");
		}
		index++;
	}

	@Override
	public void seek(Range range, Collection<ByteSequence> columnFamilies,
			boolean inclusive) throws IOException {
		if (!done) {
			throw new IllegalStateException(
					"You must mark this mock as 'done' before you can use it.");
		}
		// going to allow seeking multiple times, idk if accumulo would allow
		// it.
		if (sorted == null || sorted.isEmpty()) {
			return;
		}
		if (range.getStartKey()!=null) {
			Key rangeStartKey = range.getStartKey();
			for (int i = 0;i<sorted.size();i++) {
				Key currentKey = sorted.get(i);
				int diff = rangeStartKey.compareTo(currentKey);
				boolean sameKey = diff==0;
				if (sameKey && range.isStartKeyInclusive()) {
					index = i;
					break;
				} else if (diff<0) {
					index = i;
					break;
				}
			}
		} else {
			index = 0;
		}
		maxIndex = sorted.size();
		if (range.getEndKey()!=null) {
			for (int i = 0;i<sorted.size();i++) {
				if (range.getEndKey().compareTo(sorted.get(i))==0 && !range.isEndKeyInclusive()) {
					maxIndex = i;
					break;
				} else if (range.getEndKey().compareTo(sorted.get(i))<0) {
					maxIndex = i;
					break;
				}
			}
		}
		
	}

	@Override
	public Key getTopKey() {
		if (!done) {
			throw new IllegalStateException(
					"You must mark this mock as 'done' before you can use it.");
		}
		if (index>=maxIndex) {
			return null;
		}
		return sorted.get(index);
	}

	@Override
	public Value getTopValue() {
		if (!done) {
			throw new IllegalStateException(
					"You must mark this mock as 'done' before you can use it.");
		}
		Key topKey = getTopKey();
		if (topKey==null) {
			return null;
		}
		return realSource.get(topKey);
	}

	@Override
	public SortedKeyValueIterator<Key, Value> deepCopy(IteratorEnvironment env) {
		if (!done) {
			throw new IllegalStateException(
					"You must mark this mock as 'done' before you can use it.");
		}
		MockSortedKeyValueIterator copy = new MockSortedKeyValueIterator();
		copy.done = this.done;
		copy.index = this.index;
		copy.realSource = new TreeMap<>(this.realSource);
		copy.sorted = new ArrayList<>(this.sorted);
		copy.maxIndex = this.maxIndex;
		return copy;
	}
}
