package knapp.open.iterators;

import java.io.IOException;
import java.util.Collections;

import knapp.common.util.ByteUtils;
import knapp.open.iterators.util.MockSortedKeyValueIterator;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.junit.Assert;
import org.junit.Test;


public class CountIteratorTest {
	
	@Test
	public void countZero() throws IOException {
		MockSortedKeyValueIterator source =new MockSortedKeyValueIterator();
		source.done();
		CountIterator counter = new CountIterator();
		counter.init(source, Collections.EMPTY_MAP, null);
		counter.seek(new Range(), Collections.EMPTY_LIST, true);
		
		Assert.assertTrue(counter.hasTop());
		Key key = counter.getTopKey();
		Assert.assertNotNull(key);
		Assert.assertEquals("count",key.getRow().toString());
		Assert.assertEquals("f",key.getColumnFamily().toString());
		Assert.assertEquals("q",key.getColumnQualifier().toString());
		Value value = counter.getTopValue();
		Assert.assertNotNull(value);
		Assert.assertEquals(0,ByteUtils.bytesToInt(value.get()));
		counter.next();
		Assert.assertFalse(counter.hasTop());
	}

	@Test
	public void countOne() throws IOException {
		MockSortedKeyValueIterator source =new MockSortedKeyValueIterator();
		source.add("blah", "x", "y", "airplane".getBytes());
		source.done();
		CountIterator counter = new CountIterator();
		counter.init(source, Collections.EMPTY_MAP, null);
		counter.seek(new Range(), Collections.EMPTY_LIST, true);
		
		Assert.assertTrue(counter.hasTop());
		Key key = counter.getTopKey();
		Assert.assertNotNull(key);
		Assert.assertEquals("count",key.getRow().toString());
		Assert.assertEquals("f",key.getColumnFamily().toString());
		Assert.assertEquals("q",key.getColumnQualifier().toString());
		Value value = counter.getTopValue();
		Assert.assertNotNull(value);
		Assert.assertEquals(1,ByteUtils.bytesToInt(value.get()));
		counter.next();
		Assert.assertFalse(counter.hasTop());
	}
	@Test
	public void countSome() throws IOException {
		MockSortedKeyValueIterator source =new MockSortedKeyValueIterator();
		source.add("blah", "x", "y", "airplane".getBytes());
		source.add("blah", "x", "a", "airplane".getBytes());
		source.add("blah", "x", "c", "airplane".getBytes());
		source.add("blah", "x", "e", "airplane".getBytes());
		source.done();
		CountIterator counter = new CountIterator();
		counter.init(source, Collections.EMPTY_MAP, null);
		counter.seek(new Range(), Collections.EMPTY_LIST, true);
		
		Assert.assertTrue(counter.hasTop());
		Key key = counter.getTopKey();
		Assert.assertNotNull(key);
		Assert.assertEquals("count",key.getRow().toString());
		Assert.assertEquals("f",key.getColumnFamily().toString());
		Assert.assertEquals("q",key.getColumnQualifier().toString());
		Value value = counter.getTopValue();
		Assert.assertNotNull(value);
		Assert.assertEquals(4,ByteUtils.bytesToInt(value.get()));
		counter.next();
		Assert.assertFalse(counter.hasTop());
	}
}
