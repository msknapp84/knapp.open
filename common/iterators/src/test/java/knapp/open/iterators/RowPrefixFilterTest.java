package knapp.open.iterators;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import knapp.open.iterators.util.MockSortedKeyValueIterator;

import org.apache.accumulo.core.data.ByteSequence;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.util.MutableByteSequence;
import org.junit.Test;

import static org.junit.Assert.*;

public class RowPrefixFilterTest {
	
	@Test
	public void testPrefix() throws IOException {
		MockSortedKeyValueIterator mock = new MockSortedKeyValueIterator();
		// it's lexical.
		mock.add("a-7", "f", "x", "yawn".getBytes());
		mock.add("a-8", "f", "x", "boo".getBytes());
		mock.add("a-9", "f", "x", "blah".getBytes());
		mock.add("b-4", "f", "x", "zjfwe".getBytes());
		mock.add("b-9", "f", "x", "slir".getBytes());
		mock.add("c-9", "f", "x", "poue".getBytes());
		mock.add("d-4", "f", "x", "wqfa93".getBytes());
		mock.add("vfl-94", "f", "x", "ofj2".getBytes());
		mock.done();
		RowPrefixFilter filter = new RowPrefixFilter();
		Map<String,String> options = new HashMap<>();
		// let's use default args this time.
		filter.init(mock, options, null);
		filter.seek(new Range(), toColunmFamilies("f"), true);
		assertTrue(filter.hasTop());
		Key key = filter.getTopKey();
		Value value = filter.getTopValue();
		assertEquals("a-7",key.getRow().toString());
		assertEquals("yawn",new String(value.get()));
		filter.next();
		assertTrue(filter.hasTop());
		key = filter.getTopKey();
		value = filter.getTopValue();
		assertEquals("b-4",key.getRow().toString());
		assertEquals("zjfwe",new String(value.get()));
		filter.next();
		assertTrue(filter.hasTop());
		key = filter.getTopKey();
		value = filter.getTopValue();
		assertEquals("c-9",key.getRow().toString());
		assertEquals("poue",new String(value.get()));
		filter.next();
		assertTrue(filter.hasTop());
		key = filter.getTopKey();
		value = filter.getTopValue();
		assertEquals("d-4",key.getRow().toString());
		assertEquals("wqfa93",new String(value.get()));
		filter.next();
		assertTrue(filter.hasTop());
		key = filter.getTopKey();
		value = filter.getTopValue();
		assertEquals("vfl-94",key.getRow().toString());
		assertEquals("ofj2",new String(value.get()));
		filter.next();
		assertFalse(filter.hasTop());
		assertNull(filter.getTopKey());
		assertNull(filter.getTopValue());
	}
	
	@Test
	public void testEmpty() throws IOException {
		MockSortedKeyValueIterator source = new MockSortedKeyValueIterator();
		source.done();
		RowPrefixFilter filter = new RowPrefixFilter();
		filter.init(source, Collections.EMPTY_MAP, null);
		filter.seek(new Range(), toColunmFamilies("f"), true);
		assertFalse(filter.hasTop());
	}
	
	public static Collection<ByteSequence> toColunmFamilies(String... families) {
		Set<ByteSequence> res = new HashSet<>();
		for (String f : families) {
			res.add(new MutableByteSequence(f.getBytes(),0,f.length()));
		}
		return res;
	}

}
