package knapp.open.iterators;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import knapp.open.iterators.util.MockSortedKeyValueIterator;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.junit.Test;

import static org.junit.Assert.*;
import static knapp.common.util.ByteUtils.*;

public class RowValuesFilterTest {
	
	@Test
	public void testEmpty() throws IOException {
		MockSortedKeyValueIterator source = new MockSortedKeyValueIterator();
		source.done();
		RowValuesFilter filter = new RowValuesFilter();
		Map<String, String> options = new HashMap<>();
		options.put("req:f:x:int","3");
		filter.init(source, options, null);
		filter.seek(new Range(),RowPrefixFilterTest.toColunmFamilies("f"),true);
		assertFalse(filter.hasTop());
		assertNull(filter.getTopKey());
		assertNull(filter.getTopValue());
	}

	@Test
	public void testFilter() throws IOException {
		MockSortedKeyValueIterator source = makeTestData();
		RowValuesFilter filter = new RowValuesFilter();
		Map<String, String> options = new HashMap<>();
		options.put("req:f:x:int","3");
		filter.init(source, options, null);
		filter.seek(new Range(),RowPrefixFilterTest.toColunmFamilies("f"),true);
		assertTrue(filter.hasTop());
		Key key = filter.getTopKey();
		assertEquals("a",key.getRow().toString());
		assertEquals("x",key.getColumnQualifier().toString());
		filter.next();
		assertTrue(filter.hasTop());
		key = filter.getTopKey();
		assertEquals("a",key.getRow().toString());
		assertEquals("y",key.getColumnQualifier().toString());
		filter.next();
		assertTrue(filter.hasTop());
		key = filter.getTopKey();
		assertEquals("a",key.getRow().toString());
		assertEquals("z",key.getColumnQualifier().toString());
		filter.next();
		assertTrue(filter.hasTop());
		key = filter.getTopKey();
		assertEquals("d",key.getRow().toString());
		assertEquals("x",key.getColumnQualifier().toString());
		filter.next();
		assertTrue(filter.hasTop());
		filter.next();
		assertTrue(filter.hasTop());
		key = filter.getTopKey();
		assertEquals("d",key.getRow().toString());
		assertEquals("z",key.getColumnQualifier().toString());
		filter.next();
		assertFalse(filter.hasTop());
		assertNull(filter.getTopKey());
		assertNull(filter.getTopValue());
	}

	private MockSortedKeyValueIterator makeTestData() {
		MockSortedKeyValueIterator source = new MockSortedKeyValueIterator();
		source.add("a", "f", "x", intToBytes(3));
		source.add("a", "f", "y", "scissors".getBytes());
		source.add("a", "f", "z", "EUROPE".getBytes());
		source.add("b", "f", "x", intToBytes(6));
		source.add("b", "f", "y", "rock".getBytes());
		source.add("b", "f", "z", "USA".getBytes());
		source.add("c", "f", "x", intToBytes(5));
		source.add("c", "f", "y", "scissors".getBytes());
		source.add("c", "f", "z", "USA".getBytes());
		source.add("d", "f", "x", intToBytes(3));
		source.add("d", "f", "y", "paper".getBytes());
		source.add("d", "f", "z", "ASIA".getBytes());
		source.add("e", "f", "x", intToBytes(6));
		source.add("e", "f", "y", "rock".getBytes());
		source.add("e", "f", "z", "EUROPE".getBytes());
		source.done();
		return source;
	}

}
