package knapp.open.iterators.util;

import java.util.List;

/**
 * This is used to filter rows, it represents a requirement 
 * for a specific column.  You can combine it with other requirements,
 * to make complex combined requirements.  This just says that 
 * the value must be one that is on the list of acceptable values.
 * @author michael
 *
 */
public class ColumnRequirement {
	private String columnFamily;
	private String columnQualifier;
	private String type;
	private List<String> acceptableValues;

	public List<String> getAcceptableValues() {
		return acceptableValues;
	}

	public boolean isAcceptable(byte[] val) {
		for (String v : acceptableValues) {
			if (AccIterUtil.areSame(v, val, type)) {
				return true;
			}
		}
		return false;
	}

	public void setAcceptableValues(List<String> acceptableValues) {
		this.acceptableValues = acceptableValues;
	}

	public String getColumnFamily() {
		return columnFamily;
	}

	public void setColumnFamily(String columnFamily) {
		this.columnFamily = columnFamily;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getColumnQualifier() {
		return columnQualifier;
	}

	public void setColumnQualifier(String columnQualifier) {
		this.columnQualifier = columnQualifier;
	}
}
