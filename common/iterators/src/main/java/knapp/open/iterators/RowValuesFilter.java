package knapp.open.iterators;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import knapp.common.util.ByteUtils;
import knapp.common.util.StringHelp;
import knapp.open.iterators.util.AccIterUtil;
import knapp.open.iterators.util.ColumnRequirement;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;
import org.apache.accumulo.core.iterators.user.RowFilter;


/**
 * Filters rows down based on a set of pre-defined
 * acceptable values.  The properties given should 
 * follow this format:
 * 
 * <req>:<f>:<d>:<type>=value1[,value2[...]]
 * 
 * <p>Values can be quoted, and must be separated 
 * by commas.</p>
 * @author michael
 *
 */
public class RowValuesFilter extends RowFilter {
	private static final String ALL_MUST_PASS="all";
	private boolean allMustPass=true;
	private Map<String,ColumnRequirement> requirements = new HashMap<>();
	
	@Override
	public void init(SortedKeyValueIterator<Key, Value> source,
			Map<String, String> options, IteratorEnvironment env)
			throws IOException {
		super.init(source, options, env);
		if (options.containsKey(ALL_MUST_PASS)) {
			String s = options.get(ALL_MUST_PASS);
			this.allMustPass = Boolean.parseBoolean(s);
		}
		for (String prop : options.keySet()) {
			if (prop.startsWith("req:")) {
				// "<req>:<f>:<d>:<type>=value1[,value2[...]]
				String[] def = prop.split(":");
				ColumnRequirement req = new ColumnRequirement();
				req.setColumnFamily(def[1]);
				req.setColumnQualifier(def[2]);
				req.setType(def[3]);
				List<String> vals = StringHelp.splitConsideringQuotes(options.get(prop),',');
				req.setAcceptableValues(vals);
				String cf = String.format("%s.%s",def[1],def[2]);
				requirements.put(cf, req);
			}
		}
	}
	
	@Override
	public boolean acceptRow(SortedKeyValueIterator<Key, Value> row)
			throws IOException {
		if (row==null || !row.hasTop() || row.getTopKey()==null) {
			return false;
		}
		Map<String,byte[]> vals = AccIterUtil.rowToValues(requirements.keySet(), row);
		int passed = 0;
		int failed = 0;
		for (String cf : requirements.keySet()) {
			ColumnRequirement req = requirements.get(cf);
			byte[] val = vals.get(cf);
			boolean pass = req.isAcceptable(val);
			if (allMustPass && !pass) {
				return false;
			}
			if (!allMustPass && pass) {
				return true;
			}
			if (pass) {
				passed++;
			} else {
				failed++;
			}
		}
		return allMustPass ? failed==0 : passed > 0;
	}

	public RowValuesFilter deepCopy(IteratorEnvironment env) {
		RowValuesFilter copy = (RowValuesFilter) super.deepCopy(env);
		copy.allMustPass = allMustPass;
		copy.requirements = requirements;
		return copy;
	}
	
}