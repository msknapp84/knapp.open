package knapp.open.iterators.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class WhereTreeNode {
	private boolean isOrOperation = false;
	private Set<WhereTreeNode> subNodes = null;
	private String columnFamilyAndQualifier;
	private List<String> acceptableValues;
	private Comparable<?> min,max;
	private WhereTreeNode parent;
	private Map<String,String> types = new HashMap<>();
	
	public WhereTreeNode getRoot() {
		return parent == null ? this : parent.getRoot(); 
	}
	
	public boolean acceptRow(Map<String,byte[]> values) {
		boolean acceptableSoFar = true;
		if (subNodes!=null) {
			for (WhereTreeNode subNode : subNodes) {
				if (isOrOperation && subNode.acceptRow(values)) {
					acceptableSoFar=true;
					break;
				}
				if (!isOrOperation && !subNode.acceptRow(values)) {
					return false;
				}
			}
			return true;
		}
		// may know the types.
		// there are no subnodes, you must check the actual value.
		if (acceptableValues!=null && !acceptableValues.isEmpty()) {
			byte[] val = values.get(columnFamilyAndQualifier);
			if (val ==null) {
				return false;
			}
			String tp = types.get(columnFamilyAndQualifier);
			String realValue = expressAsType(val,tp);
			// this is like an 'in' statement.
			return acceptableValues.contains(realValue);
		}
		// TODO handle ranges.
		return false;
	}

	private String expressAsType(byte[] val, String tp) {
		
		return null;
	}

	/**
	 * This is responsible for determining what columns and 
	 * qualifiers are needed.
	 * @return
	 */
	public List<String> getRequiredColumns() {
		List<String> req = new ArrayList<>();
		if (subNodes!=null) {
			for (WhereTreeNode n : subNodes) {
				req.addAll(n.getRequiredColumns());
			}
		}
		if (this.columnFamilyAndQualifier!=null) {
			req.add(columnFamilyAndQualifier);
		}
		return req;
	}
	
	public static WhereTreeNode parse(String where,Map<String,String> types) {
		// TODO parse the where tree.
		// this usually requires a state machine, like with 
		// antlr, parboiled, or ragel.
		
		// I don't have forever though, so let's just support these:
		// in, and, or, parentheses.
		List<IsQuoted> split = IsQuoted.splitQuoted(where);
		WhereTreeNode node = new WhereTreeNode();
		node.setTypes(types);
		
		// then split by parentheses.
		List<ParenthesizedGroup> parens = splitByParentheses(split);
		for (ParenthesizedGroup pg : parens) {
			if (pg.isInParens()) {
				// it's a sub-node
				WhereTreeNode sub = parse(pg.toString(),types);
				node.subNodes.add(sub);
			} else {
				
			}
		}
		return node.getRoot();
	}
	
	private static List<ParenthesizedGroup> splitByParentheses(List<IsQuoted> split) {
		List<ParenthesizedGroup> res = new ArrayList<>();
		ParenthesizedGroup current = new ParenthesizedGroup();
		res.add(current);
		int level = 0;
		for (IsQuoted iq : split) {
			if (iq.isQuoted()) {
				// it's a value.
				current.getSubParts().add(iq);
			} else {
				// defining something here.
//				String[] parts
				int start = 0;
				for (int i = 0;i<iq.getValue().length();i++) {
					char c = iq.getValue().charAt(i);
					if (c == '(') {
						// opening paren
						IsQuoted niq = new IsQuoted(iq.getValue().substring(start,i), false, start+iq.getStart(), i+iq.getStop());
						
						start = i+1;
						level++;
					} else if (c == ')') {
						// closing.
						if (level == 0) {
							// it's closed.
							IsQuoted niq = new IsQuoted(iq.getValue().substring(start,i), false, start+iq.getStart(), i+iq.getStop());
						}
						level--;
					}
				}
			}
		}
		return res;
	}

	public boolean isOrOperation() {
		return isOrOperation;
	}

	public void setOrOperation(boolean isOrOperation) {
		this.isOrOperation = isOrOperation;
	}

	public Set<WhereTreeNode> getSubNodes() {
		return subNodes;
	}

	public void setSubNodes(Set<WhereTreeNode> subNodes) {
		this.subNodes = subNodes;
	}

	public String getColumnFamilyAndQualifier() {
		return columnFamilyAndQualifier;
	}

	public void setColumnFamilyAndQualifier(String columnFamilyAndQualifier) {
		this.columnFamilyAndQualifier = columnFamilyAndQualifier;
	}

	public List<String> getAcceptableValues() {
		return acceptableValues;
	}

	public void setAcceptableValues(List<String> acceptableValues) {
		this.acceptableValues = acceptableValues;
	}

	public Comparable<?> getMin() {
		return min;
	}

	public void setMin(Comparable<?> min) {
		this.min = min;
	}

	public Comparable<?> getMax() {
		return max;
	}

	public void setMax(Comparable<?> max) {
		this.max = max;
	}

	public WhereTreeNode getParent() {
		return parent;
	}

	public void setParent(WhereTreeNode parent) {
		this.parent = parent;
	}

	public Map<String, String> getTypes() {
		return types;
	}

	public void setTypes(Map<String, String> types) {
		this.types = types;
	}
}
