package knapp.open.iterators;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;
import org.apache.accumulo.core.iterators.user.RowFilter;

/**
 * Filters rows down, returning the rows whose value for a prefix is different
 * from the last.  If you provide no arguments, then it will use underscores and/or
 * hyphens to split the string, and will compare the first part to determine when
 * the keys have truly changed.
 * 
 * @author michael
 *
 */
public class RowPrefixFilter extends RowFilter {
	public static final String REGEX_PROPERTY = "regex";
	public static final String GROUPS_PROPERTY = "groups";
	public static final String CASE_SENSITIVE_PROPERTY = "case_sensitive";

	// patterns are immutable and thread safe.
	private Pattern pattern;
	private int[] groupsToCheck;
	private String lastMatch;

	private boolean caseSensitive = true;

	@Override
	public void init(SortedKeyValueIterator<Key, Value> source,
			Map<String, String> options, IteratorEnvironment env)
			throws IOException {
		super.init(source, options, env);
		if (options.containsKey(CASE_SENSITIVE_PROPERTY)) {
			this.caseSensitive = Boolean.parseBoolean(options
					.get(CASE_SENSITIVE_PROPERTY));
		}
		String regex = null;
		if (options.containsKey(REGEX_PROPERTY)) {
			regex = options.get(REGEX_PROPERTY);
		} else {
			// assume base 64 encoding, separated by
			// hyphens or underscores.
			regex = "([a-zA-Z0-9/=]+)[\\-_:].*";
		}
		if (caseSensitive) {
			this.pattern = Pattern.compile(regex);
		} else {
			this.pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		}
		if (options.containsKey(GROUPS_PROPERTY)) {
			String g = options.get(GROUPS_PROPERTY);
			String[] gs = g.split(",");
			groupsToCheck = new int[gs.length];
			for (int i = 0; i < gs.length; i++) {
				groupsToCheck[i] = Integer.parseInt(gs[i]);
			}
		} else {
			groupsToCheck = new int[] { 1 };
		}
	}

	@Override
	public boolean acceptRow(SortedKeyValueIterator<Key, Value> row)
			throws IOException {
		if (row==null || !row.hasTop() || row.getTopKey()==null) {
			return false;
		}
		// we just want to check the first key,
		String rowId = row.getTopKey().getRow().toString();
		Matcher m = pattern.matcher(rowId);
		boolean hasChanged = false;
		if (m.matches()) {
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < groupsToCheck.length; i++) {
				int groupToCheck = groupsToCheck[i];
				String group = m.group(groupToCheck);
				sb.append(group);
			}
			String currentMatch = sb.toString();
			if (lastMatch == null || !currentMatch.equals(lastMatch)) {
				hasChanged = true;
			}
			lastMatch = currentMatch;
		}
		return hasChanged;
	}

	public RowPrefixFilter deepCopy(IteratorEnvironment env) {
		// need to capture these private fields in the copy.
		RowPrefixFilter copy = (RowPrefixFilter) super.deepCopy(env);
		// patterns are immutable and thread safe.
		copy.pattern = pattern;
		copy.groupsToCheck = Arrays.copyOf(groupsToCheck, groupsToCheck.length);
		copy.lastMatch = lastMatch;
		copy.caseSensitive = caseSensitive;
		return copy;
	}

	public Pattern getPattern() {
		return pattern;
	}

	public int[] getGroupsToCheck() {
		return groupsToCheck;
	}

	public String getLastMatch() {
		return lastMatch;
	}

	public boolean isCaseSensitive() {
		return caseSensitive;
	}
}