package knapp.open.iterators.util;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import knapp.common.util.ByteUtils;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;

public final class AccIterUtil {

	private AccIterUtil() {
		
	}

	public static Map<String, byte[]> rowToValues(Collection<String> required,
			SortedKeyValueIterator<Key, Value> row) throws IOException {
		Map<String,byte[]> values = new HashMap<>();
		while (true) {
			if (row.hasTop()) {
				Key key = row.getTopKey();
				String col = String.format("%s.%s",key.getColumnFamily().toString(),key.getColumnQualifier().toString());
				if (required.contains(col)) {
					Value value = row.getTopValue();
					values.put(col, value.get());
				}
				row.next();
			} else {
				break;
			}
		}
		return values;
	}
	
	public static boolean areSame(String v, byte[] val, String type) {
		if ("int".equalsIgnoreCase(type)) {
			int x = ByteUtils.bytesToInt(val);
			Integer y = Integer.parseInt(v);
			return y.equals(x);
		} else if ("short".equalsIgnoreCase(type)) {
			short x = ByteUtils.bytesToShort(val);
			Short y = Short.parseShort(v);
			return y.equals(x);
		} else if ("long".equalsIgnoreCase(type)) {
			long x = ByteUtils.bytesToLong(val);
			Long y = Long.parseLong(v);
			return y.equals(x);
		} else if ("double".equalsIgnoreCase(type)) {
			double x = ByteUtils.bytesToDouble(val);
			Double y = Double.parseDouble(v);
			return Math.abs(y-x)<1e-9;
		} else if ("float".equalsIgnoreCase(type)) {
			float x = ByteUtils.bytesToFloat(val);
			Float y = Float.parseFloat(v);
			return Math.abs(y-x)<1e-6;
		} else if ("boolean".equalsIgnoreCase(type)) {
			boolean x = ByteUtils.bytesToBool(val);
			Boolean y = Boolean.parseBoolean(v);
			return y.equals(x);
		} else if ("datetime".equalsIgnoreCase(type)) {
			LocalDateTime x = ByteUtils.bytesToDateTime(val);
			LocalDateTime y = LocalDateTime.from(DateTimeFormatter.ISO_DATE_TIME.parse(v));
			return y.equals(x);
		} else if ("date".equalsIgnoreCase(type)) {
			LocalDate x = ByteUtils.bytesToDate(val);
			LocalDate y = LocalDate.from(DateTimeFormatter.ISO_DATE.parse(v));
			return y.equals(x);
		} else {
			String x = String.valueOf(val);
			return v.equals(x);
		}
	}
}
