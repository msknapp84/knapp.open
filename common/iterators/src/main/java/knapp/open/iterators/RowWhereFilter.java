package knapp.open.iterators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import knapp.open.iterators.util.WhereTreeNode;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;
import org.apache.accumulo.core.iterators.user.RowFilter;


/**
 * Filters rows down based on a where clause, 
 * with a limited amount of functionality.  It 
 * will parse the where clause into an abstract
 * syntax tree and use that to filter rows.
 * 
 * <p>You can tell it what type to treat each 
 * call as using properties.  Provide 'type.<cf>.<cq>=<type>"
 * and this will know what the types are.</p>
 * @author michael
 *
 */
public class RowWhereFilter extends RowFilter {
	private static final String WHERE_PROPERTY="where";
	private String where;
	private WhereTreeNode whereNode;
	private Map<String,String> types = new HashMap<>();
	
	@Override
	public void init(SortedKeyValueIterator<Key, Value> source,
			Map<String, String> options, IteratorEnvironment env)
			throws IOException {
		super.init(source, options, env);
		this.where = options.get(WHERE_PROPERTY);
		this.whereNode = WhereTreeNode.parse(where,types);
		for (String prop : options.keySet()) {
			if (prop.startsWith("type.")) {
				// it is telling me what the type is.
				String[] tkv = options.get(prop).split("=");
				types.put(tkv[0].substring(5).trim(),tkv[1].trim());
			}
		}
	}
	
	@Override
	public boolean acceptRow(SortedKeyValueIterator<Key, Value> row)
			throws IOException {
		List<String> required = whereNode.getRequiredColumns();
		Map<String,byte[]> values = rowToValues(required,row);
		return whereNode.acceptRow(values);
	}

	private Map<String, byte[]> rowToValues(List<String> required,
			SortedKeyValueIterator<Key, Value> row) throws IOException {
		Map<String,byte[]> values = new HashMap<>();
		while (true) {
			row.next();
			if (!row.hasTop()) {
				break;
			}
			Key key = row.getTopKey();
			String col = String.format("%s.%s",key.getColumnFamily().toString(),key.getColumnQualifier().toString());
			if (required.contains(col)) {
				Value value = row.getTopValue();
				values.put(col, value.get());
			}
		}
		return values;
	}

	public RowWhereFilter deepCopy(IteratorEnvironment env) {
		RowWhereFilter copy = (RowWhereFilter) super.deepCopy(env);
		copy.where = where;
		copy.whereNode = WhereTreeNode.parse(where,types);
		copy.types = types;
		return copy;
	}
	
}