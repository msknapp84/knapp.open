package knapp.open.iterators.util;

import java.util.ArrayList;
import java.util.List;


public class ParenthesizedGroup {
	private List<IsQuoted> subParts = new ArrayList<>();
	private ParenthesizedGroup left,right,parent;
	private boolean inParens=false;
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (IsQuoted iq : subParts) {
			sb.append(iq.toString());
		}
		
		return sb.toString();
	}
	public List<IsQuoted> getSubParts() {
		return subParts;
	}
	public void setSubParts(List<IsQuoted> subParts) {
		this.subParts = subParts;
	}
	public ParenthesizedGroup getLeft() {
		return left;
	}
	public void setLeft(ParenthesizedGroup left) {
		this.left = left;
	}
	public ParenthesizedGroup getRight() {
		return right;
	}
	public void setRight(ParenthesizedGroup right) {
		this.right = right;
	}
	public ParenthesizedGroup getParent() {
		return parent;
	}
	public void setParent(ParenthesizedGroup parent) {
		this.parent = parent;
	}
	public boolean isInParens() {
		return inParens;
	}
	public void setInParens(boolean inParens) {
		this.inParens = inParens;
	}
}
