package knapp.open.iterators;

import java.io.IOException;
import java.util.Collection;

import knapp.common.util.ByteUtils;

import org.apache.accumulo.core.data.ByteSequence;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.WrappingIterator;

/**
 * Returns the count of whatever iterator is beneath it. This does not check if
 * the underlying values are unique, so duplicates get counted. Accumulo's
 * CountingIterator does not actually return the count that it gets, so it's not
 * doing the same thing as this. This counts the number of cells, aka keys. If
 * you want to count the number of rows then have your scanner limit results to
 * only one column.
 * 
 * @author michael
 *
 */
public class CountIterator extends WrappingIterator {

	private int count = 0;
	private boolean counted = false;
	private boolean taken = false;

	// apparently the next method may be called after the getTopValue and
	// getTopKey have been called. I think that is backwards but oh well.

	// methods are invoked in this order: seek, getTopKey, getTopValue, next,
	// then hasTop.

	@Override
	public void next() throws IOException {
		if (counted) {
			return;
		}
		while (getSource().hasTop()) {
			getSource().next();
			count++;
		}
		counted = true;
	}

	@Override
	public CountIterator deepCopy(IteratorEnvironment env) {
		CountIterator c = (CountIterator) super.deepCopy(env);
		c.count = count;
		c.counted = counted;
		c.taken=taken;
		return c;
	}

	@Override
	public Value getTopValue() {
		taken=true;
		return new Value(ByteUtils.intToBytes(count));
	}

	@Override
	public Key getTopKey() {
		// it is possible that there were no rows at all.
//		if (count == 0 || getSource()==null || !getSource().hasTop() || getSource().getTopKey()==null) {
			return new Key("count", "f", "q");
//		}
//		return getSource().getTopKey();
	}

	@Override
	public boolean hasTop() {
		return !taken;
	}

	@Override
	public void seek(Range range, Collection<ByteSequence> columnFamilies,
			boolean inclusive) throws IOException {
		super.seek(range, columnFamilies, inclusive);
		next();
	}
}
