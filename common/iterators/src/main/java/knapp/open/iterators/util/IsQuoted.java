package knapp.open.iterators.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class IsQuoted {
	private String value;
	private boolean quoted;
	private char quoteChar;
	private int start,stop;
	private IsQuoted left,right;
	
	public IsQuoted(String value,boolean quoted,int start,int stop) {
		this.value = value;
		this.quoted = quoted;
		this.start = start;
		this.stop = stop;
	}
	
	public static String reconstruct(List<IsQuoted> splits) {
		return splits.stream().map(Object::toString).collect(Collectors.joining());
	}
	
	public static List<IsQuoted> splitQuoted(String input) {
		if (input == null) {
			return Collections.EMPTY_LIST;
		}
		List<IsQuoted> res = new ArrayList<>();
		int start = 0;
		Character lastQuoteChar = null;
		Character quoteChar = null;
		for (int i = 0;i<input.length();i++) {
			char c = input.charAt(i);
			boolean switched = false;
			if (c == '\'') {
				if (quoteChar==null) {
					lastQuoteChar=null;
					quoteChar = c;
					switched=true;
				} else if (quoteChar.equals(c)) {
					lastQuoteChar=quoteChar;
					quoteChar=null;
					switched=true;
				}
			} else if (c == '"') {
				if (quoteChar==null) {
					lastQuoteChar=null;
					quoteChar = c;
					switched=true;
				} else if (quoteChar.equals(c)){
					lastQuoteChar=quoteChar;
					quoteChar=null;
					switched=true;
				}
			}
			if (switched || i == input.length()-1) {
				String x = input.substring(start,i);
				// if quote char is null now, that means it was
				// in quotes prior, 
				boolean inQuotes = quoteChar==null;
				IsQuoted iq = new IsQuoted(x,inQuotes,start,i);
				if (inQuotes) {
					iq.setQuoteChar(lastQuoteChar);
				}
				res.add(iq);
				start = i+1;
			}
		}
		for (int i = 0;i<res.size();i++) {
			IsQuoted iq = res.get(i);
			if (i>0) {
				iq.setLeft(res.get(i-1));
			}
			if (i<res.size()-1) {
				iq.setRight(res.get(i+1));
			}
		}
		return res;
	}
	
	public String toString() {
		if (quoted) {
			return quoteChar+value+quoteChar;
		}
		return value;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public boolean isQuoted() {
		return quoted;
	}
	public void setQuoted(boolean quoted) {
		this.quoted = quoted;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getStop() {
		return stop;
	}

	public void setStop(int stop) {
		this.stop = stop;
	}

	public IsQuoted getLeft() {
		return left;
	}

	public void setLeft(IsQuoted left) {
		this.left = left;
	}

	public IsQuoted getRight() {
		return right;
	}

	public void setRight(IsQuoted right) {
		this.right = right;
	}

	public char getQuoteChar() {
		return quoteChar;
	}

	public void setQuoteChar(char quoteChar) {
		this.quoteChar = quoteChar;
	}
}
