package knapp.open.iterators;

import java.io.IOException;
import java.util.Map;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.iterators.IteratorEnvironment;
import org.apache.accumulo.core.iterators.SortedKeyValueIterator;
import org.apache.accumulo.core.iterators.user.RowFilter;

/**
 * Filters rows by their number.  If you provide no arguments, 
 * then this will return the first row in the range only.
 * This is mostly useful when you have a row key that ends with 
 * a descending time stamp.  You want the latest value, but you 
 * don't know in advance exactly what that row id will be, you 
 * just know it will be the first one you find in that range.
 * @author michael
 *
 */
public class NthRowFilter extends RowFilter implements SortedKeyValueIterator<Key, Value> {
	public static String DESIRED_ROW_PROPERTY = "row.number";
	private int desiredRowNumber = 0;
	
	private int rowNumber = 0;
	
	@Override
	public void init(SortedKeyValueIterator<Key, Value> source, Map<String,String> options, IteratorEnvironment env) throws IOException {
		super.init(source,options,env);
		if (options.containsKey(DESIRED_ROW_PROPERTY)) {
			this.desiredRowNumber = Integer.parseInt(options.get(DESIRED_ROW_PROPERTY));
		}
	}
	
	@Override
	public boolean acceptRow(SortedKeyValueIterator<Key, Value> arg0)
			throws IOException {
		return  (rowNumber++ == desiredRowNumber);
	}
	
	public NthRowFilter deepCopy(IteratorEnvironment env) {
		// need to capture these private fields in the copy.
		NthRowFilter copy = (NthRowFilter) super.deepCopy(env);
		copy.desiredRowNumber = this.desiredRowNumber;
		copy.rowNumber = this.rowNumber;
		return copy;
	}
}