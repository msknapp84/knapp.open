package knapp.common.collections.time;

import java.time.LocalDate;
import java.util.Iterator;

import org.junit.Test;

import static org.junit.Assert.*;

public class DoubleTableImmutableTest {

	@Test
	public void testEmpty() {
		DoubleTable dt = new DoubleTable();
		DoubleTableImmutable dti = new DoubleTableImmutable(dt);
		double v = dti.get(LocalDate.now(), 3, true);
		assertEquals(DoubleTableImmutable.SUGGESTED_UNSET_VALUE, v, 1e-7);
		assertFalse(dti.has(LocalDate.now(), 3, true));
		Iterator<DoubleTableCell> iter = dti.iterator();
		assertFalse(iter.hasNext());
		assertNull(iter.next());
	}

	@Test
	public void testOne() {
		DoubleTable dt = new DoubleTable();
		dt.add(LocalDate.of(2010, 3, 19), 3, 925.15);
		DoubleTableImmutable dti = new DoubleTableImmutable(dt);
		double v = dti.get(LocalDate.now(), 3, true);
		assertEquals(925.15, v, 1e-7);
		v = dti.get(LocalDate.of(2010, 3, 20), 3, true);
		assertEquals(925.15, v, 1e-7);
		v = dti.get(LocalDate.of(2010, 3, 19), 3, true);
		assertEquals(925.15, v, 1e-7);
		v = dti.get(LocalDate.of(2010, 3, 18), 3, true);
		assertEquals(DoubleTableImmutable.SUGGESTED_UNSET_VALUE, v, 1e-7);
		assertTrue(dti.has(LocalDate.now(), 3, true));
		assertFalse(dti.has(LocalDate.now(), 4, true));
		assertFalse(dti.has(LocalDate.now(), 3, false));
		Iterator<DoubleTableCell> iter = dti.iterator();
		assertTrue(iter.hasNext());
		DoubleTableCell cell = iter.next();
		assertEquals(925.15, cell.getValue(), 1e-7);
		assertEquals(LocalDate.of(2010, 3, 19), cell.getLocalDate());
		assertFalse(iter.hasNext());
		assertNull(iter.next());
		assertEquals(1, dti.getCellsSize());
		assertEquals(1, dti.getSetCellsSize());
		assertEquals(1, dti.getLabelsSize());
		assertEquals(1, dti.getDatesSize());
	}

	@Test
	public void testSome() {
		DoubleTable dt = new DoubleTable();
		dt.add(LocalDate.of(2010, 3, 19), 3, 925.15);
		dt.add(2005, 6, 17, 8, 64.1);
		dt.add(2009, 9, 12, 1, 48);
		dt.add(2008, 6, 27, 4, 618.03);
		dt.add(2009, 5, 17, 8, 124.7);
		dt.add(2010, 2, 5, 1, 6742.15);
		dt.add(2011, 3, 14, 4, DoubleTableImmutable.SUGGESTED_UNSET_VALUE);
		DoubleTableImmutable dti = new DoubleTableImmutable(dt);
		
		assertEquals(6, dti.getSetCellsSize());
		assertEquals(4, dti.getLabelsSize());
		assertEquals(7, dti.getDatesSize());
		assertEquals(28, dti.getCellsSize());

		assertEquals(64.1, dti.get(2005, 6, 18, 8, true), 1e-7);
		assertEquals(DoubleTableImmutable.SUGGESTED_UNSET_VALUE, dti.get(2005, 6, 18, 8, false), 1e-7);

		// it will iterate through all dates of one label
		// before moving on to the second label.
		Iterator<DoubleTableCell> iter = dti.iterator();
		assertTrue(iter.hasNext());
		DoubleTableCell cell = iter.next();
		assertEquals(48, cell.getValue(), 1e-7);
		assertEquals(LocalDate.of(2009, 9, 12), cell.getLocalDate());
		assertEquals(1, cell.getLabel());

		assertTrue(iter.hasNext());
		cell = iter.next();
		assertEquals(6742.15, cell.getValue(), 1e-7);
		assertEquals(LocalDate.of(2010, 2, 5), cell.getLocalDate());
		assertEquals(1, cell.getLabel());
		
		assertTrue(iter.hasNext());
		cell = iter.next();
		assertEquals(925.15, cell.getValue(), 1e-7);
		assertEquals(LocalDate.of(2010, 3, 19), cell.getLocalDate());
		assertEquals(3, cell.getLabel());
		
		assertTrue(iter.hasNext());
		cell = iter.next();
		assertEquals(618.03, cell.getValue(), 1e-7);
		assertEquals(LocalDate.of(2008, 6, 27), cell.getLocalDate());
		assertEquals(4, cell.getLabel());
		
		assertTrue(iter.hasNext());
		cell = iter.next();
		assertEquals(64.1, cell.getValue(), 1e-7);
		assertEquals(LocalDate.of(2005, 6, 17), cell.getLocalDate());
		assertEquals(8, cell.getLabel());
		
		assertTrue(iter.hasNext());
		cell = iter.next();
		assertEquals(124.7, cell.getValue(), 1e-7);
		assertEquals(LocalDate.of(2009, 5, 17), cell.getLocalDate());
		assertEquals(8, cell.getLabel());
		
		assertFalse(iter.hasNext());
		assertNull(iter.next());
	}
}