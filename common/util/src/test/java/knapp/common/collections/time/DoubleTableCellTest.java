package knapp.common.collections.time;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import knapp.common.util.ByteUtils;

import org.junit.Test;

import static org.junit.Assert.*;

public class DoubleTableCellTest {
	
	@Test
	public void testIt() {
		DoubleTableCell dtc1 = new DoubleTableCell(2010,1,14,25.5,3);
		DoubleTableCell dtc2 = new DoubleTableCell(2010,1,16,38.15,4);
		DoubleTableCell dtc3 = new DoubleTableCell(2010,3,14,21.1,3);
		DoubleTableCell dtc4 = new DoubleTableCell(2010,6,28,18.2,1);
		DoubleTableCell dtc5 = new DoubleTableCell(2011,2,3,95.1,4);
		List<DoubleTableCell> cells = new ArrayList<>();
		cells.addAll(Arrays.asList(dtc1,dtc2,dtc3,dtc4,dtc5));
		Collections.shuffle(cells);
		Collections.sort(cells);
		assertEquals(dtc4,cells.get(0));
		assertEquals(dtc1,cells.get(1));
		assertEquals(dtc3,cells.get(2));
		assertEquals(dtc2,cells.get(3));
		assertEquals(dtc5,cells.get(4));
		for (int i = 0;i<cells.size()-1;i++) {
			for (int j = i+1;j<cells.size();j++) {
				assertNotSame(cells.get(i),cells.get(j));
				assertNotSame(cells.get(i).hashCode(),cells.get(j).hashCode());
			}
		}
		byte[] d = ByteUtils.dateToBytes(LocalDate.of(2008,9,1));
		dtc3.setDate(d);
		assertArrayEquals(d,dtc3.getDate());
		d[0] = 0;
		assertFalse(Arrays.equals(d,dtc3.getDate()));
		assertNotSame(dtc1,dtc3);
		dtc1.copyIn(dtc3);
		assertEquals(dtc1,dtc3);
	}

}
