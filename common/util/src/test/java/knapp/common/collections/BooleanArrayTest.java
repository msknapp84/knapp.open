package knapp.common.collections;

import knapp.common.collections.BooleanArray;

import org.junit.Test;

import static org.junit.Assert.*;

public class BooleanArrayTest {
	
	@Test
	public void testArray() {
		BooleanArray ba = new BooleanArray(30);
		for (int i = 0;i<30;i++) {
			assertFalse(ba.isTrue(i));
		}
		ba.set(18, true);
		assertTrue(ba.isTrue(18));
		ba.set(18, false);
		assertFalse(ba.isTrue(18));
		
		for (int i = 0;i<30;i++) {
			ba.set(i,true);
			assertTrue("Failed on number "+i,ba.isTrue(i));
			ba.set(i, false);
			assertFalse(ba.isTrue(i));
		}
		for (int i = 0;i<30;i++) {
			if (i>0) {
				ba.set(i-1,true);
			}
			if (i<29) {
				ba.set(i+1,true);
			}
			ba.set(i,true);
			assertTrue(ba.isTrue(i));
			ba.set(i, false);
			assertFalse(ba.isTrue(i));
			if (i>0) {
				ba.set(i-1,false);
			}
			if (i<29) {
				ba.set(i+1,false);
			}
		}

	}

}
