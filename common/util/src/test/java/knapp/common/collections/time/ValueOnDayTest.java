package knapp.common.collections.time;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import knapp.common.util.ByteUtils;

import org.junit.Test;

public class ValueOnDayTest {
	
	@Test
	public void testVOD() {
		ValueOnDay<Double> vod = new ValueOnDay<Double>();
		LocalDate now = LocalDate.now();
		byte[] d = ByteUtils.dateToBytes(now);
		vod.setDate(d);
		d[0]=0;
		assertEquals(now,vod.getDate());
		vod.getBinaryDate()[0]=0;
		assertEquals(now,vod.getDate());
		ValueOnDay<Double> other = new ValueOnDay<Double>();
		other.setValue(852.91);
		other.setDate(LocalDate.of(1970,1,1));
		vod.copyIn(other);
		assertNotSame(now,vod.getDate());
		assertEquals(vod,other);
		assertEquals(vod.hashCode(),other.hashCode());
		
		vod.setValue(325.85);
		assertEquals(325.85,vod.getValue(),1e-6);
		assertEquals(852.91,other.getValue(),1e-6);
		assertNotSame(vod,other);
		assertNotSame(vod.hashCode(),other.hashCode());
	}
	
	@Test
	public void testCompare() {
		ValueOnDay<Integer> vod1 = new ValueOnDay<Integer>(LocalDate.of(2010, 5, 2));
		ValueOnDay<Integer> vod2 = new ValueOnDay<Integer>(2010, 5, 23);
		ValueOnDay<Integer> vod3 = new ValueOnDay<Integer>(2010, 2, 18);
		List<ValueOnDay<Integer>> lst = new ArrayList<>();
		lst.add(vod1);
		lst.add(vod2);
		lst.add(vod3);
		Collections.sort(lst);
		assertEquals(vod3,lst.get(0));
		assertEquals(vod1,lst.get(1));
		assertEquals(vod2,lst.get(2));
	}
	
}