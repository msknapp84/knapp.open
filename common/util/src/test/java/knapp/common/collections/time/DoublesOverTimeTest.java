package knapp.common.collections.time;

import java.util.Iterator;

import org.junit.Test;

import static org.junit.Assert.*;

public class DoublesOverTimeTest {

	@Test
	public void testEmpty() {
		DoublesOverTime dot = new DoublesOverTime();
		assertEquals(DoubleTableImmutable.SUGGESTED_UNSET_VALUE,
				dot.get(2005, 4, 9), 1e-6);
		Iterator<ValueOnDay<Double>> iter = dot.iterator();
		assertFalse(iter.hasNext());
		assertNull(iter.next());
	}

	@Test
	public void testOne() {
		DoublesOverTime dot = new DoublesOverTime();
		dot.add(2005, 4, 9, 25.1);
		assertEquals(25.1, dot.get(2005, 4, 9), 1e-6);
		Iterator<ValueOnDay<Double>> iter = dot.iterator();
		assertTrue(iter.hasNext());
		assertEquals(25.1, iter.next().getValue(), 1e-6);
		assertFalse(iter.hasNext());
		assertNull(iter.next());
	}

	@Test
	public void testOneUnset() {
		DoublesOverTime dot = new DoublesOverTime();
		dot.add(2005, 4, 9, DoubleTableImmutable.SUGGESTED_UNSET_VALUE);
		assertEquals(DoubleTableImmutable.SUGGESTED_UNSET_VALUE,
				dot.get(2005, 4, 9), 1e-6);
		Iterator<ValueOnDay<Double>> iter = dot.iterator();
		assertFalse(iter.hasNext());
		assertNull(iter.next());
		assertEquals(0, dot.getSize());
		dot.add(2005, 4, 9, 25.1);
		assertEquals(25.1, dot.get(2005, 4, 9), 1e-6);
		assertEquals(1, dot.getSize());
		dot.add(2005, 4, 9, DoubleTableImmutable.SUGGESTED_UNSET_VALUE);
		assertEquals(0, dot.getSize());
	}

	@Test
	public void testSome() {
		DoublesOverTime dot = new DoublesOverTime();
		dot.add(2005, 4, 9, 25.1);
		dot.add(2008, 1, 29, 37.5);
		assertEquals(25.1, dot.get(2005, 4, 9), 1e-6);
		assertEquals(37.5, dot.get(2008, 1, 29), 1e-6);
		Iterator<ValueOnDay<Double>> iter = dot.iterator();
		assertTrue(iter.hasNext());
		assertEquals(25.1, iter.next().getValue(), 1e-6);
		assertTrue(iter.hasNext());
		assertEquals(37.5, iter.next().getValue(), 1e-6);
		assertFalse(iter.hasNext());
		assertNull(iter.next());
	}

	@Test
	public void testComplex() {
		DoublesOverTime dot = new DoublesOverTime();
		dot.add(2005, 4, 9, 25.1);
		dot.add(2008, 1, 29, 37.5);
		dot.add(2008, 1, 13, 295.251);
		dot.add(2006, 7, 23, 89.2);
		dot.add(2001, 1, 8, DoubleTableImmutable.SUGGESTED_UNSET_VALUE);
		assertEquals(25.1, dot.get(2005, 4, 9), 1e-6);
		assertEquals(37.5, dot.get(2008, 1, 29), 1e-6);
		assertEquals(DoubleTableImmutable.SUGGESTED_UNSET_VALUE, dot.get(2001, 1, 1), 1e-6);
		assertEquals(295.251, dot.get(2008, 1,13), 1e-6);
		assertEquals(DoubleTableImmutable.SUGGESTED_UNSET_VALUE, dot.get(2008, 1,12), 1e-6);
		assertEquals(DoubleTableImmutable.SUGGESTED_UNSET_VALUE, dot.get(2008, 1,14), 1e-6);
		Iterator<ValueOnDay<Double>> iter = dot.iterator();
		assertTrue(iter.hasNext());
		assertEquals(25.1, iter.next().getValue(), 1e-6);
		assertTrue(iter.hasNext());
		assertEquals(89.2, iter.next().getValue(), 1e-6);
		assertTrue(iter.hasNext());
		assertEquals(295.251, iter.next().getValue(), 1e-6);
		assertTrue(iter.hasNext());
		assertEquals(37.5, iter.next().getValue(), 1e-6);
		assertFalse(iter.hasNext());
		assertNull(iter.next());
	}
}