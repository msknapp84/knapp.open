package knapp.common.collections.time;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.Iterator;

import org.junit.Test;


public class DoublesOverTimeImmutableTest {

	@Test
	public void testEmpty() {
		DoublesOverTime dot = new DoublesOverTime();
		DoublesOverTimeImmutable doti = new DoublesOverTimeImmutable(dot);
		assertEquals(DoubleTableImmutable.SUGGESTED_UNSET_VALUE,doti.get(LocalDate.now()),1e-8);
		Iterator<ValueOnDay<Double>> iter = doti.iterator();
		assertFalse(iter.hasNext());
		assertNull(iter.next());
	}
	
	@Test
	public void testOne() {
		DoublesOverTime dot = new DoublesOverTime();
		dot.add(2010, 3, 18, 19.32);
		DoublesOverTimeImmutable doti = new DoublesOverTimeImmutable(dot);
		assertEquals(DoubleTableImmutable.SUGGESTED_UNSET_VALUE,doti.get(LocalDate.now()),1e-8);
		assertEquals(19.32,doti.get(LocalDate.now(),true),1e-8);
		assertEquals(DoubleTableImmutable.SUGGESTED_UNSET_VALUE,doti.get(LocalDate.of(2010,3,17),true),1e-8);
		Iterator<ValueOnDay<Double>> iter = doti.iterator();
		assertTrue(iter.hasNext());
		assertEquals(19.32,iter.next().getValue(),1e-6);
		assertFalse(iter.hasNext());
		assertNull(iter.next());
	}
	
	@Test
	public void testComplex() {
		DoublesOverTime dot = new DoublesOverTime();
		assertEquals(0,dot.getSize());
		dot.add(2010, 3, 18, -19.32);
		assertEquals(1,dot.getSize());
		dot.add(2008, 1, 26, 4);
		assertEquals(2,dot.getSize());
		dot.add(2008, 1, 27, DoubleTableImmutable.SUGGESTED_UNSET_VALUE);
		assertEquals(2,dot.getSize());
		dot.add(2012, 8, 11, 71.25);
		assertEquals(3,dot.getSize());
		dot.add(2012, 5, 15, 61.3);
		assertEquals(4,dot.getSize());
		dot.add(2012, 6, 19, 84.21);
		assertEquals(5,dot.getSize());
		dot.add(2001, 2, 7, 215.3);
		assertEquals(6,dot.getSize());
		dot.add(2009, 7, 2, 925.15);
		assertEquals(7,dot.getSize());
		DoublesOverTimeImmutable doti = new DoublesOverTimeImmutable(dot);
		assertEquals(DoubleTableImmutable.SUGGESTED_UNSET_VALUE,doti.get(LocalDate.of(2000,1,1),true),1e-8);
		assertEquals(71.25,doti.get(LocalDate.now(),true),1e-8);
		assertEquals(DoubleTableImmutable.SUGGESTED_UNSET_VALUE,doti.get(LocalDate.now(),false),1e-8);
		assertEquals(4,doti.get(LocalDate.of(2008,1,28),true),1e-8);
		assertEquals(215.3,doti.get(LocalDate.of(2004,1,1),true),1e-8);
		assertEquals(925.15,doti.get(LocalDate.of(2009,7,2),true),1e-8);
		assertEquals(925.15,doti.get(LocalDate.of(2009,7,2),false),1e-8);
		Iterator<ValueOnDay<Double>> iter = doti.iterator();
		assertTrue(iter.hasNext());
		assertEquals(215.3,iter.next().getValue(),1e-6);
		assertTrue(iter.hasNext());
		assertEquals(4,iter.next().getValue(),1e-6);
		assertTrue(iter.hasNext());
		assertEquals(925.15,iter.next().getValue(),1e-6);
		assertTrue(iter.hasNext());
		assertEquals(-19.32,iter.next().getValue(),1e-6);
		assertTrue(iter.hasNext());
		assertEquals(61.3,iter.next().getValue(),1e-6);
		assertTrue(iter.hasNext());
		assertEquals(84.21,iter.next().getValue(),1e-6);
		assertTrue(iter.hasNext());
		assertEquals(71.25,iter.next().getValue(),1e-6);
		assertFalse(iter.hasNext());
		assertNull(iter.next());
	}
}