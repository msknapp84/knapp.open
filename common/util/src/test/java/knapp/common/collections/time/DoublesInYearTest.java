package knapp.common.collections.time;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import knapp.common.collections.time.DoubleTableImmutable;
import knapp.common.collections.time.DoublesInYear;
import knapp.common.collections.time.ValueOnDay;
import static org.junit.Assert.*;

public class DoublesInYearTest {
	
	@Test
	public void testIt() {
		DoublesInYear diy = new DoublesInYear(1984);
		assertEquals(DoubleTableImmutable.SUGGESTED_UNSET_VALUE,diy.get(1, 1),1e-6);
		assertFalse(diy.iterator().hasNext());
		diy.add(3, 18, 52);
		Iterator<ValueOnDay<Double>> iter = diy.iterator();
		assertTrue(iter.hasNext());
		List<ValueOnDay<Double>> t = new ArrayList<>();
		while (iter.hasNext()) {
			t.add(iter.next());
		}
		assertEquals(52,t.get(0).getValue(),1e-6);
		assertEquals(1,t.size());
		
		diy.add(12, 28, 41.36);
		iter = diy.iterator();
		assertTrue(iter.hasNext());
		t = new ArrayList<>();
		while (iter.hasNext()) {
			t.add(iter.next());
		}
		assertEquals(52,t.get(0).getValue(),1e-6);
		assertEquals(41.36,t.get(1).getValue(),1e-6);
		assertEquals(2,t.size());
		
		assertEquals(41.36,diy.get(12,28),1e-6);
		assertEquals(52,diy.get(3,18),1e-6);
		assertEquals(52,diy.get("1984-03-18"),1e-6);
		assertEquals(41.36,diy.get("1984-12-28"),1e-6);
	}

}
