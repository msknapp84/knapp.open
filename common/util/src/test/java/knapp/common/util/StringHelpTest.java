package knapp.common.util;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import static org.junit.Assert.*;
import static knapp.common.util.StringHelp.*;

public class StringHelpTest {

	@Test
	public void testSplit() {
		List<String> splits = splitConsideringQuotes("foo,bar,zop", ',');
		assertEquals(Arrays.asList("foo","bar","zop"),splits);
		
		splits = splitConsideringQuotes("foo,\"bar\",zop", ',');
		assertEquals(Arrays.asList("foo","\"bar\"","zop"),splits);
		

		splits = splitConsideringQuotes("foo,\"bar,zop\"", ',');
		assertEquals(Arrays.asList("foo","\"bar,zop\""),splits);

		splits = splitConsideringQuotes("foo,\"bar,'zop\"", ',');
		assertEquals(Arrays.asList("foo","\"bar,'zop\""),splits);

		splits = splitConsideringQuotes("foo,'bar\",\"zop',kafa \"b\"", ',');
		assertEquals(Arrays.asList("foo","'bar\",\"zop'","kafa \"b\""),splits);
		
		assertEquals(Arrays.asList(""),splitConsideringQuotes("", ','));
		assertEquals(new ArrayList<String>(),splitConsideringQuotes(null, ','));
	}
	
	@Test
	public void capDecap() {
		assertEquals("Hi",capitalize("hi"));
		assertEquals("hI",unCapitalize("HI"));
		assertEquals("",capitalize(""));
		assertEquals("",unCapitalize(""));
		assertNull(capitalize(null));
		assertNull(unCapitalize(null));
	}
	
	@Test
	public void testDateInversion() {
		testDateInversion(LocalDate.now());
		testDateInversion(LocalDate.of(1970, 1, 1));
		testDateInversion(LocalDate.of(1980, 12, 31));
		testDateInversion(LocalDate.of(2000, 2, 29));
		testDateInversion(LocalDate.of(2015, 6, 21));
	}
	
	public void testDateInversion(LocalDate x) {
		String s = createInverseDateString(x);
		LocalDate recovered = fromInverseDateString(s);
		assertEquals(x,recovered);
	}
	
}
