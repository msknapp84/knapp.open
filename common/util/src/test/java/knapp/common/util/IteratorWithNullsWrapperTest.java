package knapp.common.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import static org.junit.Assert.*;

public class IteratorWithNullsWrapperTest {

	@Test
	public void testWrap_noArg() {
		IteratorWithNullsWrapper<String> wrapper = new IteratorWithNullsWrapper<String>();
		assertFalse(wrapper.hasNext());
		assertNull(wrapper.next());
	}

	@Test
	public void testWrap_nullArg() {
		IteratorWithNullsWrapper<String> wrapper = new IteratorWithNullsWrapper<String>(
				null);
		assertFalse(wrapper.hasNext());
		assertNull(wrapper.next());
	}

	@Test
	public void testWrap_empty() {
		IteratorWithNullsWrapper<String> wrapper = new IteratorWithNullsWrapper<String>(
				Collections.EMPTY_SET.iterator());
		assertFalse(wrapper.hasNext());
		assertNull(wrapper.next());
	}

	@Test
	public void testWrap_one() {
		Set<String> core = new HashSet<>();
		core.add("foo");
		IteratorWithNullsWrapper<String> wrapper = new IteratorWithNullsWrapper<String>(
				core.iterator());
		assertTrue(wrapper.hasNext());
		assertEquals("foo", wrapper.next());
		assertFalse(wrapper.hasNext());
		assertNull(wrapper.next());
	}

	@Test
	public void testWrap_oneNull() {
		Set<String> core = new HashSet<>();
		core.add(null);
		IteratorWithNullsWrapper<String> wrapper = new IteratorWithNullsWrapper<String>(
				core.iterator());
		assertTrue(wrapper.hasNext());
		assertNull(wrapper.next());
		assertFalse(wrapper.hasNext());
		assertNull(wrapper.next());
	}

	@Test
	public void testWrap_complex() {
		List<String> core = new ArrayList<>();
		core.add("foo");
		core.add(null);
		core.add("zop");
		List<String> core2 = new ArrayList<>();
		core2.add(null);
		core2.add("bar");
		core2.add(null);
		core2.add("kogan");
		List<String> core3 = new ArrayList<>();
		core3.add("borg");
		IteratorWithNullsWrapper<String> wrapper = new IteratorWithNullsWrapper<String>(
				core.iterator(), null, core2.iterator(), core3.iterator(), null);
		assertTrue(wrapper.hasNext());
		assertEquals("foo", wrapper.next());
		assertTrue(wrapper.hasNext());
		assertNull(wrapper.next());
		assertTrue(wrapper.hasNext());
		assertEquals("zop", wrapper.next());
		assertTrue(wrapper.hasNext());
		assertNull(wrapper.next());
		assertTrue(wrapper.hasNext());
		assertEquals("bar", wrapper.next());
		assertNull(wrapper.next());
		assertEquals("kogan", wrapper.next());
		assertEquals("borg", wrapper.next());
		assertFalse(wrapper.hasNext());
		assertNull(wrapper.next());
	}
}