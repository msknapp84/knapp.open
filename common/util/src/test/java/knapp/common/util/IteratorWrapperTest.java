package knapp.common.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.Test;

import static org.junit.Assert.*;

public class IteratorWrapperTest {
	
	@Test
	public void testWrap_nothing() {
		IteratorWrapper<String> wrap = new IteratorWrapper<String>();
		assertFalse(wrap.hasNext());
		assertNull(wrap.next());
	}

	@Test
	public void testWrap_empty() {
		Set<String> core = new HashSet<>();
		IteratorWrapper<String> wrap = new IteratorWrapper<String>(core.iterator());
		assertFalse(wrap.hasNext());
		assertNull(wrap.next());
	}

	@Test
	public void testWrap_twoEmpty() {
		Set<String> core = new HashSet<>();
		Set<String> core2 = new HashSet<>();
		IteratorWrapper<String> wrap = new IteratorWrapper<String>(core.iterator(),core2.iterator());
		assertFalse(wrap.hasNext());
		assertNull(wrap.next());
	}
	
	@Test
	public void testWrap_oneIterOneElement() {
		Iterator<String> core = Collections.singleton("fubar").iterator();
		IteratorWrapper<String> wrap = new IteratorWrapper<String>(core);
		assertTrue(wrap.hasNext());
		assertEquals("fubar",wrap.next());
		assertFalse(wrap.hasNext());
		assertNull(wrap.next());
	}
	
	@Test
	public void testWrap_zeroOne() {
		Set<String> core = new HashSet<>();
		Iterator<String> core2 = Collections.singleton("fubar").iterator();
		IteratorWrapper<String> wrap = new IteratorWrapper<String>(core.iterator(),core2);
		assertTrue(wrap.hasNext());
		assertEquals("fubar",wrap.next());
		assertFalse(wrap.hasNext());
		assertNull(wrap.next());
	}
	
	@Test
	public void testWrap_oneZeroOne() {
		Iterator<String> core1 = Collections.singleton("fubar").iterator();
		Iterator<String> core2 = Collections.EMPTY_SET.iterator();
		Iterator<String> core3 = Collections.singleton("snafu").iterator();
		IteratorWrapper<String> wrap = new IteratorWrapper<String>(core1,core2,core3);
		assertTrue(wrap.hasNext());
		assertEquals("fubar",wrap.next());
		assertTrue(wrap.hasNext());
		assertEquals("snafu",wrap.next());
		assertFalse(wrap.hasNext());
		assertNull(wrap.next());
	}
	
	@Test
	public void testWrap_oneNullOne() {
		Iterator<String> core1 = Collections.singleton("fubar").iterator();
		Iterator<String> core3 = Collections.singleton("snafu").iterator();
		IteratorWrapper<String> wrap = new IteratorWrapper<String>(core1,null,core3);
		assertTrue(wrap.hasNext());
		assertEquals("fubar",wrap.next());
		assertTrue(wrap.hasNext());
		assertEquals("snafu",wrap.next());
		assertFalse(wrap.hasNext());
		assertNull(wrap.next());
	}
	
	@Test
	public void testWrap_complex() {
		Iterator<String> core1 = Collections.singleton("fubar").iterator();
		Iterator<String> core2 = Collections.EMPTY_SET.iterator();
		Iterator<String> core3 = Collections.singleton("snafu").iterator();
		IteratorWrapper<String> wrap = new IteratorWrapper<String>(core1,core2,core3,Arrays.asList("foo","bar",null,"zop").iterator());
		assertTrue(wrap.hasNext());
		assertEquals("fubar",wrap.next());
		assertTrue(wrap.hasNext());
		assertEquals("snafu",wrap.next());
		assertTrue(wrap.hasNext());
		assertEquals("foo",wrap.next());
		assertTrue(wrap.hasNext());
		assertEquals("bar",wrap.next());
		assertTrue(wrap.hasNext());
		assertEquals("zop",wrap.next());
		assertFalse(wrap.hasNext());
		assertNull(wrap.next());
	}
}