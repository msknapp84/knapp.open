package knapp.common.util;

import java.time.LocalDate;

import org.junit.Test;

import static org.junit.Assert.*;
import static knapp.common.util.ByteUtils.*;

public class ByteUtilsTest {

	@Test
	public void toFromBytes() {
		assertFalse(bytesToBool(boolToBytes(false)));
		assertTrue(bytesToBool(new byte[]{32}));
		assertTrue(bytesToBool(boolToBytes(true)));
		assertFalse(bytesToBool(new byte[]{-18}));
		assertEquals((short) 92, bytesToShort(shortToBytes((short) 92)));
		assertEquals(42, bytesToInt(intToBytes(42)));
		assertEquals(0, bytesToInt(intToBytes(0)));
		assertEquals(-28945, bytesToInt(intToBytes(-28945)));
		assertEquals(2392534723L, bytesToLong(longToBytes(2392534723L)));
		assertEquals(0, bytesToLong(longToBytes(0)));
		assertEquals(-209843L, bytesToLong(longToBytes(-209843L)));
		assertEquals(3.1415, bytesToFloat(floatToBytes((float) 3.1415)), 1e-6);
		assertEquals(3.1415, bytesToDouble(doubleToBytes(3.1415)), 1e-6);

		LocalDate d = LocalDate.now();
		assertEquals(d, bytesToDate(dateToBytes(d)));
	}

	@Test
	public void fromNull() {
		assertFalse(bytesToBool(null));
		assertEquals(0, bytesToShort(null));
		assertEquals(0, bytesToInt(null));
		assertEquals(0, bytesToLong(null));
		assertEquals(0, bytesToFloat(null), 1e-5);
		assertEquals(0, bytesToDouble(null), 1e-9);
		assertNull(bytesToDate(null));
	}
	
	@Test
	public void increment() {
		// really we should just use Range.prefix instead
		assertEquals("GOOH",incrementLexically("GOOG"));
		assertEquals("B   ",incrementLexically("A~~~"));
	}
	
	@Test
	public void testHashTruncateAndBase64() {
		byte[] x = hashTruncateAndBase64("captain picard",4);
		String y = new String(x);
		assertNotNull(y);
		x = hashTruncateAndBase64(null, 8);
		assertEquals(0,x.length);
	}
}
