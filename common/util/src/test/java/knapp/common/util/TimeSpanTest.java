package knapp.common.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import knapp.common.util.TimeSpan;

import org.junit.Assert;
import org.junit.Test;

public class TimeSpanTest {

	@Test
	public void fromString() {
		TimeSpan ts = TimeSpan.fromThenToEternity("2002-05-25");
		String strt = ts.getStart().format(DateTimeFormatter.ISO_LOCAL_DATE);
		Assert.assertEquals("2002-05-25", strt);
	}

	@Test
	public void testGetPresentTimeSpans_oneDay() {
		List<LocalDate> dates = Arrays.asList(LocalDate.of(2000, 1, 1));
		List<TimeSpan> expected = Arrays.asList(TimeSpan.of("2000-01-01",
				"2000-01-02"));
		List<TimeSpan> actual = TimeSpan.getPresentTimeSpans(dates, 5);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testGetPresentTimeSpans_twoDay() {
		List<LocalDate> dates = Arrays.asList(LocalDate.of(2000, 1, 1),
				LocalDate.of(2000, 10, 1));
		List<TimeSpan> expected = Arrays.asList(
				TimeSpan.of("2000-01-01", "2000-01-02"),
				TimeSpan.of("2000-10-01", "2000-10-02"));
		List<TimeSpan> actual = TimeSpan.getPresentTimeSpans(dates, 5);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testGetPresentTimeSpans() {
		List<LocalDate> dates = Arrays.asList(LocalDate.of(2000, 1, 1),
				LocalDate.of(2000, 1, 2), LocalDate.of(2000, 1, 4),
				LocalDate.of(2000, 1, 9), LocalDate.of(2000, 1, 15),
				LocalDate.of(2000, 1, 16), LocalDate.of(2000, 1, 20));
		List<TimeSpan> expected = Arrays.asList(
				TimeSpan.of("2000-01-01", "2000-01-10"),
				TimeSpan.of("2000-01-15", "2000-01-21"));
		List<TimeSpan> actual = TimeSpan.getPresentTimeSpans(dates, 5);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testGetPresentTimeSpans_empty() {
		List<LocalDate> dates = Collections.EMPTY_LIST;
		List<TimeSpan> expected = Collections.EMPTY_LIST;
		List<TimeSpan> actual = TimeSpan.getPresentTimeSpans(dates, 5);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testGetPresentTimeSpans_null() {
		List<TimeSpan> expected = Collections.EMPTY_LIST;
		List<TimeSpan> actual = TimeSpan.getPresentTimeSpans(null, 5);
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testRemoveFrom_smallGapsIgnored() {
		TimeSpan ts = TimeSpan.of("2000-01-01", "2010-01-01");
		List<TimeSpan> present = Arrays.asList(
				TimeSpan.of("2001-01-01", "2002-01-01"),
				TimeSpan.of("2002-01-05", "2003-01-01"),
				TimeSpan.of("2003-01-06", "2004-01-01"),
				TimeSpan.of("2007-01-01", "2009-01-01"));
		List<TimeSpan> gaps = TimeSpan.removeFrom(present, ts, 5);
		Assert.assertEquals(4, gaps.size());
		Assert.assertEquals(TimeSpan.of("2000-01-01", "2001-01-01"),
				gaps.get(0));
		Assert.assertEquals(TimeSpan.of("2003-01-01", "2003-01-06"),
				gaps.get(1));
		Assert.assertEquals(TimeSpan.of("2004-01-01", "2007-01-01"),
				gaps.get(2));
		Assert.assertEquals(TimeSpan.of("2009-01-01", "2010-01-01"),
				gaps.get(3));
	}

	@Test
	public void testRemoveFrom_gapAtStart() {
		TimeSpan ts = TimeSpan.of("2000-01-01", "2010-01-01");
		List<TimeSpan> present = Arrays.asList(TimeSpan.of("2001-01-01",
				"2009-12-30"));
		List<TimeSpan> gaps = TimeSpan.removeFrom(present, ts, 5);
		Assert.assertEquals(1, gaps.size());
		Assert.assertEquals(TimeSpan.of("2000-01-01", "2001-01-01"),
				gaps.get(0));
	}

	@Test
	public void testRemoveFrom_gapAtEnd() {
		TimeSpan ts = TimeSpan.of("2000-01-01", "2010-01-01");
		List<TimeSpan> present = Arrays.asList(TimeSpan.of("2000-01-04",
				"2008-12-30"));
		List<TimeSpan> gaps = TimeSpan.removeFrom(present, ts, 5);
		Assert.assertEquals(1, gaps.size());
		Assert.assertEquals(TimeSpan.of("2008-12-30", "2010-01-01"),
				gaps.get(0));
	}

	@Test
	public void testRemoveFrom_nonePresent() {
		TimeSpan ts = TimeSpan.of("2000-01-01", "2010-01-01");
		List<TimeSpan> present = Collections.EMPTY_LIST;
		List<TimeSpan> gaps = TimeSpan.removeFrom(present, ts, 5);
		Assert.assertEquals(ts, gaps.get(0));
	}
}