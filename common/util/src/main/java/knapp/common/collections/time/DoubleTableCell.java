package knapp.common.collections.time;

import java.time.LocalDate;
import java.util.Arrays;

import knapp.common.util.ByteUtils;

public class DoubleTableCell implements Comparable<DoubleTableCell> {
	private byte[] date;
	private double value;
	private int label;

	public DoubleTableCell() {
		date=new byte[3];
	}

	public DoubleTableCell(byte[] date) {
		this.date=new byte[3];
		this.setDate(date);
	}

	public DoubleTableCell(String date) {
		this.date=new byte[3];
		this.setDate(LocalDate.parse(date));
	}

	public DoubleTableCell(int year, int month, int day) {
		this.date=new byte[3];
		this.setDate(LocalDate.of(year,month,day));
	}

	public DoubleTableCell(int year, int month, int day,double value,int label) {
		this.date=new byte[3];
		this.setDate(LocalDate.of(year,month,day));
		this.value = value;
		this.label = label;
	}
	
	public DoubleTableCell(int label) {
		this.date=new byte[3];
		this.label = label;
	}
	
	public DoubleTableCell value(double value) {
		this.value = value;
		return this;
	}
	
	public DoubleTableCell label(int label) {
		this.label = label;
		return this;
	}
	
	public DoubleTableCell date(byte[] date) {
		setDate(date);
		return this;
	}
	
	public DoubleTableCell(DoubleTableCell other) {
		date=new byte[3];
		copyIn(other);
	}
	
	public void copyIn(DoubleTableCell other) {
		setDate(other.date);
		this.value = other.value;
		this.label = other.label;
	}
	
	public byte[] getDate() {
		byte[] defensiveCopy = new byte[3];
		System.arraycopy(date, 0, defensiveCopy, 0, 3);
		return defensiveCopy;
	}

	public void setDate(LocalDate date) {
		setDate(ByteUtils.dateToBytes(date));
	}

	public void setDate(String date) {
		setDate(LocalDate.parse(date));
	}

	public void setDate(int year, int month, int day) {
		setDate(LocalDate.of(year, month, day));
	}

	public void setDate(byte[] date) {
		if (!ByteUtils.isValidDate(date)) {
			throw new IllegalArgumentException("The date is not valid.");
		}
		System.arraycopy(date, 0, this.date, 0, 3);
	}

	public LocalDate getLocalDate() {
		return ByteUtils.bytesToDate(date);
	}

	public void setLocalDate(LocalDate date) {
		this.setDate(ByteUtils.dateToBytes(date));
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public int getLabel() {
		return label;
	}

	public void setLabel(int label) {
		this.label = label;
	}

	@Override
	public int compareTo(DoubleTableCell o) {
		if (o == null) {
			return -1;
		}
		if (label == o.label) {
			return ByteUtils.compareAsBinaryDates(this.date, o.date);
		} else {
			return label - o.label;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(date);
		result = prime * result + label;
		long temp;
		temp = Double.doubleToLongBits(value);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DoubleTableCell other = (DoubleTableCell) obj;
		if (!Arrays.equals(date, other.date))
			return false;
		if (label != other.label)
			return false;
		if (Double.doubleToLongBits(value) != Double
				.doubleToLongBits(other.value))
			return false;
		return true;
	}
}