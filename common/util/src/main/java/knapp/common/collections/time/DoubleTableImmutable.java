package knapp.common.collections.time;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Function;

import knapp.common.util.ByteUtils;

public class DoubleTableImmutable implements Iterable<DoubleTableCell> {
	public static final double SUGGESTED_UNSET_VALUE = -497e-6;
	private final byte[][] dates;
	private final int[] labelIds;
	private final double[][] table;
	private final double unsetValue;

	public DoubleTableImmutable(DoubleTable table) {
		this(table, SUGGESTED_UNSET_VALUE);
	}

	public DoubleTableImmutable(DoubleTable table, double unsetValue) {
		this.unsetValue = unsetValue;
		if (table == null || table.getValues()==null || table.getValues().isEmpty()) {
			this.dates = new byte[0][0];
			this.labelIds = new int[0];
			this.table = new double[0][0];
			return;
		}
		SortedSet<LocalDate> dateSet = new TreeSet<>();
		SortedSet<Integer> labels = new TreeSet<>();
		for (LocalDate date : table.getValues().keySet()) {
			dateSet.add(date);
			labels.addAll(table.getValues().get(date).keySet());
		}
		int i = 0;
		dates = new byte[dateSet.size()][3];
		for (LocalDate date : dateSet) {
			dates[i] = ByteUtils.dateToBytes(date);
			i++;
		}
		labelIds = new int[labels.size()];
		i = 0;
		for (Integer label : labels) {
			labelIds[i] = label;
			i++;
		}
		// the dates and the labels are both in order.
		this.table = new double[labelIds.length][dates.length];
		int dateIndex = 0;
		for (LocalDate date : dateSet) {
			int labelIndex = 0;
			for (int label : labelIds) {
				Map<Integer, Double> m = table.getValues().get(date);
				if (m != null && m.containsKey(label)) {
					this.table[labelIndex][dateIndex] = m.get(label);
				} else {
					this.table[labelIndex][dateIndex] = unsetValue;
				}
				labelIndex++;
			}
			dateIndex++;
		}
	}

	public int getLabelsSize() {
		return this.table.length;
	}

	public int getDatesSize() {
		if (this.table.length < 1) {
			return 0;
		}
		return this.table[0].length;
	}

	public int getCellsSize() {
		return this.getLabelsSize() * this.getDatesSize();
	}

	public int getSetCellsSize() {
		int count = 0;
		for (int i = 0; i < this.table.length; i++) {
			for (int j = 0; j < this.table[i].length; j++) {
				if (Math.abs(unsetValue - this.table[i][j]) > 1e-7) {
					count++;
				}
			}
		}
		return count;
	}

	public boolean has(LocalDate date, int label, boolean latest) {
		if (label < 0 || date == null) {
			return false;
		}
		return Math.abs(get(date, label, latest) - unsetValue) > 1e-7;
	}

	public DoubleTableCell[] getValues(int label) {
		int labelIndex = Arrays.binarySearch(labelIds, label);
		if (labelIndex < 0) {
			return null;
		}
		DoubleTableCell[] vods = new DoubleTableCell[this.table[labelIndex].length];
		for (int i = 0; i < this.table[labelIndex].length; i++) {
			vods[i] = new DoubleTableCell().label(label).date(dates[i])
					.value(this.table[labelIndex][i]);
		}
		return vods;
	}

	public double get(String date, int label) {
		return get(LocalDate.parse(date), label, false);
	}

	public double get(String date, int label, boolean latest) {
		return get(LocalDate.parse(date), label, latest);
	}

	public double get(int year, int month, int day, int label) {
		return get(year, month, day, label, false);
	}

	public double get(int year, int month, int day, int label, boolean latest) {
		return get(LocalDate.of(year, month, day), label, latest);
	}

	public double get(LocalDate date, int label) {
		return get(date, label, false);
	}

	public double get(LocalDate date, int label, boolean latest) {
		if (label < 0 || date == null) {
			return unsetValue;
		}
		int dateIndex = getDateIndex(date, latest);
		int labelIndex = Arrays.binarySearch(labelIds, label);
		if (labelIndex < 0 || labelIndex > this.table.length || dateIndex < 0
				|| dateIndex > dates.length) {
			return unsetValue;
		}
		return this.table[labelIndex][dateIndex];
	}

	private int getDateIndex(LocalDate date, boolean latest) {
		byte[] dateBytes = ByteUtils.dateToBytes(date);
		return ByteUtils.seek(dates, dateBytes, latest);
	}

	private int getDateIndex(byte[] dateBytes, boolean latest) {
		return ByteUtils.seek(dates, dateBytes, latest);
	}

	@Override
	public Iterator<DoubleTableCell> iterator() {
		return new DoubleTableCellIterator(this, true);
	}

	private static class DoubleTableCellIterator implements
			Iterator<DoubleTableCell> {
		private final boolean skipUnset;
		private DoubleTableImmutable source;
		private int labelIndex = 0;
		private int dateIndex = 0;
		private boolean hasNext = false;
		private DoubleTableCell toReturn = new DoubleTableCell();
		private DoubleTableCell next = new DoubleTableCell();

		public DoubleTableCellIterator(DoubleTableImmutable source,
				boolean skipUnset) {
			this.source = source;
			this.skipUnset = skipUnset;
			findNext();
		}

		@Override
		public boolean hasNext() {
			return hasNext;
		}

		@Override
		public DoubleTableCell next() {
			if (!hasNext || next == null) {
				return null;
			}
			toReturn.copyIn(next);
			findNext();
			return toReturn;
		}
		
		public void findNext() {
			// at this point, hasNext is somewhat of a default value, 
			// based purely on indexes.  If we don't skip unset values, 
			// then this is right.
			hasNext=dateIndex<source.dates.length || labelIndex<source.labelIds.length;
			// if we already know that there can't be another value,
			// then return early, and let 'next' be cleaned by gc.
			if (!hasNext) {
				next=null;
				return;
			}
			next.setValue(source.unsetValue);
			do {
				next.setLabel(source.labelIds[labelIndex]);
				next.setDate(source.dates[dateIndex]);
				next.setValue(source.table[labelIndex][dateIndex]);
				dateIndex++;
				if (dateIndex == source.dates.length) {
					labelIndex++;
					if (labelIndex < source.labelIds.length) {
						dateIndex = 0;
					} else {
						break;
					}
				}
			} while (!skipUnset || !isSet(next.getValue()));
			if (skipUnset) {
				hasNext = isSet(next.getValue());
			}
		}
		
		private boolean isSet(double value) {
			return Math.abs(value - source.unsetValue) > 1e-7;
		}
	}
	
	private boolean isSet(double value) {
		return Math.abs(value - unsetValue) > 1e-7;
	}

	public LocalDate[] getDates() {
		LocalDate[] ds = new LocalDate[dates.length];
		for (int i = 0; i < dates.length; i++) {
			ds[i] = ByteUtils.bytesToDate(dates[i]);
		}
		return ds;
	}

	public int[] getLabelIds() {
		int[] copy = new int[this.labelIds.length];
		System.arraycopy(labelIds, 0, copy, 0, labelIds.length);
		return copy;
	}

	public String toPrintString() {
		return toPrintString(null);
	}

	public String toPrintString(Function<Integer, String> labelNameResolver) {
		StringBuilder sb = new StringBuilder();
		toPrintString(sb, labelNameResolver);
		return sb.toString();
	}

	public void toPrintString(StringBuilder sb,
			Function<Integer, String> labelNameResolver) {
		boolean first = true;
		sb.append(String.format("%50s  ", " "));
		for (LocalDate ld : getDates()) {
			if (first) {
				first = false;
			} else {
				sb.append("  ");
			}
			sb.append(String.format("%16s", ld.toString()));
		}
		sb.append('\n');
		for (int i = 0; i < labelIds.length; i++) {
			int ordinal = labelIds[i];
			String name = labelNameResolver.apply(ordinal);
			sb.append(String.format("%50s: ", name));
			for (int j = 0; j < dates.length; j++) {
				double v = table[i][j];
				sb.append(String.format("%,16.0f", v)).append("  ");
			}
			sb.append('\n');
		}
	}

	public double getUnsetValue() {
		return unsetValue;
	}
}