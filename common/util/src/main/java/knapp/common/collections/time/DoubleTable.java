package knapp.common.collections.time;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import knapp.common.util.ByteUtils;

/**
 * A mutable builder for a table of doubles.  This is 
 * really just used to build DoubleTableImmutable instances.
 * @author michael
 */
public class DoubleTable {

	private Map<LocalDate,Map<Integer,Double>> values = new HashMap<>();
	
	public DoubleTable() {
		
	}
	
	public DoubleTable add(int year,int month,int day,Integer label,double value) {
		return add(LocalDate.of(year,month,day),label,value);
	}
	
	public DoubleTable add(LocalDate date,Integer label,double value) {
		if (date == null || label == null) {
			throw new IllegalArgumentException("The date cannot be null.");
		}
		Map<Integer,Double> subMap = values.get(date);
		if (subMap == null) {
			subMap = new HashMap<>();
			values.put(date,subMap);
		}
		subMap.put(label,value);
		return this;
	}
	
	public Map<LocalDate,Map<Integer,Double>> getValues() {
		return values;
	}

	public void add(byte[] date, int label, double value) {
		add(ByteUtils.bytesToDate(date),label,value);
	}
}
