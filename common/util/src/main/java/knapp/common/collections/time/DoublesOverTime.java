package knapp.common.collections.time;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Spares you a lot of ram when you have a table of doubles on a date, because
 * it uses double primitives in arrays instead of Lists of Double wrapper
 * objects. So much less garbage collection is needed, and much less ram is
 * consumed.
 * 
 * This is the ideal structure for building the collection, but once it's built
 * you should ideally transfer it to another structure that doesn't use any
 * space for the gaps in time.
 * 
 * @author michael
 */
public class DoublesOverTime implements Iterable<ValueOnDay<Double>> {
	public static short EPOCH_YEAR = 1970;

	private SortedMap<Byte, DoublesInYear> years;
	private double unsetValue;
	private int size = 0;

	public DoublesOverTime() {
		years = new TreeMap<Byte, DoublesInYear>();
		setUnsetValue(DoubleTableImmutable.SUGGESTED_UNSET_VALUE);
	}

	public DoublesOverTime(double unsetValue) {
		years = new TreeMap<Byte, DoublesInYear>();
		this.setUnsetValue(unsetValue);
	}

	public double get(int year, int month, int day) {
		if (year < EPOCH_YEAR) {
			throw new IllegalArgumentException(EPOCH_YEAR
					+ " is the minimum year, this was provided " + year);
		}
		byte index = (byte) (year - EPOCH_YEAR);
		DoublesInYear diy = years.get(index);
		if (diy == null) {
			return getUnsetValue();
		}
		return diy.get(month, day);
	}

	public DoublesOverTime add(LocalDate d, double v) {
		if (d.getYear() < EPOCH_YEAR) {
			throw new IllegalArgumentException(EPOCH_YEAR
					+ " is the minimum year, this was provided " + d.getYear());
		}
		return add(d.getYear(), d.getMonthValue(), d.getDayOfMonth(), v);
	}

	public DoublesOverTime add(int year, int month, int day, double value) {
		set(year, month, day, value);
		return this;
	}
	
	public DoublesOverTime remove(int year, int month, int day, double value) {
		unset(year,month,day,unsetValue);
		return this;
	}
	
	public void unset(int year, int month, int day, double value) {
		set(year,month,day,unsetValue);
	}

	public void set(int year, int month, int day, double value) {
		boolean removal = Math.abs(value-unsetValue)<1e-7;
		byte index = (byte) (year - EPOCH_YEAR);
		DoublesInYear diy = years.get(index);
		if (diy == null && !removal) {
			diy = new DoublesInYear(year, getUnsetValue());
			years.put(index, diy);
			if (years.containsKey((byte)(index - 1))) {
				DoublesInYear p = years.get((byte)(index - 1));
				p.setNextYear(diy);
				diy.setPreviousYear(p);
			}
			if (years.containsKey((byte)(index + 1))) {
				DoublesInYear n = years.get((byte)(index + 1));
				n.setPreviousYear(diy);
				diy.setNextYear(n);
			}
		}
		if (diy!=null) {
			// it would be null if you tried removing a value that does
			// not already exist.
			boolean originallySet = diy.isSet(month, day);
			diy.add(month, day, value);
			if (removal && originallySet) {
				size--;
			} else if (!removal && !originallySet) {
				size++;
			}
		}
	}

	/**
	 * Gets the size, which is quick since it's tracked
	 * with every add or remove operation.
	 * @return
	 */
	public int getSize() {
		return size;
	}
	
	public int quickComputeSize() {
		int sum=0;
		for (DoublesInYear diy : years.values()) {
			sum+=diy.getSize();
		}
		this.size=sum;
		return sum;
	}

	/**
	 * This is the slowest method of getting the size,
	 * it checks each and every day of the year.  This 
	 * should always match getSize or quickComputeSize 
	 * unless there is a bug in the code.
	 * @return
	 */
	public int computeSize() {
		int sum=0;
		for (DoublesInYear diy : years.values()) {
			sum+=diy.computeSize();
		}
		this.size=sum;
		return sum;
	}
	
	public int numberOfYears() {
		return this.years.size();
	}
	
	public int numberOfYearsWithValues() {
		int sum=0;
		for (DoublesInYear diy : years.values()) {
			if (diy.getSize()>0) {
				sum++;
			}
		}
		return sum;
	}
	
	@Override
	public Iterator<ValueOnDay<Double>> iterator() {
		return new DoublesOverTimeIterator(this);
	}

	public double getUnsetValue() {
		return unsetValue;
	}

	public void setUnsetValue(double unsetValue) {
		this.unsetValue = unsetValue;
	}

	private static class DoublesOverTimeIterator implements
			Iterator<ValueOnDay<Double>> {

		private ValueOnDay<Double> toReturn = new ValueOnDay<Double>();
		private ValueOnDay<Double> next = new ValueOnDay<Double>();
		private boolean hasNext = false;
		private DoublesInYear[] years;
		private int yearIndex = 0;
		private Iterator<ValueOnDay<Double>> yearIterator;
		private double unsetValue;
		
		private DoublesOverTimeIterator(DoublesOverTime source) {
			if (source == null) {
				return;
			}
			this.unsetValue = source.getUnsetValue();
			if (!source.years.isEmpty()) {
				years = source.years.values().toArray(new DoublesInYear[source.years.size()]);
				if (years.length>0) {
					yearIterator = years[0].iterator();
				}
				findNext();
			}
		}

		@Override
		public boolean hasNext() {
			return hasNext;
		}
		
		@Override
		public ValueOnDay<Double> next() {
			if (!hasNext) {
				return null;
			}
			toReturn.copyIn(next);
			findNext();
			return toReturn;
		}
		
		private void findNext() {
			hasNext=false;
			if (years==null || years.length<1) {
				return;
			}
			ValueOnDay<Double> x;
			while (!hasNext && yearIndex<years.length) {
				if (yearIterator!=null && yearIterator.hasNext()) {
					x = yearIterator.next();
					if (x!=null && x.getValue()!=null && !x.getValue().equals(unsetValue)) {
						next.copyIn(x);
						hasNext=true;
						break;
					}
				} else {
					DoublesInYear diy = null;
					while (diy==null && yearIndex<years.length-1) {
						yearIndex++;
						diy = years[yearIndex];
					}
					if (diy!=null) {
						yearIterator=diy.iterator();
					} else {
						// there are no more possible values.
						break;
					}
				}
			}
		}
	}
}