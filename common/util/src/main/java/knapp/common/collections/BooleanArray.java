package knapp.common.collections;

public class BooleanArray {
	
	private byte[] bytes;
	private int size;
	
	public BooleanArray(int size) {
		int sizeBytes = (int)Math.round(Math.ceil((double)size/8.0));
		bytes = new byte[sizeBytes];
		this.size = size;
	}
	
	public boolean isTrue(int index) {
		if (index >= size) {
			throw new ArrayIndexOutOfBoundsException();
		}
		int[] b = getByteIndex(index);
		byte v = bytes[b[0]];
		byte shifted = (byte) (v >> (7-b[1]));
		byte rem = (byte) (shifted % 2);
		// for some reason when you shift -128 to the right,
		// java preserves the sign.
		return Math.abs(rem) == 1;
	}
	
	public boolean get(int index) {
		return isTrue(index);
	}
	
	public BooleanArray set(int index,boolean val) {
		if (index >= size) {
			throw new ArrayIndexOutOfBoundsException();
		}
		if (get(index)==val) {
			return this;
		}
		int[] b = getByteIndex(index);
		byte delta = (byte) (1 << (7-b[1]));
		bytes[b[0]] = (byte) (bytes[b[0]] + (val ? 1 : -1)*delta);
		return this;
	}
	
	private int[] getByteIndex(int index) {
		int byteIndex = (int)Math.round(Math.floor((double)index/8.0));
		int offset = index % 8;
		return new int[]{byteIndex,offset};
	}

}
