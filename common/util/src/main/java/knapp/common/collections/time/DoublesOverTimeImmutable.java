package knapp.common.collections.time;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import knapp.common.util.ByteUtils;

public class DoublesOverTimeImmutable implements Iterable<ValueOnDay<Double>> {
	private final double unsetValue;
	private final byte[][] dates;
	private final double[] values;

	public DoublesOverTimeImmutable(DoublesOverTime builder) {
		this.unsetValue = builder.getUnsetValue();
		dates = new byte[builder.getSize()][];
		values = new double[builder.getSize()];
		int i = 0;
		for (ValueOnDay<Double> vod : builder) {
			dates[i] = ByteUtils.dateToBytes(vod.getDate());
			values[i] = vod.getValue();
			i++;
		}
	}

	public int getSizeIncludingUnset() {
		return values.length;
	}

	public int determineSizeWithoutUnset() {
		int count = 0;
		for (int i = 0; i < values.length; i++) {
			if (Math.abs(values[i] - unsetValue) < 1e-7) {
				count++;
			}
		}
		return count;
	}

	public int determineNumberOfYears() {
		Set<Byte> years = new HashSet<Byte>();
		for (int i = 0;i<dates.length;i++) {
			years.add(dates[i][0]);
		}
		return years.size();
	}

	public int determineNumberOfYearsWithoutUnset() {
		Set<Byte> years = new HashSet<Byte>();
		for (int i = 0;i<dates.length;i++) {
			if (Math.abs(values[i] - unsetValue) < 1e-7) {
				years.add(dates[i][0]);
			}
		}
		return years.size();
	}

	@Override
	public Iterator<ValueOnDay<Double>> iterator() {
		return new DOTIIterator(this);
	}

	private static class DOTIIterator implements Iterator<ValueOnDay<Double>> {

		private int index = 0;
		private DoublesOverTimeImmutable source;
		private ValueOnDay<Double> val = new ValueOnDay<Double>();

		private DOTIIterator(DoublesOverTimeImmutable source) {
			this.source = source;
		}

		@Override
		public boolean hasNext() {
			return index < source.dates.length;
		}

		@Override
		public ValueOnDay<Double> next() {
			if (index >= source.dates.length) {
				return null;
			}
			val.setDate(ByteUtils.bytesToDate(source.dates[index]));
			val.setValue(source.values[index]);
			index++;
			return val;
		}
	}

	public double get(String date) {
		return get(date, false);
	}

	public double get(LocalDate date) {
		return get(date, false);
	}

	public double get(String date, boolean latest) {
		return get(LocalDate.parse(date), latest);
	}

	public double get(byte[] date, boolean latest) {
		int i = ByteUtils.seek(dates, date, latest);
		if (i < 0 || i >= this.values.length) {
			return this.unsetValue;
		}
		return this.values[i];
	}

	public double get(LocalDate date, boolean latest) {
		return get(ByteUtils.dateToBytes(date), latest);
	}
}