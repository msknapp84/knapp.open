package knapp.common.collections.time;

import java.time.LocalDate;
import java.util.Iterator;

public class DoublesInYear implements Iterable<ValueOnDay<Double>> {
	private static short YEAR_EPOCH = 1970;
	private final byte yearsAfterEpoch;

	private DoublesInYear previousYear;
	private DoublesInYear nextYear;

	private double[][] values;
	private double unsetValue;
	
	private int size;

	public DoublesInYear(int year) {
		this(year,DoubleTableImmutable.SUGGESTED_UNSET_VALUE);
	}

	public DoublesInYear(int year, double unsetValue) {
		this.yearsAfterEpoch = (byte) (year - YEAR_EPOCH);
		values = new double[12][];
		this.unsetValue = unsetValue;
	}
	
	public DoublesInYear remove(int month, int day) {
		return add(month,day,unsetValue);
	}

	public DoublesInYear add(int month, int day, double v) {
		if (values[month - 1] == null) {
			values[month - 1] = new double[daysInMonth(month, getYear())];
			for (int i = 0; i < values[month - 1].length; i++) {
				values[month - 1][i] = unsetValue;
			}
		}
		boolean previouslySet = isValueSet(values[month - 1][day - 1]);
		values[month - 1][day - 1] = v;
		boolean nowSet = isValueSet(values[month - 1][day - 1]);
		if (!previouslySet && nowSet) {
			size++;
		} else if (previouslySet && !nowSet) {
			size--;
		}
		return this;
	}
	
	public int getSize() {
		return size;
	}

	public double get(int year,int month, int day) {
		if (year == this.getYear()) {
			return get(month,day);
		} else if (year < this.getYear() && previousYear!=null) {
			// find previous year;
			DoublesInYear d = previousYear;
			while (d!=null && d.getYear()>year) {
				d = d.previousYear;
			}
			if (d!=null && d.getYear() == year) {
				return d.get(month, day);
			}
		} else if (year > this.getYear() && nextYear!=null) {
			DoublesInYear d = nextYear;
			while (d!=null && d.getYear()<year) {
				d = d.nextYear;
			}
			if (d!=null && d.getYear() == year) {
				return d.get(month, day);
			}
		}
		return unsetValue;
	}
	
	public double getValueOnDayOfYear(int dayOfYear) {
		int[] md = getMonthAndDay(dayOfYear);
		return get(md[0], md[1]);
	}
	
	/**
	 * Gets the month and day for the day of the year,
	 * which starts with one, not zero.
	 * @param dayOfYear
	 * @return
	 */
	public int[] getMonthAndDay(int dayOfYear) {
		return getMonthAndDay(getYear(),dayOfYear,false);
	}

	public static int[] getMonthAndDay(int year,int dayOfYear,boolean zeroBased) {
		// if it's zero based, just offset the numbers. 
		if (zeroBased) {
			dayOfYear = dayOfYear+1;
		}
		// we are using 1 as the first day of the year now.
		LocalDate d = LocalDate.ofYearDay(year, dayOfYear);
		return new int[]{d.getMonthValue(),d.getDayOfMonth()};
	}
	
	public double get(String date) {
		return get(LocalDate.parse(date));
	}
	
	public double get(LocalDate date) {
		return get(date.getMonthValue(),date.getDayOfMonth());
	}
	
	public double get(int month, int day) {
		if (month<1 || month > 12) {
			throw new IllegalArgumentException("Months start at 1 and end at 12");
		}
		if (day<1 || day > 31) {
			throw new IllegalArgumentException("days start at 1 and end at 31");
		}
		if (values[month - 1] == null) {
			return unsetValue;
		}
		if (values[month-1].length<=day-1) {
			throw new IllegalArgumentException(String.format("The month %d is %d long, "
					+ "and you tried getting day %d",month,values[month-1].length,day));
		}
		return values[month - 1][day - 1];
	}

	public boolean isSet(int month, int day) {
		if (values[month - 1] == null) {
			return false;
		}
		return isValueSet(values[month - 1][day - 1]);
	}
	
	private boolean isValueSet(double value) {
		return (Math.abs(unsetValue - value)>1e-7);
	}
	
	public int computeSize() {
		int sum=0;
		for (int i = 0;i<12;i++) {
			double[] ds = values[i];
			if (ds==null) {
				continue;
			}
			for (double d : ds) {
				if (isValueSet(d)){
					sum++;
				}
			}
		}
		this.size = sum;
		return sum;
	}

	public int getYear() {
		return YEAR_EPOCH + yearsAfterEpoch;
	}

	public boolean leapYear() {
		return isLeapYear(this.getYear());
	}

	public int days() {
		return isLeapYear(getYear()) ? 366 : 365;
	}

	public static int daysInMonth(int month, int year) {
		if (month == 2) {
			return isLeapYear(year) ? 29 : 28;
		}
		boolean even = month % 2 == 0;
		if (month >= 8) {
			return even ? 31 : 30;
		}
		return even ? 30 : 31;
	}

	private static boolean isLeapYear(int year) {
		return year % 4 == 0;
	}

	public Iterator<ValueOnDay<Double>> iteratorWithoutSkipping() {
		return new DoublesInYearIterator(false,this);
	}

	@Override
	public Iterator<ValueOnDay<Double>> iterator() {
		return new DoublesInYearIterator(true,this);
	}

	private static class DoublesInYearIterator implements Iterator<ValueOnDay<Double>> {
		private boolean skipUnset;
		private DoublesInYear source;
		
		private ValueOnDay<Double> current;
		private ValueOnDay<Double> next;
		private int dayOfYear = 1;
		
		private DoublesInYearIterator(boolean skipUnset, DoublesInYear source) {
			this.skipUnset = skipUnset;
			this.source = source;
			current = new ValueOnDay<Double>();
			next = new ValueOnDay<Double>();
			next();
		}

		@Override
		public boolean hasNext() {
			return next != null;
		}

		@Override
		public ValueOnDay<Double> next() {
			ValueOnDay<Double> x = current;
			current = next;
			next = null;
			int max = source.leapYear() ? 367 : 366;
			if (dayOfYear<max) {
				do {
					LocalDate d = LocalDate.ofYearDay(source.getYear(), dayOfYear);
					double v = source.get(d);
					if (!skipUnset || v != source.unsetValue) {
						next = x;
						next.setDate(d);
						next.setValue(v);
					}
					dayOfYear++;
				} while (next==null && dayOfYear<max);
			}
			return current;
		}
	}

	public DoublesInYear getPreviousYear() {
		return previousYear;
	}

	public void setPreviousYear(DoublesInYear previousYear) {
		this.previousYear = previousYear;
	}

	public DoublesInYear getNextYear() {
		return nextYear;
	}
	
	public boolean hasNext() {
		return nextYear!=null;
	}

	public void setNextYear(DoublesInYear nextYear) {
		this.nextYear = nextYear;
	}

	public byte getYearsAfterEpoch() {
		return yearsAfterEpoch;
	}
}