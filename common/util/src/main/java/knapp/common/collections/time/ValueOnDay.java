package knapp.common.collections.time;

import java.time.LocalDate;
import java.util.Arrays;

import knapp.common.util.ByteUtils;

/**
 * Represents a value on a day in time.
 * 
 * @author michael
 *
 * @param <T>
 */
public class ValueOnDay<T> implements Comparable<ValueOnDay<T>>{
	private byte[] date;
	private T value;

	public ValueOnDay() {
		this.date = new byte[3];
	}

	public ValueOnDay(byte[] date) {
		this.date = new byte[3];
		if (!ByteUtils.isValidDate(date)) {
			throw new IllegalArgumentException("The date is not valid");
		}
		setDate(date);
	}

	public ValueOnDay(ValueOnDay<T> other) {
		this.date = new byte[3];
		copyIn(other);
	}

	public ValueOnDay(int year,int month, int day) {
		this.date = new byte[3];
		setDate(LocalDate.of(year,month,day));
	}
	
	public ValueOnDay(LocalDate d) {
		this.date = new byte[3];
		setDate(d);
	}

	public ValueOnDay(LocalDate d, T value) {
		this.date = new byte[3];
		setDate(d);
		this.value = value;
	}

	/**
	 * Copies the values from the other object into 
	 * this objects values.  This is useful if you're
	 * going to re-use this object in order to avoid
	 * creating extra garbage.  Naturally this copies
	 * 
	 * @param other
	 */
	public void copyIn(ValueOnDay<T> other) {
		setDate(other.date);
		this.value = other.getValue();
	}

	public LocalDate getDate() {
		return ByteUtils.bytesToDate(date);
	}

	public void setDate(LocalDate date) {
		setDate(ByteUtils.dateToBytes(date));
	}

	public void setDate(byte[] date) {
		if (!ByteUtils.isValidDate(date)) {
			throw new IllegalArgumentException("The date is not valid");
		}
		System.arraycopy(date, 0, this.date, 0, 3);
	}

	public byte[] getBinaryDate() {
		byte[] defensiveCopy = new byte[3];
		System.arraycopy(this.date, 0, defensiveCopy, 0, 3);
		return defensiveCopy;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	@Override
	public int compareTo(ValueOnDay<T> o) {
		if (o == null) {
			return -1;
		}
		return ByteUtils.compareAsBinaryDates(this.date, o.date);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(date);
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValueOnDay other = (ValueOnDay) obj;
		if (!Arrays.equals(date, other.date))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
}