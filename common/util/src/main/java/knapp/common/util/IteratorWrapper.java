package knapp.common.util;

import java.util.Iterator;

/**
 * Iterates over a possibly complex set of sub iterators,
 * and skips over any null values.  If you only wrap 
 * one iterator with this, then you have effectively 
 * removed all nulls from that iterator.
 * @author michael
 *
 * @param <T>
 */
public class IteratorWrapper<T> implements Iterator<T> {

	private final Iterator<T>[] given;
	private int index = 0;
	private T next = null;
	
	public IteratorWrapper(Iterator<T>... given) {
		this.given = given;
		findNext();
	}

	@Override
	public boolean hasNext() {
		return next!=null;
	}

	@Override
	public T next() {
		T current = next;
		findNext();
		return current;
	}

	private void findNext() {
		next=null;
		if (given==null) {
			return;
		}
		while (next == null && index<given.length) {
			if (given[index]!=null && given[index].hasNext()) {
				next = given[index].next();
			} else {
				index++;
			}
		}
	}
}