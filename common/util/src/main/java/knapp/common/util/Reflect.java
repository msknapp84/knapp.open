package knapp.common.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;

import org.apache.commons.collections4.CollectionUtils;

public final class Reflect {

	private Reflect() {

	}

	public static void invoke(String methodName, Object target)
			throws ReflectionException {
		if (target == null) {
			// be nice, just move on
			return;
		}
		try {
			Method method = getMethod(methodName, target, true);
			method.invoke(target);
		} catch (Exception e) {
			// do nothing.
		}
	}
	
	public static Method getGetterSetterMethod(String fieldName,Class<?> targetClass,boolean getter) {
		if (fieldName==null || targetClass==null) {
			return null;
		}
		String name = determineGetterSetterName(fieldName, targetClass, getter);
		if (name == null) {
			return null;
		}
		return getMethod(name, targetClass, true);
	}
	
	public static Method getSetterMethod(String fieldName,Class<?> targetClass) {
		return getGetterSetterMethod(fieldName, targetClass, false);
	}
	
	public static Method getGetterMethod(String fieldName,Class<?> targetClass) {
		return getGetterSetterMethod(fieldName, targetClass, true);
	}

	public static String determineGetterName(String fieldName, Class<?> targetClass) {
		return determineGetterSetterName(fieldName, targetClass, true);
	}

	public static String determineSetterName(String fieldName, Class<?> targetClass) {
		return determineGetterSetterName(fieldName, targetClass, false);
	}
	
	public static String determineGetterSetterName(String fieldName, Class<?> targetClass,boolean getter) {
		Field field = getField(fieldName, targetClass, true);
		if (field == null) {
			return null;
		}
		Class<?> tp = field.getType();
		if (getter && (boolean.class.equals(tp) || Boolean.class.isAssignableFrom(tp))) {
			return "is"+StringHelp.capitalize(fieldName);
		}
		return (getter ? "get" : "set") +StringHelp.capitalize(fieldName);
	}

	public static Field getField(String fieldName, Class<?> targetClass,
			boolean searchParents) {
		if (targetClass == null) {
			return null;
		}
		Field field = null;
		Class<?> currentClass = targetClass;
		do {
			try {
				field = currentClass.getDeclaredField(fieldName);
			} catch (NoSuchFieldException | SecurityException e) {
			} finally {
				currentClass = getParent(currentClass);
			}
		} while (searchParents && field == null && hasParent(currentClass));
		return field;
	}

	public static boolean hasParent(Class<?> targetClass) {
		return getParent(targetClass) != null;
	}

	public static Class<?> getParent(Class<?> targetClass) {
		if (targetClass == null) {
			return null;
		}
		if (targetClass.equals(Object.class)) {
			return null;
		}
		Class<?> parent = targetClass.getSuperclass();
		if (parent == null || parent.equals(targetClass)) {
			return null;
		}
		return parent;
	}

	public static Method getMethod(String methodName, Object target,
			boolean searchParents) {
		if (target == null) {
			return null;
		}
		return getMethod(methodName, target.getClass(), searchParents);
	}

	public static Method getMethod(String methodName, Class<?> targetClass,
			boolean searchParents) {
		List<Method> methods = getMethods(methodName, targetClass,
				searchParents);
		if (CollectionUtils.isNotEmpty(methods)) {
			return methods.get(0);
		}
		return null;
	}

	public static List<Method> getMethods(String methodName,
			Class<?> targetClass, boolean searchParents) {
		return getMethods(methodName, targetClass, searchParents, null, null);
	}

	public static Method getMethod(String methodName, Class<?> targetClass,
			boolean searchParents, Class<?>[] argumentTypes, Class<?> returnType) {
		List<Method> methods = getMethods(methodName, targetClass,
				searchParents, argumentTypes, returnType);
		if (CollectionUtils.isNotEmpty(methods)) {
			return methods.get(0);
		}
		return null;
	}

	public static List<Method> getMethods(String methodName,
			Class<?> targetClass, boolean searchParents,
			Class<?>[] argumentTypes, Class<?> returnType) {
		if (targetClass == null) {
			return null;
		}
		List<Method> methods = filterMethods(
				targetClass,
				searchParents,
				(method, currentClass) -> {
					if (!methodName.equals(method.getName())) {
						return false;
					}
					if (returnType != null
							&& !method.getReturnType().equals(returnType)) {
						return false;
					}
					if (argumentTypes != null
							&& !Arrays.equals(method.getParameterTypes(),
									argumentTypes)) {
						return false;
					}
					return true;
				});
		return methods;
	}

	public static List<Method> filterMethods(
			Class<?> targetClass, boolean searchParents,
			BiPredicate<Method, Class<?>> predicate) {
		if (targetClass == null) {
			return null;
		}
		List<Method> methods = new ArrayList<>();
		scanMethods(targetClass, searchParents, (method,
				currentClass) -> {
			if (predicate.test(method, currentClass)) {
				methods.add(method);
			}
		});
		return methods;
	}

	public static void scanMethods(Class<?> targetClass,
			boolean searchParents, BiConsumer<Method, Class<?>> consumer) {
		if (targetClass == null) {
			return;
		}
		try {
			Class<?> originalClass = targetClass;
			Class<?> currentClass = originalClass;
			do {
				for (Method method : currentClass.getDeclaredMethods()) {
					consumer.accept(method, currentClass);
				}
				currentClass = getParent(currentClass);
			} while (searchParents && currentClass!=null && hasParent(currentClass));
		} catch (SecurityException e) {
			// do nothing.
		}
	}

	public static List<Field> filterFields(
			Class<?> targetClass, boolean searchParents,
			BiPredicate<Field, Class<?>> predicate) {
		if (targetClass == null) {
			return null;
		}
		List<Field> fields = new ArrayList<>();
		scanFields(targetClass, searchParents, (field,
				currentClass) -> {
			if (predicate.test(field, currentClass)) {
				fields.add(field);
			}
		});
		return fields;
	}

	public static void scanFields(Class<?> targetClass,
			boolean searchParents, BiConsumer<Field, Class<?>> consumer) {
		if (targetClass == null) {
			return;
		}
		try {
			Class<?> originalClass = targetClass;
			Class<?> currentClass = originalClass;
			do {
				try {
					for (Field field : currentClass.getDeclaredFields()) {
						consumer.accept(field, currentClass);
					}
				} catch (Exception e) {
					
				}
				currentClass = getParent(currentClass);
			} while (searchParents && hasParent(originalClass));
		} catch (SecurityException e) {
			// do nothing.
		}
	}
}