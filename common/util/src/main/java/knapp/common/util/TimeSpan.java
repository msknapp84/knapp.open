package knapp.common.util;

/*
 * #%L
 * jinvestor.parent
 * %%
 * Copyright (C) 2014 Michael Scott Knapp
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeSpan implements Comparable<TimeSpan> {
	private static final Logger logger = LoggerFactory
			.getLogger(TimeSpan.class);
	private static final DateTimeFormatter formatter = DateTimeFormatter
			.ofPattern("yyyy-MM-dd");
	private final LocalDate start;
	private final LocalDate end;

	public static TimeSpan fromThenToNow(String start) {
		TimeSpan ts = null;
		try {
			ts = new TimeSpan(LocalDate.parse(start), LocalDate.now());
		} catch (DateTimeParseException e) {
			logger.error("Could not parse the start: " + start, e);
		}
		return ts;
	}

	public static TimeSpan of(String start, String end) {
		TimeSpan ts = null;
		try {
			ts = new TimeSpan(LocalDate.parse(start), LocalDate.parse(end));
		} catch (DateTimeParseException e) {
			logger.error("Could not parse the start or end: " + start + ", "
					+ end, e);
		}
		return ts;
	}

	public static TimeSpan fromThenToEternity(String start) {
		LocalDate end = LocalDate.of(9999, 1, 1);
		TimeSpan ts = null;
		try {
			ts = new TimeSpan(LocalDate.parse(start), end);
		} catch (DateTimeParseException e) {
			logger.error("Could not parse the start: " + start, e);
		}
		return ts;
	}

	public static TimeSpan until(String end) {
		LocalDate start = LocalDate.of(0, 1, 1);
		TimeSpan ts = null;
		try {
			ts = new TimeSpan(start, LocalDate.parse(end));
		} catch (DateTimeParseException e) {
			logger.error("Could not parse the end: " + end, e);
		}
		return ts;
	}

	public static TimeSpan fromThenToNow(LocalDate start) {
		return new TimeSpan(start, LocalDate.now());
	}

	public static TimeSpan fromThenToEternity(LocalDate start) {
		LocalDate end = LocalDate.of(9999, 1, 1);
		return new TimeSpan(start, end);
	}

	public static TimeSpan until(LocalDate end) {
		LocalDate start = LocalDate.of(0, 1, 1);
		return new TimeSpan(start, end);
	}

	public TimeSpan(final LocalDate start, final LocalDate end) {
		this.start = start;
		this.end = end;
		if (this.end.isBefore(this.start)) {
			throw new IllegalArgumentException(
					"start time must be before end time.");
		}
	}

	public LocalDate getStart() {
		return start;
	}

	public LocalDate getEnd() {
		return end;
	}

	public String toString() {
		return String.format("%s to %s", start.format(formatter),
				end.format(formatter));
	}

	public static LocalDate getStart(TimeSpan ts) {
		return ts == null ? null : ts.getStart();
	}

	public static LocalDate getEnd(TimeSpan ts) {
		return ts == null ? null : ts.getEnd();
	}

	/**
	 * Takes the given list of present timespans, and produces a list of absent
	 * timespans.
	 * 
	 * @param timeSpansPresent
	 * @param within
	 * @return
	 */
	public static List<TimeSpan> removeFrom(List<TimeSpan> timeSpansPresent,
			TimeSpan within, int thresholdDays) {
		if (timeSpansPresent == null || timeSpansPresent.isEmpty()) {
			return Collections.singletonList(within);
		}
		List<TimeSpan> missing = new ArrayList<>();
		if (within.getStart().plus(thresholdDays, ChronoUnit.DAYS)
				.isBefore(timeSpansPresent.get(0).getStart())) {
			missing.add(new TimeSpan(within.getStart(), timeSpansPresent.get(0)
					.getStart()));
		}
		for (int i = 0; i < timeSpansPresent.size() - 1; i++) {
			TimeSpan left = timeSpansPresent.get(i);
			TimeSpan right = timeSpansPresent.get(i + 1);
			long gap = ChronoUnit.DAYS.between(left.getEnd(), right.getStart());
			// since the end day is exclusive, I add one to the gap
			if (gap+1>thresholdDays) {
				missing.add(new TimeSpan(left.getEnd(), right.getStart()));
			}
		}

		if (within
				.getEnd()
				.minus(thresholdDays, ChronoUnit.DAYS)
				.isAfter(
						timeSpansPresent.get(timeSpansPresent.size() - 1)
								.getEnd())) {
			missing.add(new TimeSpan(timeSpansPresent.get(
					timeSpansPresent.size() - 1).getEnd(), within.getEnd()));
		}
		return missing;
	}
	


	public static List<TimeSpan> getPresentTimeSpans(List<LocalDate> dates,
			int thresholdDays) {
		if (CollectionUtils.isEmpty(dates)) {
			return Collections.EMPTY_LIST;
		}
		Collections.sort(dates);
		List<TimeSpan> present = new ArrayList<TimeSpan>();
		LocalDate start = null;
		LocalDate last = null;
		for (int i = 0;i<dates.size();i++) {
			LocalDate current = dates.get(i);
			if (start==null) {
				start = current;
			}
			if (last!=null) {
				long gap = ChronoUnit.DAYS.between(last, current);
				if (gap<= thresholdDays) {
					// the timespan continues
				} else {
					// there is a gap

					// since the end is present, and the timespan convention 
					// is to have the stop date be exclusive, I add one to the 
					// end date.
					TimeSpan ts = new TimeSpan(start,last.plus(1,ChronoUnit.DAYS));
					present.add(ts);
					start=current;
				}
			}
			last = current;
		}
		// the last timespan may not have been added
		if (start!=null) {
			TimeSpan ts = new TimeSpan(start,last.plus(1,ChronoUnit.DAYS));
			present.add(ts);
		}
		return present;
	}

	@Override
	public int compareTo(TimeSpan o) {
		return start.compareTo(o.start);
	}

	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof TimeSpan)) {
			return false;
		}
		TimeSpan ts = (TimeSpan) o;
		return this.start.equals(ts.start) && this.end.equals(ts.end);
	}

	public int hashCode() {
		return new HashCodeBuilder().append(this.start).append(this.end)
				.toHashCode();
	}
}