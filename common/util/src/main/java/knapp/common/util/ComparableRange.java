package knapp.common.util;

public class ComparableRange<T extends Comparable<T>> {
	private final T start;
	private final T end;
	
	public ComparableRange(T start,T end) {
		this.start = start;
		this.end = end;
	}

	public T getEnd() {
		return end;
	}

	public T getStart() {
		return start;
	}
}
