package knapp.common.util;

import java.util.Iterator;

/**
 * Wraps multiple other iterators and preserves nulls.
 * @author michael
 *
 * @param <T>
 */
public class IteratorWithNullsWrapper <T> implements Iterator<T> {

	private final Iterator<T>[] given;
	private int index = 0;
	private T next;
	private boolean hasNext;
	
	public IteratorWithNullsWrapper(Iterator<T>... given) {
		this.given = given;
		findNext();
	}

	@Override
	public boolean hasNext() {
		return hasNext;
	}

	@Override
	public T next() {
		T current = next;
		findNext();
		return current;
	}

	private void findNext() {
		hasNext=false;
		next=null;
		if (given==null) {
			return;
		}
		while (!hasNext && index<given.length) {
			if (given[index]!=null && given[index].hasNext()) {
				next = given[index].next();
				hasNext=true;
			} else {
				index++;
			}
		}
	}
}