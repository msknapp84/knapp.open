package knapp.common.util;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

/**
 * This is primarily written so it can be used
 * as a key in a map.  Unfortunately in java, two
 * byte arrays with the same length and values are
 * different things.  Their '.equals' methods, and 
 * ==, and their '.hashCode' methods will return 
 * different things.  Consequently, they can't be
 * used as a key in a map.  So I have invented
 * the ByteArray.  Unlike a string, this does not
 * require that the value is readable.
 * @author michael
 *
 */
public class ByteArray implements Serializable, CharSequence {
	private static final long serialVersionUID = 7959699226163426888L;
	
	private final byte[] core;
	
	public ByteArray(byte[] c) {
		if (c == null) {
			this.core = new byte[0];
		} else {
			core = new byte[c.length];
			System.arraycopy(c, 0, core, 0, c.length);
		}
	}
	
	public ByteArray(String id) {
		this(id==null ? null : id.getBytes());
	}

	public ByteArray(ByteArray other) {
		this(other.getBytes());
	}
	
	public byte[] getBytes() {
		// we want this to be immutable, so make a 
		// defensive copy.
		byte[] defensiveCopy = new byte[core.length];
		System.arraycopy(core, 0, defensiveCopy, 0, core.length);
		return defensiveCopy;
	}
	
	public String toString() {
		return new String(core);
	}
	
	public int toInt() {
		return ByteUtils.bytesToInt(core);
	}
	
	public float toFloat() {
		return ByteUtils.bytesToFloat(core);
	}
	
	public double toDouble() {
		return ByteUtils.bytesToDouble(core);
	}
	
	public long toLong() {
		return ByteUtils.bytesToLong(core);
	}
	
	public boolean toBoolean() {
		return ByteUtils.bytesToBool(core);
	}
	
	public String toBase64() {
		return Base64.getEncoder().encodeToString(core);
	}
	
	public ByteArray toMd5() {
		ByteArray n = null;
		try {
			n = new ByteArray(java.security.MessageDigest.getInstance("MD5").digest(core));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return n;
	}
	
	public String toMd5Hex() {
		return ByteUtils.toMd5Hex(core);
	}
	
	public String toHex() {
		return ByteUtils.toHex(core);
	}
	
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		}
		if (!(o instanceof ByteArray)) {
			return false;
		}
		ByteArray ba = (ByteArray)o;
		return Arrays.equals(core, ba.core);
	}
	
	public int hashCode() {
		return Arrays.hashCode(core);
	}

	@Override
	public int length() {
		return core.length;
	}

	@Override
	public char charAt(int index) {
		return (char)core[index];
	}

	@Override
	public CharSequence subSequence(int start, int end) {
		byte[] c = new byte[end-start];
		System.arraycopy(core, start, c, 0, start-end);
		return new ByteArray(c);
	}

}
