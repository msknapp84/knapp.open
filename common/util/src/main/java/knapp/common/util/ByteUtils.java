package knapp.common.util;

import java.lang.reflect.Constructor;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class ByteUtils {
//	private static char firstPrintableAsciiChar = ' '; // corresponds to 32, the
//														// space character
//	private static char lastPrintableAsciiChar = '~'; // corresponds to 126, 127
//														// id the DEL character

	private ByteUtils() {
		// private constructor to avoid instantiation.
	}

	/**
	 * This increments the last character
	 * 
	 * @param s
	 * @return
	 */
	public static String incrementLexically(String s) {
		return new String(incrementLexically(s.getBytes()));
	}

	public static byte[] incrementBitwise(byte[] s) {
		return increment(s, false);
	}

	public static byte[] incrementLexically(byte[] s) {
		return increment(s, true);
	}

	public static byte[] increment(byte[] s, boolean lexically) {
		byte[] copy = new byte[s.length];
		System.arraycopy(s, 0, copy, 0, s.length);
		byte min = lexically ? 32 : Byte.MIN_VALUE;
		byte max = lexically ? 126 : Byte.MAX_VALUE;
		boolean worked = false;
		for (int index = copy.length - 1; index > -1; index--) {
			byte b = copy[index];
			if (b < max) {
				copy[index] = (byte) (b + 1);
				worked = true;
				break;
			} else {
				// it is the max, bytes to the left will also be
				// incremented
				copy[index] = min;
			}
		}
		if (!worked) {
			// this resorts to an alternative method.
			// this should be rare. It would only happen
			// with all max chars, like all tildas.
			copy = new byte[s.length + 1];
			System.arraycopy(s, 0, copy, 0, s.length);
			copy[copy.length - 1] = 127;
		}
		return copy;
	}

	public static void addBytes(List<Byte> bytes, byte[] toAdd) {
		for (byte b : toAdd) {
			bytes.add(b);
		}
	}

	public static String toIndex8(String input) {
		// a truncation of 6 will become 8 characters after
		// base 64'ing it.
		return new String(hashTruncateAndBase64(input, 6));
	}

	public static String toIndex4(String input) {
		return hashTruncateAndBase64(input);
	}

	public static String hashTruncateAndBase64(String input) {
		// a default truncation of 3 will yield 4 partitions for the
		// base64, which in turn becomes a 4 character string.
		return new String(hashTruncateAndBase64(input, 3));
	}

	public static byte[] hashTruncateAndBase64(String input, int length) {
		if (input == null) {
			return new byte[0];
		}
		byte[] res = null;
		try {
			byte[] src = MessageDigest.getInstance("MD5").digest(
					input.getBytes());
			// only keep a few bytes. We may be downloading from a
			// few million different urls, 4 bytes gives me 4 billion
			// possible values, with 1 million possible urls the odds of
			// a collission are 1 in 4000.
			byte[] truncated = new byte[4];
			System.arraycopy(src, 0, truncated, 0, 4);

			// this guarantees that it will be readable:
			res = Base64.getEncoder().encode(truncated);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return res;
	}

	public static byte[] longToBytes(long v) {
		return ByteBuffer.allocate(Long.BYTES).putLong(v).array();
	}

	public static byte[] floatToBytes(float v) {
		return ByteBuffer.allocate(Float.BYTES).putFloat(v).array();
	}

	public static long bytesToLong(byte[] v) {
		return v == null ? 0 : ByteBuffer.wrap(v).getLong();
	}

	public static short bytesToShort(byte[] v) {
		return v == null ? 0 : ByteBuffer.wrap(v).getShort();
	}

	public static LocalDate bytesToDate(byte[] bs) {
		if (bs == null) {
			return null;
		}
		if (bs.length != 3) {
			throw new IllegalArgumentException(
					"The byte array must be three bytes long.");
		}
		if (bs[0] == 0 && bs[1] == 0 && bs[2] == 0) {
			return null;
		}
		int year = 1970 + bs[0];
		int month = bs[1];
		int day = bs[2];
		return LocalDate.of(year, month, day);
	}

	public static byte[] dateToBytes(LocalDate date) {
		if (date == null) {
			return new byte[] { 0, 0, 0 };
		}
		byte y = (byte) (date.getYear() - 1970);
		byte m = (byte) date.getMonthValue();
		byte d = (byte) date.getDayOfMonth();
		return new byte[] { y, m, d };
	}

	public static LocalDateTime bytesToDateTime(byte[] val) {
		if (val == null) {
			return null;
		}
		if (val.length != 6) {
			throw new IllegalArgumentException(
					"The byte array must be 6 bytes long.");
		}
		boolean allZeros = true;
		for (int i = 0; i < 6; i++) {
			if (val[i] != 0) {
				allZeros = false;
				break;
			}
		}
		if (allZeros) {
			return null;
		}
		int year = 1970 + val[0];
		int month = val[1];
		int day = val[2];
		int hour = val[3];
		int minute = val[4];
		int second = val[5];
		return LocalDateTime.of(year, month, day, hour, minute, second);
	}

	public static byte[] dateTimeToBytes(LocalDateTime date) {
		if (date == null) {
			return new byte[] { 0, 0, 0, 0, 0, 0 };
		}
		byte y = (byte) (date.getYear() - 1970);
		byte m = (byte) date.getMonthValue();
		byte d = (byte) date.getDayOfMonth();
		byte h = (byte) date.getHour();
		byte mn = (byte) date.getMinute();
		byte s = (byte) date.getSecond();
		return new byte[] { y, m, d, h, mn, s };
	}

	public static byte[] boolToBytes(boolean v) {
		return new byte[] { v ? (byte) 1 : (byte) 0 };
	}

	public static boolean bytesToBool(byte[] v) {
		return v == null ? false : v[0] > (byte) 0;
	}

	public static byte[] doubleToBytes(double v) {
		return ByteBuffer.allocate(Double.BYTES).putDouble(v).array();
	}

	public static byte[] shortToBytes(short v) {
		return ByteBuffer.allocate(Short.BYTES).putShort(v).array();
	}

	public static byte[] intToBytes(int v) {
		return ByteBuffer.allocate(Integer.BYTES).putInt(v).array();
	}

	public static int bytesToInt(byte[] v) {
		return v == null ? 0 : ByteBuffer.wrap(v).getInt();
	}

	public static float bytesToFloat(byte[] val) {
		return val == null ? 0 : ByteBuffer.wrap(val).getFloat();
	}

	public static double bytesToDouble(byte[] val) {
		return val == null ? 0 : ByteBuffer.wrap(val).getDouble();
	}

	public static String toHex(byte[] bytes) {
		StringBuilder sb = new StringBuilder();
		for (byte b : bytes) {
			sb.append(String.format("%02x", b));
		}
		return sb.toString();
	}

	public static byte[] toMd5(byte[] bytes) {
		byte[] md5 = null;
		try {
			md5 = java.security.MessageDigest.getInstance("MD5").digest(bytes);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return md5;
	}

	public static String toMd5Hex(byte[] bytes) {
		return toHex(toMd5(bytes));
	}

	public static byte[] objectToBytes(Object o) {
		if (o == null) {
			return null;
		}
		if (Boolean.class.isAssignableFrom(o.getClass())) {
			return boolToBytes((Boolean) o);
		}
		if (Byte.class.isAssignableFrom(o.getClass())) {
			return new byte[]{(byte)o};
		}
		if (Short.class.isAssignableFrom(o.getClass())) {
			return shortToBytes((Short) o);
		}
		if (Integer.class.isAssignableFrom(o.getClass())) {
			return intToBytes((Integer) o);
		}
		if (Long.class.isAssignableFrom(o.getClass())) {
			return longToBytes((Long) o);
		}
		if (Float.class.isAssignableFrom(o.getClass())) {
			return floatToBytes((Float) o);
		}
		if (Double.class.isAssignableFrom(o.getClass())) {
			return doubleToBytes((Double) o);
		}
		if (ByteArray.class.isAssignableFrom(o.getClass())) {
			return ((ByteArray)o).getBytes();
		}
		if (TimeSpan.class.isAssignableFrom(o.getClass())) {
			return timeSpanToBytes((TimeSpan)o);
		}
		if (LocalDate.class.isAssignableFrom(o.getClass())) {
			return dateToBytes((LocalDate)o);
		}
		if (LocalDateTime.class.isAssignableFrom(o.getClass())) {
			return dateTimeToBytes((LocalDateTime)o);
		}
		if (Calendar.class.isAssignableFrom(o.getClass())) {
			long t = ((Calendar)o).getTimeInMillis();
			return longToBytes(t);
		}
		if (Date.class.isAssignableFrom(o.getClass())) {
			long t = ((Date)o).getTime();
			return longToBytes(t);
		}
		if (URI.class.isAssignableFrom(o.getClass())) {
			return ((URI)o).toString().getBytes();
		}
		
		if (!(o instanceof String)) {
			System.out.println("Investigate casting this to bytes: "+o.getClass());
		}
		// last ditch effort
		String s = o.toString();
		return s.getBytes();
	}

	@SuppressWarnings("unchecked")
	public static <T> T bytesToObject(byte[] bytes,Class<T> type) {
		if (bytes == null) {
			return null;
		}
		if (Boolean.class.isAssignableFrom(type) || boolean.class.isAssignableFrom(type)) {
			return (T) (Boolean)bytesToBool(bytes);
		}
		if (Byte.class.isAssignableFrom(type) || byte.class.isAssignableFrom(type)) {
			return (T) (Byte)bytes[0];
		}
		if (Short.class.isAssignableFrom(type) || short.class.isAssignableFrom(type)) {
			return (T) (Short)bytesToShort(bytes);
		}
		if (Integer.class.isAssignableFrom(type) || int.class.isAssignableFrom(type)) {
			return (T) (Integer)bytesToInt(bytes);
		}
		if (Long.class.isAssignableFrom(type) || long.class.isAssignableFrom(type)) {
			return (T) (Long)bytesToLong(bytes);
		}
		if (Float.class.isAssignableFrom(type) || float.class.isAssignableFrom(type)) {
			return (T) (Float)bytesToFloat(bytes);
		}
		if (Double.class.isAssignableFrom(type) || double.class.isAssignableFrom(type)) {
			return (T) (Double)bytesToDouble(bytes);
		}
		if (ByteArray.class.isAssignableFrom(type)) {
			return (T) new ByteArray(bytes);
		}
		if (LocalDate.class.isAssignableFrom(type)) {
			return (T) (LocalDate)bytesToDate(bytes);
		}
		if (LocalDateTime.class.isAssignableFrom(type)) {
			return (T) (LocalDateTime)bytesToDateTime(bytes);
		}
		if (TimeSpan.class.isAssignableFrom(type)) {
			return (T) (TimeSpan)bytesToTimeSpan(bytes);
		}
		if (Calendar.class.isAssignableFrom(type)) {
			long t = bytesToLong(bytes);
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(t);
			return (T) c;
		}
		if (Date.class.isAssignableFrom(type)) {
			long t = bytesToLong(bytes);
			Date d = new Date(t);
			return (T) d;
		}
		if (String.class.isAssignableFrom(type)) {
			return (T) new String(bytes);
		}
		String s = new String(bytes);
		if (URI.class.isAssignableFrom(type)) {
			try {
				return (T) new URI(s);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		if (!type.equals(String.class)) {
			System.out.println("Investigate casting bytes to this: "+type);
			Constructor<T> c;
			try {
				c = type.getConstructor(new Class<?>[]{String.class});
				T t = c.newInstance(s);
				return t;
			} catch (Exception e) {
			}
			try {
				T t = type.newInstance();
//				Reflect.invoke("setValue", t, s);
				return t;
			} catch (Exception e) {
			}
		}
		return (T)s;
	}

	public static int seek(byte[][] binaryDates, byte[] date, boolean latest) {
		LocalDateByteComparator c = new LocalDateByteComparator();
		int index = Arrays.binarySearch(binaryDates, date, c);
		if (index < 0 && latest) {
			// the user allows returning the latest value before that date.

			// from the Arrays.binarySearch javadocs:
			// if it's not present, this returns (-(insertion point) - 1). The
			// insertion point is
			// defined as the point at which the key would be inserted into the
			// array: the index of the first element greater than the key, or
			// a.length if all elements in the array are less than the specified
			// key.
			int insertionPoint = -(1 + index);
			// since we want the latest at that time, we must subtract one.
			index = insertionPoint - 1;
		}
		return index;
	}

	public static class LocalDateByteComparator implements Comparator<byte[]> {

		@Override
		public int compare(byte[] o1, byte[] o2) {
			return compareAsBinaryDates(o1, o2);
		}
	}
	
	public static int compareAsBinaryDates(byte[] o1, byte[] o2) {
		if (o1 == null) {
			return o2==null ? 0 : 1;
		} else if (o2 == null) {
			return -1;
		}
		for (int i = 0; i < 3; i++) {
			int delta = o1[i] - o2[i];
			if (delta != 0) {
				return delta;
			}
		}
		return 0;
	}
	
	public static boolean isValidDate(byte[] date) {
		if (date.length!=3) {
			return false;
		}
		if (date[0]<0) {
			return false;
		}
		if (date[1]<1 || date[1]>12) {
			return false;
		}
		if (date[2]<1 || date[2]>31) {
			return false;
		}
		return true;
	}
	
	public static byte[] timeSpanToBytes(TimeSpan ts) {
		byte[] b1 = ByteUtils.dateToBytes(ts.getStart());
		byte[] b2 = ByteUtils.dateToBytes(ts.getEnd());
		byte[] b = new byte[b1.length + b2.length];
		System.arraycopy(b1, 0, b, 0, b1.length);
		System.arraycopy(b2, 0, b, b1.length, b2.length);
		return b;
	}

	public static TimeSpan bytesToTimeSpan(byte[] b) {
		byte[] b1 = new byte[b.length / 2];
		byte[] b2 = new byte[b.length / 2];
		System.arraycopy(b, 0, b1, 0, b1.length);
		System.arraycopy(b, b1.length, b2, 0, b2.length);
		LocalDate start = ByteUtils.bytesToDate(b1);
		LocalDate end = ByteUtils.bytesToDate(b2);
		return new TimeSpan(start, end);
	}
}
