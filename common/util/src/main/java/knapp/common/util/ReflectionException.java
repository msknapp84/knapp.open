package knapp.common.util;

public class ReflectionException extends RuntimeException {
	private static final long serialVersionUID = -5794134385330612809L;

	private String methodName;
	private String fieldName;
	private Object target;
	
	public ReflectionException(String message,Throwable cause) {
		super(message,cause);
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Object getTarget() {
		return target;
	}

	public void setTarget(Object target) {
		this.target = target;
	}
	
	
}
