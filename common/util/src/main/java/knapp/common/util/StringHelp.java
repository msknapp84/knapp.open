package knapp.common.util;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringHelp {
	
	private StringHelp() {
		
	}
	
	public static String createInverseDateString(LocalDate date) {
		if (date == null) {
			// months start at 1
			date = LocalDate.of(1970, 1, 1);
		}
		int y = 10000-date.getYear();
		int m = 12-date.getMonthValue();
		int d = 31-date.getDayOfMonth();
		return String.format("%4d%02d%02d",y,m,d);
	}
	
	public static LocalDate fromInverseDateString(String inverseDateString) {
		int y = 10000-Integer.parseInt(inverseDateString.substring(0,4));
		int m = 12-Integer.parseInt(inverseDateString.substring(4,6).trim());
		int d = 31-Integer.parseInt(inverseDateString.substring(6,8).trim());
		return LocalDate.of(y, m, d);
	}

	public static List<String> splitConsideringQuotes(String input,char delimiter) {
		if (input == null) {
			return Collections.EMPTY_LIST;
		}
		List<String> res = new ArrayList<>();
		int start = 0;
		Character quoteChar = null;
		for (int i = 0;i<input.length();i++) {
			char c = input.charAt(i);
			
			if (c == '\'') {
				if (quoteChar==null) {
					quoteChar = c;
				} else if (quoteChar.equals(c)) {
					quoteChar=null;
				}
			} else if (c == '"') {
				if (quoteChar==null) {
					quoteChar = c;
				} else if (quoteChar.equals(c)){
					quoteChar=null;
				}
			} else if (c == delimiter && quoteChar==null) {
				// not in quotes, and found a delimiter
				String x = input.substring(start,i);
				res.add(x);
				start=i+1;
			}
		}
		String x = input.substring(start,input.length());
		res.add(x);
		return res;
	}
	
	public static String capitalize(String s) {
		if (s == null || s.isEmpty()){
			return s;
		}
		return Character.toUpperCase(s.charAt(0)) + s.substring(1);
	}
	
	public static String unCapitalize(String s) {
		if (s == null || s.isEmpty()){
			return s;
		}
		return Character.toLowerCase(s.charAt(0)) + s.substring(1);
	}
}
