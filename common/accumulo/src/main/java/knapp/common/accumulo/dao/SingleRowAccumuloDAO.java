package knapp.common.accumulo.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import knapp.common.data.domain.Entity;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;

import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Value;

import static knapp.common.accumulo.dao.AccUtil.*;

public abstract class SingleRowAccumuloDAO<T extends Entity> extends BaseAccumuloDAO<T> {

	private Collection<MutationMapping<T>> mutationMappings;
	
	public SingleRowAccumuloDAO(Connector connector) {
		super(connector);
	}
	
	protected Collection<MutationMapping<T>> lazilyGetMutationMappings() {
		if (mutationMappings==null) {
			mutationMappings = getMutationMappings();
		}
		return mutationMappings;
	}

	@Override
	public Iterable<Mutation> toMutations(T entity) {
		List<Mutation> mutations = new ArrayList<>();
		Mutation mutation = new Mutation(entity.getId());
		for (MutationMapping<T> mutationMapping : lazilyGetMutationMappings()) {
			mutationMapping.set(mutation, entity);
		}
		List<Mutation> extra = customMutations(entity,mutation);
		if (extra!=null) {
			mutations.addAll(extra);
		}
		return Collections.singleton(mutation);
	}

	protected List<Mutation> customMutations(T entity,Mutation currentMutation) {
		// here so children could potentially add some extra mutations 
		// for an entity.
		return null;
	}

	@Override
	public Map<ByteArray, T> toEntities(Iterable<Entry<Key, Value>> scan)
			throws DataException {
		Map<ByteArray,T> entities = new HashMap<>();
		for (Entry<Key,Value> kv : scan) {
			ByteArray ticker = new ByteArray(kv.getKey().getRow().getBytes());
			T entity = entities.get(ticker);
			if (entity == null) {
				try {
					entity = getEntityClass().newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					e.printStackTrace();
				}
				entities.put(ticker,entity);
			}
			if (entity == null) {
				continue;
			}
			entity.setId(ticker);
			for (MutationMapping<T> mutationMapping : lazilyGetMutationMappings()) {
				if (isCFQ(kv,mutationMapping.getFamily(),mutationMapping.getQualifier())) {
					mutationMapping.set(entity, kv.getValue().get());
				}
			}
			customToEntity(ticker,entity,kv);
		}
		return entities;
	}
	
	protected void customToEntity(ByteArray ticker, T entity, Entry<Key, Value> kv) {
		// here for children to do any extra processing.
	}

	public abstract Collection<MutationMapping<T>> getMutationMappings();
	public abstract Class<T> getEntityClass();
}
