package knapp.common.accumulo.dao;

import knapp.common.data.domain.Entity;

import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.data.Mutation;

/**
 * Base class for accumulo DAOs that could have multiple
 * rows in a table for a single entity.
 * @author michael
 *
 * @param <T>
 * @param <R> an object that represents the row, it should be easily mutated so reflection is possible.
 */
public abstract class MultiRowAccumuloDAO<T extends Entity,R> extends BaseAccumuloDAO<T> {

	public MultiRowAccumuloDAO(Connector connector) {
		super(connector);
	}

	@Override
	public abstract String getTable();

	@Override
	public abstract Iterable<Mutation> toMutations(T entity);
}