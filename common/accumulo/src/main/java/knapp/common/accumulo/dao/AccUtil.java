package knapp.common.accumulo.dao;

import static knapp.common.util.ByteUtils.boolToBytes;
import static knapp.common.util.ByteUtils.bytesToBool;
import static knapp.common.util.ByteUtils.bytesToInt;
import static knapp.common.util.ByteUtils.bytesToLong;
import static knapp.common.util.ByteUtils.intToBytes;
import static knapp.common.util.ByteUtils.longToBytes;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;

import knapp.common.util.ByteUtils;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Value;
import org.apache.hadoop.hdfs.util.ByteArray;

public final class AccUtil {
	private static final AccumuloHelper DEFAULT_HELPER = new AccumuloHelper();

	private AccUtil() {

	}

	public static Value boolToValue(boolean v) {
		return new Value(boolToBytes(v));
	}

	public static boolean valueToBool(Value v) {
		return bytesToBool(v.get());
	}

	public static Value longToValue(Long v) {
		return new Value(longToBytes(v));
	}

	public static Long valueToLong(Value v) {
		return bytesToLong(v.get());
	}

	public static Value calendarToValue(Calendar c) {
		return new Value(longToBytes(c.getTimeInMillis()));
	}

	public static Calendar valueToCalendar(Value v) {
		long lv = valueToLong(v);
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(lv);
		return c;
	}

	public static Value intToValue(int v) {
		return new Value(intToBytes(v));
	}

	public static int valueToInt(Value v) {
		return bytesToInt(v.get());
	}

	public static boolean isCQ(Entry<Key, Value> keyValue, String qualifier) {
		return qualifier.equals(keyValue.getKey().getColumnQualifier()
				.toString());
	}

	public static boolean isCFQ(Entry<Key, Value> keyValue, String family,
			String qualifier) {
		return qualifier.equals(keyValue.getKey().getColumnQualifier()
				.toString())
				&& family
						.equals(keyValue.getKey().getColumnFamily().toString());
	}

	public static boolean isCQ(Key key, String qualifier) {
		return qualifier.equals(key.getColumnQualifier().toString());
	}

	public static boolean isCFQ(Key key, String family, String qualifier) {
		return qualifier.equals(key.getColumnQualifier().toString())
				&& family.equals(key.getColumnFamily().toString());
	}

	public static void put(Mutation mutation, String family, String qualifier,
			byte[] bytes) {
		if (bytes==null) {
			return;
		}
		mutation.put(family.getBytes(), qualifier.getBytes(), bytes);
	}

	public static void put(Mutation mutation, String family, String qualifier,
			boolean bool) {
		mutation.put(family.getBytes(), qualifier.getBytes(), boolToBytes(bool));
	}

	public static void put(Mutation mutation, String family, String qualifier,
			byte num) {
		mutation.put(family.getBytes(), qualifier.getBytes(),
				new byte[] { num });
	}

	public static void put(Mutation mutation, String family, String qualifier,
			short num) {
		mutation.put(family.getBytes(), qualifier.getBytes(),
				ByteUtils.shortToBytes(num));
	}

	public static void put(Mutation mutation, String family, String qualifier,
			int num) {
		mutation.put(family.getBytes(), qualifier.getBytes(),
				ByteUtils.intToBytes(num));
	}

	public static void put(Mutation mutation, String family, String qualifier,
			long num) {
		mutation.put(family.getBytes(), qualifier.getBytes(),
				ByteUtils.longToBytes(num));
	}

	public static void put(Mutation mutation, String family, String qualifier,
			double num) {
		mutation.put(family.getBytes(), qualifier.getBytes(),
				ByteUtils.doubleToBytes(num));
	}

	public static void put(Mutation mutation, String family, String qualifier,
			float num) {
		mutation.put(family.getBytes(), qualifier.getBytes(),
				ByteUtils.floatToBytes(num));
	}

	public static void put(Mutation mutation, String family, String qualifier,
			LocalDate date) {
		if (date == null) {
			return;
		}
		mutation.put(family.getBytes(), qualifier.getBytes(),
				ByteUtils.dateToBytes(date));
	}

	public static void put(Mutation mutation, String family, String qualifier,
			String s) {
		if (s == null) {
			return;
		}
		mutation.put(family, qualifier, s);
	}

	public static void put(Mutation mutation, String family, String qualifier,
			ByteArray ba) {
		if (ba == null) {
			return;
		}
		mutation.put(family.getBytes(), qualifier.getBytes(), ba.getBytes());
	}

	public static void setIfBool(String qualifier, Entry<Key, Value> keyValue,
			Consumer<Boolean> consumer) {
		if (isCQ(keyValue, qualifier)) {
			consumer.accept(ByteUtils.bytesToBool(keyValue.getValue().get()));
		}
	}

	public static void setIfByte(String qualifier, Entry<Key, Value> keyValue,
			Consumer<Byte> consumer) {
		if (isCQ(keyValue, qualifier)) {
			consumer.accept(keyValue.getValue().get()[0]);
		}
	}

	public static void setIfShort(String qualifier, Entry<Key, Value> keyValue,
			Consumer<Short> consumer) {
		if (isCQ(keyValue, qualifier)) {
			consumer.accept(ByteUtils.bytesToShort(keyValue.getValue().get()));
		}
	}

	public static void setIfInt(String qualifier, Entry<Key, Value> keyValue,
			Consumer<Integer> consumer) {
		if (isCQ(keyValue, qualifier)) {
			consumer.accept(ByteUtils.bytesToInt(keyValue.getValue().get()));
		}
	}

	public static void setIfLong(String qualifier, Entry<Key, Value> keyValue,
			Consumer<Long> consumer) {
		if (isCQ(keyValue, qualifier)) {
			consumer.accept(ByteUtils.bytesToLong(keyValue.getValue().get()));
		}
	}

	public static void setIfFloat(String qualifier, Entry<Key, Value> keyValue,
			Consumer<Float> consumer) {
		if (isCQ(keyValue, qualifier)) {
			consumer.accept(ByteUtils.bytesToFloat(keyValue.getValue().get()));
		}
	}

	public static void setIfDouble(String qualifier,
			Entry<Key, Value> keyValue, Consumer<Double> consumer) {
		if (isCQ(keyValue, qualifier)) {
			consumer.accept(ByteUtils.bytesToDouble(keyValue.getValue().get()));
		}
	}

	public static void setIfDate(String qualifier, Entry<Key, Value> keyValue,
			Consumer<LocalDate> consumer) {
		if (isCQ(keyValue, qualifier)) {
			consumer.accept(ByteUtils.bytesToDate(keyValue.getValue().get()));
		}
	}

	public static void setIfBytes(String qualifier, Entry<Key, Value> keyValue,
			Consumer<byte[]> consumer) {
		if (isCQ(keyValue, qualifier)) {
			consumer.accept(keyValue.getValue().get());
		}
	}

	public static void setIfBool(String family, String qualifier,
			Entry<Key, Value> keyValue, Consumer<Boolean> consumer) {
		if (isCFQ(keyValue, family, qualifier)) {
			consumer.accept(ByteUtils.bytesToBool(keyValue.getValue().get()));
		}
	}

	public static void setIfByte(String family, String qualifier,
			Entry<Key, Value> keyValue, Consumer<Byte> consumer) {
		if (isCFQ(keyValue, family, qualifier)) {
			consumer.accept(keyValue.getValue().get()[0]);
		}
	}

	public static void setIfShort(String family, String qualifier,
			Entry<Key, Value> keyValue, Consumer<Short> consumer) {
		if (isCFQ(keyValue, family, qualifier)) {
			consumer.accept(ByteUtils.bytesToShort(keyValue.getValue().get()));
		}
	}

	public static void setIfInt(String family, String qualifier,
			Entry<Key, Value> keyValue, Consumer<Integer> consumer) {
		if (isCFQ(keyValue, family, qualifier)) {
			consumer.accept(ByteUtils.bytesToInt(keyValue.getValue().get()));
		}
	}

	public static void setIfLong(String family, String qualifier,
			Entry<Key, Value> keyValue, Consumer<Long> consumer) {
		if (isCFQ(keyValue, family, qualifier)) {
			consumer.accept(ByteUtils.bytesToLong(keyValue.getValue().get()));
		}
	}

	public static void setIfFloat(String family, String qualifier,
			Entry<Key, Value> keyValue, Consumer<Float> consumer) {
		if (isCFQ(keyValue, family, qualifier)) {
			consumer.accept(ByteUtils.bytesToFloat(keyValue.getValue().get()));
		}
	}

	public static void setIfDouble(String family, String qualifier,
			Entry<Key, Value> keyValue, Consumer<Double> consumer) {
		if (isCFQ(keyValue, family, qualifier)) {
			consumer.accept(ByteUtils.bytesToDouble(keyValue.getValue().get()));
		}
	}

	public static void setIfDate(String family, String qualifier,
			Entry<Key, Value> keyValue, Consumer<LocalDate> consumer) {
		if (isCFQ(keyValue, family, qualifier)) {
			consumer.accept(ByteUtils.bytesToDate(keyValue.getValue().get()));
		}
	}

	public static void setIfBytes(String family, String qualifier,
			Entry<Key, Value> keyValue, Consumer<byte[]> consumer) {
		if (isCFQ(keyValue, family, qualifier)) {
			consumer.accept(keyValue.getValue().get());
		}
	}

	public static void setIfBool(String qualifier, Map<String, Value> row,
			Consumer<Boolean> consumer) {
		DEFAULT_HELPER.setIfBool(qualifier, row, consumer);
	}

	public static void setIfByte(String qualifier, Map<String, Value> row,
			Consumer<Byte> consumer) {
		DEFAULT_HELPER.setIfByte(qualifier, row, consumer);
	}

	public static void setIfShort(String qualifier, Map<String, Value> row,
			Consumer<Short> consumer) {
		DEFAULT_HELPER.setIfShort(qualifier, row, consumer);
	}

	public static void setIfInt(String qualifier, Map<String, Value> row,
			Consumer<Integer> consumer) {
		DEFAULT_HELPER.setIfInt(qualifier, row, consumer);
	}

	public static void setIfLong(String qualifier, Map<String, Value> row,
			Consumer<Long> consumer) {
		DEFAULT_HELPER.setIfLong(qualifier, row, consumer);
	}

	public static void setIfFloat(String qualifier, Map<String, Value> row,
			Consumer<Float> consumer) {
		DEFAULT_HELPER.setIfFloat(qualifier, row, consumer);
	}

	public static void setIfDouble(String qualifier, Map<String, Value> row,
			Consumer<Double> consumer) {
		DEFAULT_HELPER.setIfDouble(qualifier, row, consumer);
	}

	public static void setIfDate(String qualifier, Map<String, Value> row,
			Consumer<LocalDate> consumer) {
		DEFAULT_HELPER.setIfDate(qualifier, row, consumer);
	}

	public static void setIfBytes(String qualifier, Map<String, Value> row,
			Consumer<byte[]> consumer) {
		DEFAULT_HELPER.setIfBytes(qualifier, row, consumer);
	}

	public static void setIfString(String qualifier, Map<String, Value> row,
			Consumer<String> consumer) {
		DEFAULT_HELPER.setIfString(qualifier, row, consumer);
	}

	public static void setIfBool(String family, String qualifier,
			Map<String, Value> row, Consumer<Boolean> consumer) {
		DEFAULT_HELPER.setIfBool(family, qualifier, row, consumer);
	}

	public static void setIfByte(String family, String qualifier,
			Map<String, Value> row, Consumer<Byte> consumer) {
		DEFAULT_HELPER.setIfByte(family, qualifier, row, consumer);
	}

	public static void setIfShort(String family, String qualifier,
			Map<String, Value> row, Consumer<Short> consumer) {
		DEFAULT_HELPER.setIfShort(family, qualifier, row, consumer);
	}

	public static void setIfInt(String family, String qualifier,
			Map<String, Value> row, Consumer<Integer> consumer) {
		DEFAULT_HELPER.setIfInt(family, qualifier, row, consumer);
	}

	public static void setIfLong(String family, String qualifier,
			Map<String, Value> row, Consumer<Long> consumer) {
		DEFAULT_HELPER.setIfLong(family, qualifier, row, consumer);
	}

	public static void setIfFloat(String family, String qualifier,
			Map<String, Value> row, Consumer<Float> consumer) {
		DEFAULT_HELPER.setIfFloat(family, qualifier, row, consumer);
	}

	public static void setIfDouble(String family, String qualifier,
			Map<String, Value> row, Consumer<Double> consumer) {
		DEFAULT_HELPER.setIfDouble(family, qualifier, row, consumer);
	}

	public static void setIfDate(String family, String qualifier,
			Map<String, Value> row, Consumer<LocalDate> consumer) {
		DEFAULT_HELPER.setIfDate(family, qualifier, row, consumer);
	}

	public static void setIfBytes(String family, String qualifier,
			Map<String, Value> row, Consumer<byte[]> consumer) {
		DEFAULT_HELPER.setIfBytes(family, qualifier, row, consumer);
	}

	public static void setIfString(String family, String qualifier,
			Map<String, Value> row, Consumer<String> consumer) {
		DEFAULT_HELPER.setIfString(family, qualifier, row, consumer);
	}
	
	public static void setIfByteArray(String family, String qualifier,
			Map<String, Value> row, Consumer<ByteArray> consumer) {
		DEFAULT_HELPER.setIfByteArray(family, qualifier, row, consumer);
	}

	public static class AccumuloHelper {
		private String defaultFamily = "f";
		private String cfqDelimiterInMaps = ".";

		public void setIfBool(String qualifier, Map<String, Value> row,
				Consumer<Boolean> consumer) {
			setIfBool(defaultFamily, qualifier, row, consumer);
		}

		public void setIfByte(String qualifier, Map<String, Value> row,
				Consumer<Byte> consumer) {
			setIfByte(defaultFamily, qualifier, row, consumer);
		}

		public void setIfShort(String qualifier, Map<String, Value> row,
				Consumer<Short> consumer) {
			setIfShort(defaultFamily, qualifier, row, consumer);
		}

		public void setIfInt(String qualifier, Map<String, Value> row,
				Consumer<Integer> consumer) {
			setIfInt(defaultFamily, qualifier, row, consumer);
		}

		public void setIfLong(String qualifier, Map<String, Value> row,
				Consumer<Long> consumer) {
			setIfLong(defaultFamily, qualifier, row, consumer);
		}

		public void setIfFloat(String qualifier, Map<String, Value> row,
				Consumer<Float> consumer) {
			setIfFloat(defaultFamily, qualifier, row, consumer);
		}

		public void setIfDouble(String qualifier, Map<String, Value> row,
				Consumer<Double> consumer) {
			setIfDouble(defaultFamily, qualifier, row, consumer);
		}

		public void setIfDate(String qualifier, Map<String, Value> row,
				Consumer<LocalDate> consumer) {
			setIfDate(defaultFamily, qualifier, row, consumer);
		}

		public void setIfBytes(String qualifier, Map<String, Value> row,
				Consumer<byte[]> consumer) {
			setIfBytes(defaultFamily, qualifier, row, consumer);
		}

		public void setIfString(String qualifier, Map<String, Value> row,
				Consumer<String> consumer) {
			setIfString(defaultFamily, qualifier, row, consumer);
		}

		public void setIfByteArray(String qualifier, Map<String, Value> row,
				Consumer<ByteArray> consumer) {
			setIfByteArray(defaultFamily, qualifier, row, consumer);
		}

		public void setIfBool(String family, String qualifier,
				Map<String, Value> row, Consumer<Boolean> consumer) {
			String key = String.format("%s%s%s", family, cfqDelimiterInMaps,
					qualifier);
			if (row.containsKey(key)) {
				consumer.accept(ByteUtils.bytesToBool(row.get(key).get()));
			}
		}

		public void setIfByte(String family, String qualifier,
				Map<String, Value> row, Consumer<Byte> consumer) {
			String key = String.format("%s%s%s", family, cfqDelimiterInMaps,
					qualifier);
			if (row.containsKey(key)) {
				consumer.accept(row.get(key).get()[0]);
			}
		}

		public void setIfShort(String family, String qualifier,
				Map<String, Value> row, Consumer<Short> consumer) {
			String key = String.format("%s%s%s", family, cfqDelimiterInMaps,
					qualifier);
			if (row.containsKey(key)) {
				consumer.accept(ByteUtils.bytesToShort(row.get(key).get()));
			}
		}

		public void setIfInt(String family, String qualifier,
				Map<String, Value> row, Consumer<Integer> consumer) {
			String key = String.format("%s%s%s", family, cfqDelimiterInMaps,
					qualifier);
			if (row.containsKey(key)) {
				consumer.accept(ByteUtils.bytesToInt(row.get(key).get()));
			}
		}

		public void setIfLong(String family, String qualifier,
				Map<String, Value> row, Consumer<Long> consumer) {
			String key = String.format("%s%s%s", family, cfqDelimiterInMaps,
					qualifier);
			if (row.containsKey(key)) {
				consumer.accept(ByteUtils.bytesToLong(row.get(key).get()));
			}
		}

		public void setIfFloat(String family, String qualifier,
				Map<String, Value> row, Consumer<Float> consumer) {
			String key = String.format("%s%s%s", family, cfqDelimiterInMaps,
					qualifier);
			if (row.containsKey(key)) {
				consumer.accept(ByteUtils.bytesToFloat(row.get(key).get()));
			}
		}

		public void setIfDouble(String family, String qualifier,
				Map<String, Value> row, Consumer<Double> consumer) {
			String key = String.format("%s%s%s", family, cfqDelimiterInMaps,
					qualifier);
			if (row.containsKey(key)) {
				consumer.accept(ByteUtils.bytesToDouble(row.get(key).get()));
			}
		}

		public void setIfDate(String family, String qualifier,
				Map<String, Value> row, Consumer<LocalDate> consumer) {
			String key = String.format("%s%s%s", family, cfqDelimiterInMaps,
					qualifier);
			if (row.containsKey(key)) {
				consumer.accept(ByteUtils.bytesToDate(row.get(key).get()));
			}
		}

		public void setIfBytes(String family, String qualifier,
				Map<String, Value> row, Consumer<byte[]> consumer) {
			String key = String.format("%s%s%s", family, cfqDelimiterInMaps,
					qualifier);
			if (row.containsKey(key)) {
				consumer.accept(row.get(key).get());
			}
		}

		public void setIfString(String family, String qualifier,
				Map<String, Value> row, Consumer<String> consumer) {
			String key = String.format("%s%s%s", family, cfqDelimiterInMaps,
					qualifier);
			if (row.containsKey(key)) {
				consumer.accept(new String(row.get(key).get()));
			}
		}

		public void setIfByteArray(String family, String qualifier,
				Map<String, Value> row, Consumer<ByteArray> consumer) {
			String key = String.format("%s%s%s", family, cfqDelimiterInMaps,
					qualifier);
			if (row.containsKey(key)) {
				consumer.accept(new ByteArray(row.get(key).get()));
			}
		}

		public String getDefaultFamily() {
			return defaultFamily;
		}

		public void setDefaultFamily(String defaultFamily) {
			this.defaultFamily = defaultFamily;
		}

		public String getCfqDelimiterInMaps() {
			return cfqDelimiterInMaps;
		}

		public void setCfqDelimiterInMaps(String cfqDelimiterInMaps) {
			this.cfqDelimiterInMaps = cfqDelimiterInMaps;
		}
	}
}