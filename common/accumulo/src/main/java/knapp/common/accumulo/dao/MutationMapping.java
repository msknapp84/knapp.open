package knapp.common.accumulo.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import knapp.common.util.ByteUtils;
import knapp.common.util.Reflect;

import org.apache.accumulo.core.data.Mutation;

public class MutationMapping<T> {
	private String family, qualifier, fieldName;
	private Class<T> entityType;
	private Method setter;
	private Method getter;

	public MutationMapping() {

	}

	public MutationMapping(String family, String qualifier,String fieldName) {
		this.family = family;
		this.qualifier = qualifier;
		this.fieldName = fieldName;
	}
	
	public MutationMapping<T> finish() {
		// validate.
		getter = Reflect.getGetterMethod(fieldName, entityType);
		setter = Reflect.getSetterMethod(fieldName, entityType);
		if (getter == null) {
			throw new IllegalArgumentException("We could not find the getter for the field named: "+fieldName);
		}
		if (setter == null) {
			throw new IllegalArgumentException("We could not find the setter for the field named: "+fieldName);
		}
		return this;
	}

	public byte[] get(T entity) {
		Object value = null;
		try {
			value = getter.invoke(entity);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
		return ByteUtils.objectToBytes(value);
	}

	public void set(T entity, byte[] value) {
		Class<?> type = setter.getParameterTypes()[0];
		Object x = ByteUtils.bytesToObject(value, type);
		try {
			setter.invoke(entity, x);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	public void set(Mutation mutation, T entity) {
		byte[] x = get(entity);
		AccUtil.put(mutation,family,qualifier,x);
	}

	public MutationMapping<T> family(String f) {
		this.family = f;
		return this;
	}

	public MutationMapping<T> qualifier(String f) {
		this.qualifier = f;
		return this;
	}

	public MutationMapping<T> fieldName(String f) {
		this.fieldName = f;
		return this;
	}

	public MutationMapping<T> entityType(Class<T> entityType) {
		this.entityType = entityType;
		return this;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public String getQualifier() {
		return qualifier;
	}

	public void setQualifier(String qualifier) {
		this.qualifier = qualifier;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Class<T> getEntityType() {
		return entityType;
	}

	public void setEntityType(Class<T> entityType) {
		this.entityType = entityType;
	}
	
	public static class MutationMappingBuilder<T> {
		private Class<T> type;
		private String family;
		
		private List<MutationMapping<T>> built = new ArrayList<>();
		
		public MutationMappingBuilder(Class<T> type,String family) {
			this.type = type;
			this.family = family;
		}
		
		public MutationMappingBuilder<T> add(String family, String qualifier,String fieldName) {
			MutationMapping<T> m = new MutationMapping<T>(family,qualifier,fieldName).entityType(type).finish();
			built.add(m);
			return this;
		}
		
		public MutationMappingBuilder<T> add(String qualifier,String fieldName) {
			return add(family,qualifier,fieldName);
		}
		
		public List<MutationMapping<T>> getList() {
			return built;
		}

		public Class<T> getType() {
			return type;
		}

		public void setType(Class<T> type) {
			this.type = type;
		}

		public String getFamily() {
			return family;
		}

		public void setFamily(String family) {
			this.family = family;
		}
	}
}