package knapp.common.accumulo.dao;

import static knapp.common.accumulo.dao.AccUtil.valueToInt;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;

import knapp.common.data.dao.DAO;
import knapp.common.data.dao.DAOSkeleton;
import knapp.common.data.domain.Entity;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;
import knapp.open.iterators.CountIterator;

import org.apache.accumulo.core.client.BatchDeleter;
import org.apache.accumulo.core.client.BatchScanner;
import org.apache.accumulo.core.client.BatchWriter;
import org.apache.accumulo.core.client.BatchWriterConfig;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.IteratorSetting;
import org.apache.accumulo.core.client.MutationsRejectedException;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.client.TableNotFoundException;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.security.Authorizations;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.hadoop.io.Text;

public abstract class BaseAccumuloDAO<T extends Entity> extends DAOSkeleton<T>
		implements DAO<T> {
	public static final long TENMEGIBYTES = (long) 1e7;
	public static final BatchWriterConfig batchWriterConfig;
	public static final Authorizations PUBLIC_AUTHORIZATIONS;
	public static final int THREADS = 2;
	
	private String namespace;

	private Connector connector;

	static {
		batchWriterConfig = new BatchWriterConfig();
		batchWriterConfig.setMaxMemory(TENMEGIBYTES);
		PUBLIC_AUTHORIZATIONS = new Authorizations("public");
	}

	public Map<ByteArray, T> load(String from, String to,
			Consumer<BatchScanner> scannerCallback) throws DataException {
		Range range = new Range(from, to);
		return getByRanges(Collections.singleton(range), scannerCallback);
	}

	public T getOneByRange(Range range) throws DataException {
		return getOneByRange(range, null);
	}

	public Map<ByteArray, T> getByRange(Range range) throws DataException {
		return getByRange(range, null);
	}

	public Map<ByteArray, T> getByRanges(Collection<Range> ranges)
			throws DataException {
		return getByRanges(ranges, null);
	}

	public T getOneByRange(Range range, Consumer<BatchScanner> scannerCallback)
			throws DataException {
		Map<ByteArray, T> results = loadRanges(Collections.singleton(range),
				scannerCallback);
		return MapUtils.isEmpty(results) ? null : results.values().iterator()
				.next();
	}

	public Map<ByteArray, T> getByRange(Range range,
			Consumer<BatchScanner> scannerCallback) throws DataException {
		return loadRanges(Collections.singleton(range), scannerCallback);
	}

	public Map<ByteArray, T> getByRanges(Collection<Range> ranges,
			Consumer<BatchScanner> scannerCallback) throws DataException {
		return loadRanges(ranges, scannerCallback);
	}

	public T load(String id) throws DataException {
		return load(id, null);
	}

	public T load(String id, Consumer<BatchScanner> scannerCallback)
			throws DataException {
		Map<ByteArray, T> res = load(id, id, scannerCallback);
		return res.get(id);
	}

	public Map<ByteArray, T> loadRanges(Collection<Range> ranges,
			Consumer<BatchScanner> scannerCallback) throws DataException {
		Map<ByteArray, T> res = Collections.EMPTY_MAP;
		BatchScanner scan = null;
		try {
			scan = getConnector()
					.createBatchScanner(getNameSpaceAndTable(), PUBLIC_AUTHORIZATIONS, THREADS);
			scan.setRanges(ranges);
			if (scannerCallback != null) {
				scannerCallback.accept(scan);
			}
			res = toEntities(scan);
		} catch (TableNotFoundException e) {
			wrapException(null, getNameSpaceAndTable(), null, e);
		} finally {
			if (scan != null) {
				scan.close();
			}
		}
		return res;
	}
	
	public void scanRanges(Collection<Range> ranges,
			Consumer<BatchScanner> scannerCallback) throws DataException {
		BatchScanner scan = null;
		try {
			scan = getConnector()
					.createBatchScanner(getNameSpaceAndTable(), PUBLIC_AUTHORIZATIONS, THREADS);
			scan.setRanges(ranges);
			if (scannerCallback != null) {
				scannerCallback.accept(scan);
			}
		} catch (TableNotFoundException e) {
			wrapException(null, getNameSpaceAndTable(), null, e);
		} finally {
			if (scan != null) {
				scan.close();
			}
		}
	}

	public void delete(String id) throws DataException {
		delete(id, id);
	}

	@Override
	public void delete(Collection<ByteArray> ids) throws DataException {
		delete(ids,false,null);
	}

	public void delete(Collection<ByteArray> ids,boolean exact,Consumer<BatchDeleter> deleterConsumer) throws DataException {
		Collection<Range> ranges = new HashSet<>();
		ids.forEach(lx -> ranges.add(exact?Range.exact(lx):Range.prefix(lx)));
		deleteRanges(ranges,deleterConsumer);
	}

	public void deleteRanges(Collection<Range> ranges) throws DataException {
		deleteRanges(ranges,null);
	}
	
	public void deleteRanges(Collection<Range> ranges,Consumer<BatchDeleter> deleterConsumer) throws DataException {
		BatchDeleter deleter = null;
		try {
			deleter = getConnector().createBatchDeleter(getNameSpaceAndTable(), PUBLIC_AUTHORIZATIONS,
					THREADS, batchWriterConfig);
			deleter.setRanges(ranges);
			if (deleterConsumer!=null) {
				deleterConsumer.accept(deleter);
			}
			deleter.delete();
		} catch (TableNotFoundException | MutationsRejectedException e) {
			wrapException("deleting ids", getNameSpaceAndTable(), null, e);
		} finally {
			if (deleter != null) {
				deleter.close();
			}
		}
	}

	public void delete(String from, String to) throws DataException {
		if (from == null) {
			return;
		}
		if (to == null) {
			to = from;
		}
		BatchDeleter deleter = null;
		try {
			deleter = getConnector().createBatchDeleter(getNameSpaceAndTable(), PUBLIC_AUTHORIZATIONS,
					THREADS, batchWriterConfig);
			deleter.setRanges(Collections.singletonList(new Range(from, to)));
		} catch (TableNotFoundException e) {
			wrapException(String.format("%s to %s", from, to), getNameSpaceAndTable(),
					null, e);
		} finally {
			if (deleter != null) {
				deleter.close();
			}
		}
	}

	public void save(Collection<T> entities) throws DataException {
		if (entities == null) {
			return;
		}
		BatchWriter writer = null;
		T violating = null;
		try {
			writer = getConnector().createBatchWriter(getNameSpaceAndTable(),
					batchWriterConfig);
			for (T entity : entities) {
				if (entity == null) {
					continue;
				}

				// keep track of current entity so we know which one broke it.
				violating = entity;
				entity.validate();
				addMutations(entity, writer);
			}
		} catch (MutationsRejectedException | TableNotFoundException e) {
			wrapException(null, getNameSpaceAndTable(), violating, e);
		} finally {
			quietlyCloseWriter(writer, violating);
		}
	}

	public void addMutations(T entity, BatchWriter writer)
			throws MutationsRejectedException {
		Iterable<Mutation> mutations = toMutations(entity);
		writer.addMutations(mutations);
	}

	private void quietlyCloseWriter(BatchWriter writer, T violating)
			throws DataException {
		if (writer != null) {
			try {
				writer.close();
			} catch (MutationsRejectedException e) {
				wrapException(null, getNameSpaceAndTable(), violating, e);
			}
		}
	}
	@Override
	public Map<ByteArray, T> getByIds(Collection<ByteArray> ids) throws DataException {
		return get(ids,false);
	}

	/**
	 * Gets entities by using the ids as a prefix.
	 */
	@Override
	public Map<ByteArray, T> get(Collection<ByteArray> ids) throws DataException {
		return get(ids,false);
	}
	
	public Map<ByteArray, T> get(Collection<ByteArray> ids,boolean exact) throws DataException {
		return get(ids,exact,null);
	}

	/**
	 * Gets the entities. 
	 * @param ids
	 * @param exact
	 * @return
	 * @throws DataException
	 */
	public Map<ByteArray, T> get(Collection<ByteArray> ids,boolean exact,Consumer<BatchScanner> consumer) throws DataException {
		BatchScanner scanner = null;
		Map<ByteArray, T> res = Collections.EMPTY_MAP;
		Collection<Range> ranges = new HashSet<>();
		ids.forEach(id -> ranges.add(exact?Range.exact(id):Range.prefix(id)));
		try {
			scanner = getConnector().createBatchScanner(getNameSpaceAndTable(), PUBLIC_AUTHORIZATIONS,
					THREADS);
			scanner.setRanges(ranges);
			if (consumer!=null) {
				consumer.accept(scanner);
			}
			res = toEntities(scanner);
			// entities.forEach(e -> res.put(new String(e.getId()), e));
		} catch (TableNotFoundException e1) {
			e1.printStackTrace();
		} finally {
			scanner.close();
		}
		return res;
	}
	
	@Override
	public int countAll() throws DataException {
		return countAll(null);
	}

	public int countAll(Consumer<Scanner> scannerConsumer) throws DataException {
		Scanner scan = null;
		int count = 0;
		try {
			scan = getConnector()
					.createScanner(getNameSpaceAndTable(), PUBLIC_AUTHORIZATIONS);
			// I tell it to only fetch the url, so only one cell per row
			// is iterated over.
			
			scan.fetchColumn(new Text(getFamilyForCount()), new Text(getQualifierForCount()));
			
			// full table scan
			scan.setRange(new Range());
			
			// I use the counting iterator so it just returns the count 
			IteratorSetting s = new IteratorSetting(21,CountIterator.class);
			scan.addScanIterator(s);
			
			if (scannerConsumer!=null) {
				scannerConsumer.accept(scan);
			}
			
			
			// if the table is entirely empty, there will not even be one result
			Iterator<Entry<Key,Value>> iter = scan.iterator();
			
			// accumulo does iterators backwards, you call 'next' before you 
			// check if it has a next.
			Entry<Key,Value> entry = iter.next();
			if (entry !=null) {
				Value v = entry.getValue();
				if (v!=null) {
					count = valueToInt(v);
				}
			}
		} catch (TableNotFoundException e) {
			wrapException(null, getNameSpaceAndTable(), null, e);
		} finally {
			if (scan != null) {
				scan.close();
			}
		}
		return count;
	}

	protected String getNameSpaceAndTable() {
		if (namespace == null) {
			namespace = System.getProperty("jstock.accumulo.namespace");
			if (namespace == null) {
				String env = System.getProperty("env","test");
				namespace = "test_jstock";
				if (env!=null && !env.toLowerCase().contains("test")) {
					namespace = "jstock";
				}
			}
		}
		return String.format("%s.%s",namespace ,getTable());
	}

	public abstract String getFamilyForCount();
	public abstract String getQualifierForCount();

	public BaseAccumuloDAO(Connector connector) {
		this.setConnector(connector);
	}

	public Connector getConnector() {
		return connector;
	}

	public void setConnector(Connector connector) {
		this.connector = connector;
	}

	public T toSingleEntity(Scanner scan) throws DataException {
		Map<ByteArray, T> entities = toEntities(scan);
		return CollectionUtils.isEmpty(entities.values()) ? null : entities
				.values().iterator().next();
	}

	public ByteArray determineId(T entity) {
		return entity.getId();
	}
	
	public abstract String getTable();

	public abstract Iterable<Mutation> toMutations(T entity);

	public abstract Map<ByteArray, T> toEntities(Iterable<Entry<Key, Value>> scan)
			throws DataException;

}