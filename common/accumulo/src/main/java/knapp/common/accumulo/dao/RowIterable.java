package knapp.common.accumulo.dao;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;

/**
 * Helps you iterate over whole rows on the client side
 * when you get an accumulo scanner.  Naturally accumulo
 * iterates over each cell in the table at a time, but 
 * in the client code it's often much easier to work with
 * a whole row at one time.
 * @author michael
 *
 */
public class RowIterable implements Iterable<Map<String, Value>> {

	private final Comparator<Key> keyComparator;
	private final Iterable<Entry<Key, Value>> core;

	// delimits the family and qualifier.
	private final char delimiter;
	
	public static RowIterable beforeFirstTwoHyphens(Iterable<Entry<Key, Value>> core) {
		return byRowRegex("(.*?-.*?)-.*",1, core,'.');
	}
	
	public static RowIterable beforeFirstHyphen(Iterable<Entry<Key, Value>> core) {
		return byRowBefore("-", core,'.');
	}
	
	public static RowIterable beforeFirstUnderscore(Iterable<Entry<Key, Value>> core) {
		return byRowBefore("_", core,'.');
	}
	
	public static RowIterable beforeFirstTwoUnderscores(Iterable<Entry<Key, Value>> core) {
		return byRowBefore("(.*?_.*?)_.*", core,'.');
	}
	
	public static RowIterable byRowBefore(String before,Iterable<Entry<Key, Value>> core) {
		return byRowBefore(before, core,'.');
	}
	
	public static RowIterable byRowBefore(String before,Iterable<Entry<Key, Value>> core,char delimiter) {
		// some characters need to be escaped.
		String escape = (before.matches("[\\.\\-]")?"\\":"");
		// anything, but not greedy.
		String regex = "(.*?)"+escape+before+".*";
		return byRowRegex(regex,1,core,delimiter);
	}
	
	public static RowIterable byRowRegex(String regex,int group,Iterable<Entry<Key, Value>> core,char delimiter) {
		return new RowIterable(new RowIdRegexComparator(regex, group), core,delimiter);
	}
	
	public static RowIterable byRow(Iterable<Entry<Key, Value>> core) {
		return byRow(core,'.');
	}
	
	public static RowIterable byRow(Iterable<Entry<Key, Value>> core,char delimiter) {
		return new RowIterable(new RowIdComparator(),core,delimiter);
	}
	
	private static final class RowIdRegexComparator implements Comparator<Key> {
		private final Pattern pattern;
		private final int group;
		
		public RowIdRegexComparator(String regex,int group) {
			this.pattern = Pattern.compile(regex);
			this.group = group;
		}
		
		@Override
		public int compare(Key o1, Key o2) {
			String first = o1.getRow().toString();
			String second = o2.getRow().toString();
			Matcher m1 = pattern.matcher(first);
			Matcher m2 = pattern.matcher(second);
			String s1 = "";
			String s2 = "";
			if (m1.find()) {
				s1 = m1.group(group);
			}
			if (m2.find()) {
				s2 = m2.group(group);
			}
			return s1.compareTo(s2);
		}
	}
	
	private static final class RowIdComparator implements Comparator<Key> {
		@Override
		public int compare(Key o1, Key o2) {
			String first = o1.getRow().toString();
			String second = o2.getRow().toString();
			return first.compareTo(second);
		}
	}

	public RowIterable(Comparator<Key> grouper, Iterable<Entry<Key, Value>> core) {
		this.keyComparator = grouper;
		this.core = core;
		this.delimiter = '.';
	}

	public RowIterable(Comparator<Key> grouper,
			Iterable<Entry<Key, Value>> core, char delimiter) {
		this.keyComparator = grouper;
		this.core = core;
		this.delimiter = delimiter;
	}

	@Override
	public RowIterator iterator() {
		return new RowIterator(keyComparator, core.iterator(), delimiter);
	}

	public static class RowIterator implements Iterator<Map<String, Value>> {

		private final Comparator<Key> keComparator;
		private final Iterator<Entry<Key, Value>> core;
		private final char delimiter;

		private Entry<Key, Value> last;

		private final Map<String, Value> row = new HashMap<>();
		
		private String currentRowId;

		public RowIterator(Comparator<Key> keyComparator,
				Iterator<Entry<Key, Value>> core, char delimiter) {
			this.keComparator = keyComparator;
			this.core = core;
			this.delimiter = delimiter;
		}

		@Override
		public boolean hasNext() {
			return last != null || core.hasNext();
		}
		
		public String getCurrentRowId() {
			return currentRowId;
		}

		@Override
		public Map<String, Value> next() {
			// must populate the row.
			row.clear();
			if (last == null) {
				if (!core.hasNext()) {
					// the core was always empty.
					return null;
				}
				last = core.next();
			}
			String columnName = RowIterable.toColumnName(last.getKey(),delimiter);
			row.put(columnName,last.getValue());
			currentRowId = last.getKey().getRow().toString();
			
			boolean evenMore = false;
			while (core.hasNext()) {
				Entry<Key, Value> coreNext = core.next();
				int delta = keComparator.compare(last.getKey(),
						coreNext.getKey());
				last = coreNext;
				if (delta == 0) {
					// the row has not changed.
					columnName = RowIterable.toColumnName(coreNext.getKey(),delimiter);
					row.put(columnName,coreNext.getValue());
				} else {
					evenMore = true;
					break;
				}
			}
			if (!evenMore) {
				// this is the last row.
				last = null;
				// by setting last to null, the hasNext method will return false;
			}
			return row;
		}
	}
	
	public static String toColumnName(Key key,char delimiter) {
		return String.format("%s%s%s", key.getColumnFamily().toString(),
				delimiter, key.getColumnQualifier().toString());
	}

}
