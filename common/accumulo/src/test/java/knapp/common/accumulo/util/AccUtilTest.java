package knapp.common.accumulo.util;

import java.util.Calendar;

import knapp.common.accumulo.dao.AccUtil;

import org.apache.accumulo.core.data.Value;
import org.junit.Assert;
import org.junit.Test;

public class AccUtilTest {
	
	@Test
	public void calendar() {
		Calendar c = Calendar.getInstance();
		Value v = AccUtil.calendarToValue(c);
		Calendar rec = AccUtil.valueToCalendar(v);
		Assert.assertEquals(c,rec);
		Assert.assertEquals(c.getTimeInMillis(),rec.getTimeInMillis());
	}

}
