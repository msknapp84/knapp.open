package knapp.common.accumulo.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import knapp.common.accumulo.dao.RowIterable;

import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Value;
import org.junit.Assert;
import org.junit.Test;

public class RowIteratorTest {

	
	@Test
	public void testEmpty() {
		SortedMap<Key,Value> mockAccumuloTable = new TreeMap<>();
		RowIterable foo = RowIterable.byRow(mockAccumuloTable.entrySet());
		Iterator<Map<String,Value>> iter = foo.iterator();
		Assert.assertFalse(iter.hasNext());
		Assert.assertNull(iter.next());
	}
	
	@Test
	public void testOne() {
		SortedMap<Key,Value> mockAccumuloTable = new TreeMap<>();
		mockAccumuloTable.put(new Key("a","f","q"),new Value("blah".getBytes()));
		RowIterable foo = RowIterable.byRow(mockAccumuloTable.entrySet());
		Iterator<Map<String,Value>> iter = foo.iterator();
		Assert.assertTrue(iter.hasNext());
		Map<String,Value> row = iter.next();
		Assert.assertEquals(1,row.size());
		Assert.assertFalse(iter.hasNext());
		Assert.assertNull(iter.next());
	}
	
	@Test
	public void testIteration() {
		SortedMap<Key, Value> mockAccumuloTable = createTestData();
		
		RowIterable foo = RowIterable.byRow(mockAccumuloTable.entrySet());
		Iterator<Map<String,Value>> iter = foo.iterator();
		
		Assert.assertTrue(iter.hasNext());
		Map<String,Value> first = iter.next();
		Assert.assertEquals("1",new String(first.get("f.a").get()));
		Assert.assertEquals("2",new String(first.get("f.b").get()));
		Assert.assertEquals("5",new String(first.get("f2.z").get()));

		Assert.assertTrue(iter.hasNext());
		Map<String,Value> second = iter.next();
		Assert.assertEquals("6",new String(second.get("f.a").get()));
		Assert.assertEquals("7",new String(second.get("f.b").get()));
		Assert.assertEquals("8",new String(second.get("f.c").get()));
		
		Assert.assertTrue(iter.hasNext());
		Map<String,Value> third = iter.next();
		Assert.assertEquals("9",new String(second.get("f.b").get()));
		
		Assert.assertFalse(iter.hasNext());
		Assert.assertNull(iter.next());
	}
	
	@Test
	public void testPrefixIteration() {
		SortedMap<Key, Value> mockAccumuloTable = createTestData();
		
		RowIterable foo = RowIterable.byRowBefore(".",mockAccumuloTable.entrySet());
		Iterator<Map<String,Value>> iter = foo.iterator();
		
		Assert.assertTrue(iter.hasNext());
		Map<String,Value> first = iter.next();
		Assert.assertEquals("6",new String(first.get("f.a").get()));
		Assert.assertEquals("9",new String(first.get("f.b").get()));
		Assert.assertEquals("5",new String(first.get("f2.z").get()));
		
		Assert.assertFalse(iter.hasNext());
		Assert.assertNull(iter.next());
	}

	private SortedMap<Key, Value> createTestData() {
		SortedMap<Key,Value> mockAccumuloTable = new TreeMap<>();
		Key k1 = new Key("foo.bar","f","a");
		Key k2 = new Key("foo.bar","f","b");
		Key k3 = new Key("foo.bar","f","c");
		Key k4 = new Key("foo.bar","f2","a");
		Key k5 = new Key("foo.bar","f2","z");
		Key k6 = new Key("foo.baz","f","a");
		Key k7 = new Key("foo.baz","f","b");
		Key k8 = new Key("foo.baz","f","c");
		Key k9 = new Key("foo.bell","f","b");
		
		mockAccumuloTable.put(k1, new Value("1".getBytes()));
		mockAccumuloTable.put(k2, new Value("2".getBytes()));
		mockAccumuloTable.put(k3, new Value("3".getBytes()));
		mockAccumuloTable.put(k4, new Value("4".getBytes()));
		mockAccumuloTable.put(k5, new Value("5".getBytes()));
		mockAccumuloTable.put(k6, new Value("6".getBytes()));
		mockAccumuloTable.put(k7, new Value("7".getBytes()));
		mockAccumuloTable.put(k8, new Value("8".getBytes()));
		mockAccumuloTable.put(k9, new Value("9".getBytes()));
		return mockAccumuloTable;
	}
}
