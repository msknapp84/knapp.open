package knapp.common.data.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import knapp.common.data.domain.Entity;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;

import org.junit.Test;

public abstract class DAOTestBase<T extends Entity> {

	// the rules are:
	// 1. Start with a blank slate
	// 2. Return at least three entities.
	// 3. Ids should already be set.

	public abstract List<T> getEntities();

	public abstract DAO<T> getDAO();

	// probably will be overridden by children
	public boolean areEqual(T exp, T ret) {
		return (exp == null) ? ret == null : exp.equals(ret);
	}

	// probably will be overridden by children
	public String makeDebugString(T ret) {
		return ret == null ? "null" : ret.toString();
	}
	

	@Test
	public void crud() throws DataException {
		List<T> entities = getEntities();
		Map<ByteArray, T> indexedStartEntities = new HashMap<>();
		entities.forEach(e -> indexedStartEntities.put(e.getId(), e));
		assertTrue(
				"The test should be run with at least three entities.  You have "
						+ entities.size(), entities.size() > 3);
		List<ByteArray> ids = entities.stream().filter(e -> e != null)
				.map(e -> e.getId()).filter(b -> b != null)
				.collect(Collectors.toList());
		assertEquals("Some ids were null, they should be set from the start.",
				ids.size(), entities.size());
		int countAll = getDAO().countAll();
		assertEquals(
				"The test should start with an empty DAO.  There appears to already be "
						+ countAll + " entities", 0, countAll);
		Map<ByteArray, Boolean> exists = getDAO().exists(ids);
		assertEquals(
				"The exists method should report on all ids, whether they exist or not.",
				exists.size(), ids.size());
		for (Entry<ByteArray, Boolean> ex : exists.entrySet()) {
			assertNotNull(ex.getKey());
			assertFalse(
					"The entity with this id already exists: "
							+ String.valueOf(ex.getKey()), ex.getValue());
		}
		// let's save them.
		getDAO().save(entities);

		countAll = getDAO().countAll();
		assertEquals(
				"After trying to save entities, the total count did not increase to the expected size of "
						+ entities.size() + ", it is actually " + countAll,
				entities.size(), countAll);

		// see if we can get them
		Map<ByteArray, T> retrieved = getDAO().get(ids);
		for (ByteArray id : ids) {
			T ret = retrieved.get(id);
			assertNotNull(ret);
			T exp = indexedStartEntities.get(id);
			assertEquals(exp.getId(), ret.getId());
			
//			assertTrue(String.format(
//					"These two entities are not equal %n%s%n%s",
//					makeDebugString(exp), makeDebugString(ret)),
//					areEqual(exp, ret));
		}

		// make sure they all exist
		exists = getDAO().exists(ids);
		for (Entry<ByteArray, Boolean> ex : exists.entrySet()) {
			assertNotNull(ex.getKey());
			assertTrue("The entity with this id does not appear to exist: "
					+ String.valueOf(ex.getKey()), ex.getValue());
		}

		// we should be able to delete them.
		getDAO().delete(ids);
		exists = getDAO().exists(ids);
		for (Entry<ByteArray, Boolean> ex : exists.entrySet()) {
			assertNotNull(ex.getKey());
			assertFalse(
					"The entity with this id was not deleted: "
							+ String.valueOf(ex.getKey()), ex.getValue());
		}
		countAll = getDAO().countAll();
		assertEquals(
				"After deleting all entities, countAll should say 0, but it has "
						+ countAll, 0, countAll);
		// the test ends how it started, with an empty DAO.
	}

}
