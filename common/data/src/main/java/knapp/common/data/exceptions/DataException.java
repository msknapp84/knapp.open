package knapp.common.data.exceptions;

public class DataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String query;
	private String table;
	private Class<?> entityType;
	private Object violatingObject;

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public Class<?> getEntityType() {
		return entityType;
	}

	public void setEntityType(Class<?> entityType) {
		this.entityType = entityType;
	}

	public Object getViolatingObject() {
		return violatingObject;
	}

	public void setViolatingObject(Object violatingObject) {
		this.violatingObject = violatingObject;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public DataException(Throwable cause) {
		super(cause);
	}
	
	public DataException(String message,Throwable cause) {
		super(message,cause);
	}
}
