package knapp.common.data.domain;

import java.util.Collections;
import java.util.List;

import knapp.common.util.ByteArray;

public class Entity extends Validatable {
	
	private ByteArray id;
	
	public Entity() {
		
	}

	public Entity(byte[] id) {
		this.setId(id);
	}

	public Entity(ByteArray id) {
		this.setId(id);
	}

	public Entity(String id) {
		this.setId(id);
	}
	
	public ByteArray getId() {
		return id;
	}

	public String getIdString() {
		return String.valueOf(id);
	}
	
	@Override
	public List<String> getValidationProblems() {
		if (getId() == null) {
			return Collections.singletonList("The id is not set.");
		}
		return Collections.EMPTY_LIST;
	}
	
	public void setId(byte[] id) {
		this.id = new ByteArray(id);
	}
	
	public void setId(String id) {
		this.id = new ByteArray(id);
	}
	
	public void setId(ByteArray id) {
		this.id = id;
	}

	public static abstract class EntityBuilder<T> {
		protected byte[] id;
		
		public EntityBuilder<T> id(byte[] id) {
			this.id = id;
			return this;
		}
		
		public byte[] getId() {
			return id;
		}
		
		public abstract T build();
	}

}
