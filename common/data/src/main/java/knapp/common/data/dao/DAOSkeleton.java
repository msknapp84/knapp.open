package knapp.common.data.dao;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import knapp.common.data.domain.Entity;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;
import knapp.common.util.Reflect;

public abstract class DAOSkeleton<T extends Entity> implements DAO<T> {

	public abstract void save(Collection<T> entity) throws DataException;

	public abstract void delete(Collection<ByteArray> ids) throws DataException;

	public void save(T entity) throws DataException {
		save(Collections.singleton(entity));
	}

	public void delete(ByteArray id) throws DataException {
		delete(Collections.singleton(id));
	}

	public T get(ByteArray id) throws DataException {
		Map<ByteArray, T> es = get(Collections.singleton(id));
		return es.get(id);
	}

	public T get(String id) throws DataException {
		ByteArray b = new ByteArray(id);
		Map<ByteArray, T> mp = getByIds(Collections.singleton(b));
		return mp.get(b);
	}

	public Map<ByteArray, Boolean> exists(Collection<ByteArray> ids)
			throws DataException {
		// this should probably be overridden by children since it could
		// easily be optimized.
		Map<ByteArray, T> entities = get(ids);
		Map<ByteArray, Boolean> results = new HashMap<>();
		for (ByteArray id : entities.keySet()) {
			T t = entities.get(id);
			results.put(id, t != null);
		}
		ids.stream().filter(id -> id != null && !results.containsKey(id))
				.forEach(id -> results.put(id, false));
		return results;
	}

	public Boolean exists(ByteArray id) throws DataException {
		return exists(Collections.singleton(id)).get(id);
	}

	public static void wrapException(String query, String table,
			Object violating, Exception e) throws DataException {
		DataException de = new DataException(e);
		de.setViolatingObject(violating);
		de.setQuery(query);
		de.setTable(table);
		throw de;
	}

	public static void closeQuieter(String query, String table,
			Object violating, Closeable writer) throws DataException {
		if (writer != null) {
			try {
				writer.close();
			} catch (IOException e) {
				wrapException(query, table, violating, e);
			}
		}
	}

	// if it walks like a duck....
	public static void closeLikeADuck(String query, String table,
			Object violating, Object closeable) throws DataException {
		if (closeable != null) {
			try {
				Reflect.invoke("close", closeable);
			} catch (Exception e) {
				wrapException(query, table, violating, e);
			}
		}
	}
}