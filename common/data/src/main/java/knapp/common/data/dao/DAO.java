package knapp.common.data.dao;

import java.util.Collection;
import java.util.Map;

import knapp.common.data.domain.Entity;
import knapp.common.data.exceptions.DataException;
import knapp.common.util.ByteArray;

public interface DAO<T extends Entity> {
	void save(T entity) throws DataException;
	void save(Collection<T> entity) throws DataException;
	void delete(ByteArray id) throws DataException;
	void delete(Collection<ByteArray> ids) throws DataException;
	T get(ByteArray id) throws DataException;
	Map<ByteArray,T> get(Collection<ByteArray> ids) throws DataException;
	Map<ByteArray,T> getByIds(Collection<ByteArray> ids) throws DataException;
	Map<ByteArray,Boolean> exists(Collection<ByteArray> ids) throws DataException;
	Boolean exists(ByteArray id) throws DataException;
	int countAll() throws DataException;
}
