package knapp.common.data.domain;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

public class Validatable {
	
	public List<String> getValidationProblems() {
		return Collections.EMPTY_LIST;
	}
	
	public void validate() {
		List<String> problems = getValidationProblems();
		if (CollectionUtils.isNotEmpty(problems)) {
			String combined = StringUtils.join(problems,"\n");
			throw new IllegalArgumentException("The entity is not valid for the following reasons:\n"+combined);
		}
	}
	
	public boolean isValid() {
		return CollectionUtils.isEmpty(getValidationProblems());
	}
}
