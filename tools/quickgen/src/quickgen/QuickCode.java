package quickgen;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QuickCode {

	// Test with this:
	// builder Fubar true String:name Date:timestamp int:favoriteNumber:true
	// List<Calendar>:otherTimes Set<Pair>:pairs Map<String,Integer>:myMap

	public static void main(String[] args) {
		// format is: type includeValidation className def [def...]
		// where type is: builder or factory
		// includeValidation is true or false
		// className is the name of the class being built.
		// and def is: type:name[:mutable]
		// type can be anything, but List, Set, and Map are treated a special
		// way
		//
		if (args.length < 4) {
			usage();
			return;
		}

		String type = args[0];
		if (!type.equals("builder") && !type.equals("factory")) {
			usage();
			return;
		}
		boolean factory = "factory".equalsIgnoreCase(type);
		String className = args[2];
		boolean includeValidation = Boolean.parseBoolean(args[1]);
		List<FieldDef> defs = new ArrayList<>();
		for (int i = 3; i < args.length; i++) {
			FieldDef fd = new FieldDef();
			String[] parts = args[i].split(":");
			fd.setTypeName(parts[0]);
			fd.setFieldName(parts[1]);
			if (parts.length == 3) {
				fd.setMutable(parts[2].toLowerCase().contains("t"));
			}
			defs.add(fd);
		}
		
		Set<String> requiredImports = new HashSet<>();
		String basicClass = generateBasicClass(defs, className, factory,
				includeValidation,requiredImports);
		String sub = generateFactory(defs, className, factory,requiredImports);
		String header = generateHeader(requiredImports,className);
		System.out.println(header);
		System.out.println(basicClass);
		System.out.println(sub);
		System.out.println("}");
	}

	private static String generateHeader(Set<String> requiredImports,
			String className) {
		StringBuilder sb = new StringBuilder();
		sb.append("package TBD;\n\n");
		for (String imp : requiredImports) {
			sb.append("import ").append(imp).append(";\n");
		}
		sb.append("\npublic class ").append(className).append(" {\n");
		
		return sb.toString();
	}

	private static void usage() {
		StringBuilder sb = new StringBuilder();
		sb.append("\n\nThis is a quick code generation tool for writing builder and factory style classes.\n");
		sb.append("It generates the body of the code, but does not generate equals, hashCode, toString,\n");
		sb.append("compareTo, or similar methods.  That is because the IDE already has good ways to do that,\n");
		sb.append("but it does not have a good way to generate builders or factories.\n\n");
		sb.append("Usage: <builder|factory> <includeValidation> <className> def [def...]\n\n");
		sb.append("includeValidation must either be true or false.\n\t\tIf it's true, some methods will be added to the output");
		sb.append("\n\t\tthat validate your object.\n\n");
		sb.append("def - <typeName:fieldName[:mutable]>\n");
		sb.append("\t\tthe fields generated are immutable by default.\n");
		sb.append("\t\tIf the type is Map<...>, List<...>, or Set<...>, the builder or factory\n");
		sb.append("\t\twill add a few extra methods to support adding single objects to those collections.\n\n");
		sb.append("Examples: A great way to understand this tool is by running it.  Try doing these:\n\n");
		sb.append("java -jar quickCode.jar builder true Fubar String:name Date:timestamp ");
		sb.append("int:favoriteNumber:true \"List<Calendar>:otherTimes\" \"Set<Pair>:pairs\" \"Map<String,Integer>:myMap\"\n\n");
		sb.append("java -jar quickCode.jar factory false Snafu String:name Date:timestamp ");
		sb.append("int:favoriteNumber:true \"List<Calendar>:otherTimes\" \"Set<Pair>:pairs\" \"Map<String,Integer>:myMap\"\n\n");
		sb.append("Recommendations:  Setting some aliases in your ~/.bashrc can help you run this even faster:\n\n");
		sb.append("alias qf='java -jar $QUICK_CODE_HOME/quickCode.jar factory true $@'\n");
		sb.append("alias qb='java -jar $QUICK_CODE_HOME/quickCode.jar builder true $@'\n\n");
		sb.append("Using those aliases, your commands get simplified to this:\n\n");
		sb.append("qb Fubar String:name Date:timestamp ");
		sb.append("int:favoriteNumber:true \"List<Calendar>:otherTimes\" \"Set<Pair>:pairs\" \"Map<String,Integer>:myMap\"\n\n");
		sb.append("qf Snafu String:name Date:timestamp ");
		sb.append("int:favoriteNumber:true \"List<Calendar>:otherTimes\" \"Set<Pair>:pairs\" \"Map<String,Integer>:myMap\"\n\n");
		sb.append("Author: Michael Knapp, report bugs or suggestions to michaelscottknapp@hotmail.com\n");
		sb.append("See the project on bitbucket: https://bitbucket.org/msknapp84/knapp.open\n\n");
		System.out.println(sb.toString());
	}

	private static String generateBasicClass(List<FieldDef> defs,
			String className, boolean factory, boolean includeValidation,Set<String> requiredImports) {
		StringBuilder sb = new StringBuilder();
		String tab = "\t";
		String twoTabs = "\t\t";
		if (factory) {
			sb.append(tab).append("private static final ").append(className).append("Factory FACTORY = new ");
			sb.append(className).append("Factory();\n");
		}
		for (FieldDef def : defs) {
			sb.append(tab).append("private ");
			if (!def.isMutable()) {
				sb.append("final ");
			}
			sb.append(def.getTypeName()).append(' ').append(def.getFieldName())
					.append(";\n");
		}
		// private constructor
		sb.append("\n").append(tab).append("private ").append(className)
				.append('(').append(className)
				.append(factory ? "Factory" : "Builder").append(" given) {\n");
		for (FieldDef def : defs) {
			sb.append(twoTabs).append("this.").append(def.getFieldName())
					.append(" = ");
			if (def.isCollection() && !def.isMutable()) {
				// make sure we create a new one that is immutable.
				String collectionType = def.getTypeName().substring(0,def.getTypeName().indexOf("<"));
				sb.append("Collections.unmodifiable").append(collectionType)
					.append("(new ");
				requiredImports.add("java.util.Collections");
				if ("List".equals(collectionType)) {
					sb.append("Array");
					requiredImports.add("java.util.List");
					requiredImports.add("java.util.ArrayList");
				} else {
					sb.append("Hash");
					if ("Map".equals(collectionType)) {
						requiredImports.add("java.util.Map");
						requiredImports.add("java.util.HashMap");
					} else {
						// it's a set
						requiredImports.add("java.util.Set");
						requiredImports.add("java.util.HashSet");
					}
				}
				
				sb.append(collectionType).append("<>(");
			}
			sb.append("given.").append(def.fieldName);
			if (def.isCollection() && !def.isMutable()) {
				sb.append("))");
			}
			sb.append(";\n");
		}
		if (includeValidation) {
			sb.append(twoTabs).append("validate();\n");
		}
		sb.append(tab).append("}\n\n");
		if (includeValidation) {
			// validate method
			
			sb.append(tab)
					.append("public void validate() throws IllegalArgumentException {\n");
			sb.append(twoTabs).append(
					"List<String> problems = validationProblems();\n");
			sb.append(twoTabs).append("if (!problems.isEmpty()) {\n");
			sb.append(twoTabs)
					.append('\t')
					.append("String problemString = problems.stream().collect(Collectors.joining(\"\\n\"));\n");
			requiredImports.add("java.util.stream.Collectors");
			sb.append(twoTabs)
					.append('\t')
					.append("throw new IllegalArgumentException(problemString);\n");
			requiredImports.add("java.lang.IllegalArgumentException");
			sb.append(twoTabs).append("}\n");
			sb.append(tab).append("}\n\n");

			// is valid method
			sb.append(tab).append("public boolean isValid() {\n");
			sb.append(twoTabs).append(
					"return validationProblems().isEmpty();\n");
			sb.append(tab).append("}\n\n");

			// validationProblems method
			sb.append(tab).append(
					"public List<String> validationProblems() {\n");
			sb.append(twoTabs).append(
					"List<String> problems = new ArrayList<>();\n");
			requiredImports.add("java.util.List");
			requiredImports.add("java.util.ArrayList");
			sb.append(twoTabs).append(
					"// TODO list all validation problems.\n");
			sb.append(twoTabs).append("return problems;\n");
			sb.append(tab).append("}\n\n");
		}

		if (factory) {
			sb.append(tab)
					.append("public static final ")
					.append(className)
					.append("Factory factory() {\n\t\t\treturn FACTORY;\n\t\t}\n\n");
		} else {
			sb.append(tab).append("public static final ").append(className)
					.append("Builder newBuilder() {\n\t\t\treturn new ")
					.append(className).append("Builder()")
					.append(";\n\t\t}\n\n");
		}
		// getter methods
		for (FieldDef def : defs) {
			String get = def.getTypeName().equalsIgnoreCase("boolean") ? "is"
					: "get";

			sb.append(tab).append("public ").append(def.getTypeName())
					.append(" ").append(get).append(cap(def.getFieldName()))
					.append("() {\n");
			sb.append(twoTabs).append("return ").append(def.getFieldName())
					.append(";\n");
			sb.append(tab).append("}\n\n");

			if (def.isMutable()) {
				sb.append(tab).append("public void set")
						.append(cap(def.getFieldName())).append("(")
						.append(def.getTypeName()).append(' ')
						.append(def.getFieldName()).append(") {\n");
				sb.append(twoTabs).append("this.").append(def.getFieldName())
						.append(" = ").append(def.getFieldName()).append(";\n");
				sb.append(tab).append("}\n\n");
			}
		}

		return sb.toString();
	}

	private static String generateFactory(List<FieldDef> defs,
			String className, boolean factory,Set<String> requiredImports) {
		StringBuilder sb = new StringBuilder();
		String tab = "\t";
		String twoTabs = "\t\t";
		String threeTabs = "\t\t\t";
		// I let it be protected in case children want to override it.
		sb.append(tab).append(factory ? "private" : "protected")
				.append(" static class ").append(className)
				.append(factory ? "Factory" : "Builder").append(" {\n");

		// fields.
		for (FieldDef def : defs) {
			sb.append(twoTabs).append("private ");
			sb.append(def.getTypeName()).append(' ').append(def.getFieldName())
					.append(";\n");
		}
		sb.append('\n');

		for (FieldDef def : defs) {

			String get = def.getTypeName().equalsIgnoreCase("boolean") ? "is"
					: "get";

			sb.append(twoTabs).append("public ").append(def.getTypeName())
					.append(" ").append(get).append(cap(def.getFieldName()))
					.append("() {\n");
			sb.append(threeTabs).append("return ").append(def.getFieldName())
					.append(";\n");
			sb.append(twoTabs).append("}\n\n");

			sb.append(twoTabs).append("public void set")
					.append(cap(def.getFieldName())).append("(")
					.append(def.getTypeName()).append(' ')
					.append(def.getFieldName()).append(") {\n");
			sb.append(threeTabs).append("this.").append(def.getFieldName())
					.append(" = ").append(def.getFieldName()).append(";\n");
			sb.append(twoTabs).append("}\n\n");

			sb.append(twoTabs).append("public ").append(className)
					.append(factory ? "Factory " : "Builder ")
					.append(def.getFieldName()).append("(")
					.append(def.getTypeName()).append(' ')
					.append(def.getFieldName()).append(") {\n");
			sb.append(threeTabs).append("this.").append(def.getFieldName())
					.append(" = ").append(def.getFieldName()).append(";\n");
			sb.append(threeTabs).append("return this;\n");
			sb.append(twoTabs).append("}\n\n");

			if (def.getTypeName().matches("List<.+>")
					|| def.getTypeName().matches("Set<.+>")
					|| def.getTypeName().matches("Map<.+>")
					|| def.getTypeName().matches("Collection<.+>")) {
				String collectionType = def.getTypeName().substring(0,
						def.getTypeName().indexOf("<"));
				String keyType = getGenericType(def.getTypeName());
				String valueType = null;
				if ("Map".equals(collectionType)) {
					int i = keyType.indexOf(",");
					valueType = keyType.substring(i + 1);
					keyType = keyType.substring(0, i);
				}
				sb.append(twoTabs).append("public ").append(className)
						.append(factory ? "Factory add" : "Builder add")
						.append(cap(def.getFieldName())).append("(")
						.append(keyType).append(' ').append(decap(keyType));
				if (valueType != null) {
					sb.append(", ").append(valueType).append(' ')
							.append(decap(valueType));
				}
				sb.append(") {\n");
				sb.append(threeTabs).append("if (this.")
						.append(def.getFieldName()).append(" == null) {\n");
				sb.append(threeTabs).append('\t').append("this.")
						.append(def.getFieldName()).append(" = new ");
				if ("List".equals(collectionType)) {
					sb.append("Array");
					requiredImports.add("java.util.List");
					requiredImports.add("java.util.ArrayList");
				} else {
					sb.append("Hash");
				}
				// if it's just a collection, a HashSet is used by default.
				sb.append(collectionType!="Collection"?collectionType:"Set");
				sb.append("<>();\n");
				sb.append(threeTabs).append("}\n");
				sb.append(threeTabs).append("this.").append(def.getFieldName());
				if ("Map".equals(collectionType)) {
					sb.append(".put(").append(decap(keyType)).append(", ")
							.append(decap(valueType)).append(");\n");
				} else {
					sb.append(".add(").append(decap(keyType)).append(");\n");
				}
				sb.append(threeTabs).append("return this;\n");
				sb.append(twoTabs).append("}\n\n");
			}
			
			// some types are so commonly used, it would be nice to guess their imports:
			guessImports(requiredImports, def);
		}

		sb.append(twoTabs).append("public ").append(className)
				.append(" build() {\n");
		sb.append(threeTabs).append("return new ").append(className)
				.append("(this);\n");
		sb.append(twoTabs).append("}\n\n");

		sb.append(tab).append("}\n");
		return sb.toString();
	}

	private static void guessImports(Set<String> requiredImports, FieldDef def) {
		if ("Date".equals(def.getTypeName())) {
			requiredImports.add("java.util.Date");
		}
		if ("Calendar".equals(def.getTypeName())) {
			requiredImports.add("java.util.Calendar");
		}
		if ("File".equals(def.getTypeName())) {
			requiredImports.add("java.io.File");
		}
		if ("BigDecimal".equals(def.getTypeName())) {
			requiredImports.add("java.math.BigDecimal");
		}
		if ("BigInteger".equals(def.getTypeName())) {
			requiredImports.add("java.math.BigInteger");
		}
		if ("URI".equals(def.getTypeName())) {
			requiredImports.add("java.net.URI");
		}
		if ("URL".equals(def.getTypeName())) {
			requiredImports.add("java.net.URL");
		}
		if ("Path".equals(def.getTypeName())) {
			requiredImports.add("java.nio.file.Path");
		}
	}

	private static String getGenericType(String typeName) {
		String regex = ".*<(.+)>";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(typeName);
		if (m.matches()) {
			return m.group(1);
		}
		return null;
	}

	private static String cap(String fieldName) {
		return Character.toUpperCase(fieldName.charAt(0))
				+ fieldName.substring(1);
	}

	private static Object decap(String fieldName) {
		return Character.toLowerCase(fieldName.charAt(0))
				+ fieldName.substring(1);
	}

	private static class FieldDef {
		private String typeName;
		private String fieldName;
		private boolean mutable;

		public String getTypeName() {
			return typeName;
		}

		public boolean isCollection() {
			return typeName.startsWith("Map") || typeName.startsWith("List")
					|| typeName.startsWith("Set");
		}

		public void setTypeName(String typeName) {
			this.typeName = typeName;
		}

		public String getFieldName() {
			return fieldName;
		}

		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}

		public boolean isMutable() {
			return mutable;
		}

		public void setMutable(boolean mutable) {
			this.mutable = mutable;
		}
	}
}