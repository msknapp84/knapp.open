package TBD;

import java.util.HashSet;
import java.util.List;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.lang.IllegalArgumentException;
import java.util.Collections;
import java.util.stream.Collectors;
import java.util.ArrayList;

public class Fubar {

	private final String name;
	private final Date timestamp;
	private int favoriteNumber;
	private final List<Calendar> otherTimes;
	private final Set<Pair> pairs;
	private final Map<String,Integer> myMap;

	private Fubar(FubarBuilder given) {
		this.name = given.name;
		this.timestamp = given.timestamp;
		this.favoriteNumber = given.favoriteNumber;
		this.otherTimes = Collections.unmodifiableList(new ArrayList<>(given.otherTimes));
		this.pairs = Collections.unmodifiableSet(new HashSet<>(given.pairs));
		this.myMap = Collections.unmodifiableMap(new HashMap<>(given.myMap));
		validate();
	}

	public void validate() throws IllegalArgumentException {
		List<String> problems = validationProblems();
		if (!problems.isEmpty()) {
			String problemString = problems.stream().collect(Collectors.joining("\n"));
			throw new IllegalArgumentException(problemString);
		}
	}

	public boolean isValid() {
		return validationProblems().isEmpty();
	}

	public List<String> validationProblems() {
		List<String> problems = new ArrayList<>();
		// TODO list all validation problems.
		return problems;
	}

	public static final FubarBuilder newBuilder() {
			return new FubarBuilder();
		}

	public String getName() {
		return name;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public int getFavoriteNumber() {
		return favoriteNumber;
	}

	public void setFavoriteNumber(int favoriteNumber) {
		this.favoriteNumber = favoriteNumber;
	}

	public List<Calendar> getOtherTimes() {
		return otherTimes;
	}

	public Set<Pair> getPairs() {
		return pairs;
	}

	public Map<String,Integer> getMyMap() {
		return myMap;
	}


	protected static class FubarBuilder {
		private String name;
		private Date timestamp;
		private int favoriteNumber;
		private List<Calendar> otherTimes;
		private Set<Pair> pairs;
		private Map<String,Integer> myMap;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public FubarBuilder name(String name) {
			this.name = name;
			return this;
		}

		public Date getTimestamp() {
			return timestamp;
		}

		public void setTimestamp(Date timestamp) {
			this.timestamp = timestamp;
		}

		public FubarBuilder timestamp(Date timestamp) {
			this.timestamp = timestamp;
			return this;
		}

		public int getFavoriteNumber() {
			return favoriteNumber;
		}

		public void setFavoriteNumber(int favoriteNumber) {
			this.favoriteNumber = favoriteNumber;
		}

		public FubarBuilder favoriteNumber(int favoriteNumber) {
			this.favoriteNumber = favoriteNumber;
			return this;
		}

		public List<Calendar> getOtherTimes() {
			return otherTimes;
		}

		public void setOtherTimes(List<Calendar> otherTimes) {
			this.otherTimes = otherTimes;
		}

		public FubarBuilder otherTimes(List<Calendar> otherTimes) {
			this.otherTimes = otherTimes;
			return this;
		}

		public FubarBuilder addOtherTimes(Calendar calendar) {
			if (this.otherTimes == null) {
				this.otherTimes = new ArrayList<>();
			}
			this.otherTimes.add(calendar);
			return this;
		}

		public Set<Pair> getPairs() {
			return pairs;
		}

		public void setPairs(Set<Pair> pairs) {
			this.pairs = pairs;
		}

		public FubarBuilder pairs(Set<Pair> pairs) {
			this.pairs = pairs;
			return this;
		}

		public FubarBuilder addPairs(Pair pair) {
			if (this.pairs == null) {
				this.pairs = new HashSet<>();
			}
			this.pairs.add(pair);
			return this;
		}

		public Map<String,Integer> getMyMap() {
			return myMap;
		}

		public void setMyMap(Map<String,Integer> myMap) {
			this.myMap = myMap;
		}

		public FubarBuilder myMap(Map<String,Integer> myMap) {
			this.myMap = myMap;
			return this;
		}

		public FubarBuilder addMyMap(String string, Integer integer) {
			if (this.myMap == null) {
				this.myMap = new HashMap<>();
			}
			this.myMap.put(string, integer);
			return this;
		}

		public Fubar build() {
			return new Fubar(this);
		}

	}

}
