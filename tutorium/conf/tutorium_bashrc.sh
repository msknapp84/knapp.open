export JAVA_HOME=/opt/java8
export CATALINA_HOME=$HOME/tomcat7
export CATALINA_OPTS=' -Xms256m -Xmx4g -XX:MaxPermSize=1g'
export WORKSPACE=$HOME/workspace
export TUTORIUM_HOME=$WORKSPACE/knapp.open/tutorium
export TUTORIUM_CONF=$TUTORIUM_HOME/conf
export TUTORIUM_MAINTENANCE=$WORKSPACE/knapp.open/maintenance
export PATH=$CATALINA_HOME/bin:$JAVA_HOME/bin:$TUTORIUM_MAINTENANCE:$PATH

alias t='. $TUTORIUM_MAINTENANCE/tutorium.sh $@'
alias tr='ruby $TUTORIUM_MAINTENANCE/tutorium.rb $@'
alias cdws='cd $WORKSPACE'
alias cdt='cd $TUTORIUM_HOME'
alias cdtc='cd $CATALINA_HOME'
alias cdc='cd $TUTORIUM_CONF'
alias mci='mvn clean install'
alias gs='git status'
alias sbrc='source $HOME/.bashrc'
alias vt='vi $TUTORIUM_CONF/tutorium_bashrc.sh'
alias catalina='$CATALINA_HOME/bin/catalina.sh $@'
alias c='$CATALINA_HOME/bin/catalina.sh $@'