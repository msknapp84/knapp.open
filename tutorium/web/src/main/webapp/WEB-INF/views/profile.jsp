<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="tutorium.data.entity.User" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Profile</title>
</head>
<body>
<h1>${user.userName}</h1>
First Name: ${user.firstName }<br/>
Second Name: ${user.lastName }<br/>
Second Name: ${user.email }<br/>
Expired: ${user.expired }<br/>
Locked: ${user.locked }<br/>
Enabled: ${user.enabled }<br/>
<h2>Authorizations</h2>
<c:forEach var="auth" items="${user.auths}">
${auth }
</c:forEach>
<a href="home">Home</a>
</body>
</html>