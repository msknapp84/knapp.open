<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${tutorial.name }</title>
</head>
<body>
	<!-- This is the tutorial table of contents page. -->
	<h1>${tutorial.name }</h1>
	<p>${tutorial.author }</p>
	<p>${tutorial.created }</p>
	<p>${tutorial.summary }</p>
	<c:if test="tutorial.sections!=null && !tutorial.sections.isEmpty()">
		<c:forEach var="tutorialSection" items="tutorial.sections">
			<!-- TODO: Add a way to view each section, and move them up/down the list -->
			<c:if test="tutorialSection.pages!=null && tutorialSection.pages.page!=null && 
					!tutorialSection.pages.page.isEmpty()">
				<c:forEach var="page" items="tutorialSection.pages.page">
					<h2>${page.name}</h2>
					<!-- TODO: add buttons/icons to move or delete these pages. -->
				</c:forEach>
			</c:if>
			<security:authorize access="hasRole('ROLE_ADMIN')">
				<!-- Give them the right to add pages. -->
				<a href="<c:url value='${tutorialSection.title}/newPage'/>">New Page</a>
			</security:authorize>
		</c:forEach>
	</c:if>
	<security:authorize access="hasRole('ROLE_ADMIN')">
		<!-- Give them the right to add sections. -->
		<a href="newSection">New Section</a>
	</security:authorize>
</body>
</html>