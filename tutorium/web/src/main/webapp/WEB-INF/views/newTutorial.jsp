<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create A Tutorial</title>
</head>
<body>
	<!-- Lets the user create a new tutorial -->
	<form action="resource/tutorial" method="POST">
		<label for="title">Title</label><input name="title" type="text"/><br/>
		<label for="author">Author</label><input name="author" type="text"/><br/>
		<label for="summary">Summary</label><textarea name="summary"></textarea><br/>
		<input type="submit" name="submit" value="Create"/>
	</form>
</body>
</html>