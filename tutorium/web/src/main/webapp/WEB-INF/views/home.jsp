<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page import="tutorium.data.entity.Tutorial" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home</title>
</head>
<body>
	<!-- login link -->
	<security:authorize access="isAnonymous()">
		<a href="login">Login</a><br/>
		<a href="createAccount">Create Account</a><br/>
	</security:authorize>
	<security:authorize access="isAuthenticated()">
		<a href="profile">Profile</a><br/>
		<a href="logout">Logout</a><br/>
	</security:authorize>
	<!-- List of tutorials -->
	<h1>Tutorials</h1>
	<c:if test="${tutorials.isEmpty()}">
		There are no tutorials at this time.
	</c:if>
	<c:forEach var="tutorial" items="${tutorials}">
		<h2><a href="tutorials/${tutorial.id}">${tutorial.name}</a></h2>
		<p>${tutorial.summary}</p>
	</c:forEach>
	<!-- Let them define a new tutorial. -->
	<security:authorize access="hasRole('ROLE_ADMIN')">
		<a href="newTutorial">create new tutorial</a>
	</security:authorize>
	<a href="resource/simple">test ajax</a>
	end of page
</body>
</html>