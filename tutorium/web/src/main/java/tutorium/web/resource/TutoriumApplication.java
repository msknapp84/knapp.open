package tutorium.web.resource;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.spring.scope.RequestContextFilter;

public class TutoriumApplication extends ResourceConfig {

	public TutoriumApplication() {
		register(RequestContextFilter.class);
        register(TutorialResource.class);
        register(UserResource.class);
        register(SimpleTestResource.class);
	}
}