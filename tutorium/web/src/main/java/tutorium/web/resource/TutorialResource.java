package tutorium.web.resource;

import java.net.URI;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tutorium.data.dao.TutorialDAO;
import tutorium.data.entity.Tutorial;

@Service
@Path("/")
public class TutorialResource {

	private final TutorialDAO tutorialDAO;

	@Autowired
	public TutorialResource(final TutorialDAO tutorialDAO) {
		this.tutorialDAO = tutorialDAO;
	}

	@POST
	@Path("tutorial")
	@Consumes("application/x-www-form-urlencoded")
	public Response createTutorial(@FormParam("name") String name,
			@FormParam("author") String author,
			@FormParam("summary") String summary,
			@Context ServletContext servletContext) {
		Tutorial tutorial = new Tutorial();
		tutorial.setAuthor(author);
		tutorial.setCreated(new Date());
		tutorial.setName(name);
		tutorial.setSummary(summary);
		tutorialDAO.saveTutorial(tutorial);
		URI loc = null;
		try {
			loc = UriBuilder.fromPath(servletContext.getContextPath()+"/tutorials/{tut}").build(tutorial.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.status(Status.SEE_OTHER).location(loc).build();
	}
}
