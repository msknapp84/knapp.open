package tutorium.web.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.stereotype.Service;

@Service
@Path("simple")
public class SimpleTestResource {
	
	@GET
//	@Produces("text/plain")
	public String getIt() {
		return "jersey is working";
	}
}