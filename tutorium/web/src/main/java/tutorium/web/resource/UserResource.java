package tutorium.web.resource;

import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import tutorium.data.dao.UserDAO;
import tutorium.data.entity.User;

@Service
@Path("/createAccount")
public class UserResource {

	private final UserDAO userDAO;

	private final BCryptPasswordEncoder encoder;

	@Autowired
	public UserResource(final UserDAO userDAO,
			final BCryptPasswordEncoder encoder) {
		this.userDAO = userDAO;
		this.encoder = encoder;
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createAccount(@FormParam("firstName") String firstName,
			@FormParam("lastName") String lastName,
			@FormParam("userName") String userName,
			@FormParam("email") String email,
			@FormParam("password") String password, @Context UriInfo uriInfo,
			@Context ServletContext servletContext) {
		User user = new User();
		// there is no need to generate salt here, the BCryptPasswordEncoder
		// will generate a random salt automatically. You may be wondering
		// "well how does it know what salt to use later when it's trying to
		// validate a password?" My assumption is that the encoder somehow
		// includes the salt value in it's output, after it has been encoded,
		// meaning that when you call it's 'matches' function, it is able
		// to determine what salt was used to encode the string the first time.
		// short answer, don't worry about salting this password, it's taken
		// care of.
		String encryptedPassword = encoder.encode(password);
		user.setPassword(encryptedPassword);
		user.setEnabled(true);
		user.setExpired(false);
		user.setUserName(userName);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setAuths(new HashSet<>(Arrays.asList("ROLE_USER")));
		userDAO.save(user);
		URI loc = null;
		try {
			loc = uriInfo.getBaseUriBuilder()
					.replacePath(servletContext.getContextPath())
					.path("/profile").build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.created(loc).contentLocation(loc).status(303).build();
	}
}
