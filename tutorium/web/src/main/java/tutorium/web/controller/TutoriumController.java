package tutorium.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import tutorium.data.dao.TutorialDAO;
import tutorium.data.entity.Section;
import tutorium.data.entity.Tutorial;
import tutorium.data.schema.TutorialSection;

@Controller
public class TutoriumController {
	
	private final TutorialDAO tutorialDAO;
	
	@Autowired
	public TutoriumController(TutorialDAO tutorialDAO) {
		this.tutorialDAO = tutorialDAO;
	}

	@RequestMapping(value={"/","/home","/tutorials"})
	public String getHome(Model model) {
		// the home page wants a list of tutorials.
		List<Tutorial> tutorials = tutorialDAO.getTutorialSummaries();
		model.addAttribute("tutorials", tutorials);
		return "home";
	}

	@RequestMapping("/tutorials/{tutorialId}")
	public String getTutorial(Model model,@PathVariable("tutorialId") long tutorialId) {
		// the tutorial page wants the table of contents.
		Tutorial tutorial = tutorialDAO.getTutorial(tutorialId);
		model.addAttribute("tutorial", tutorial);
		return "tutorial";
	}

	@RequestMapping("/newTutorial")
	public String newTutorial(Model model) {
		return "newTutorial";
	}

	@RequestMapping("/tutorials/{tutorial}/{section}/newPage")
	public String newPage(Model model,@PathVariable("tutorial") long id,@PathVariable("section") String sectionTitle) {
		Tutorial tutorial = tutorialDAO.getTutorial(id);
		model.addAttribute("tutorial",tutorial);
		for (Section s : tutorial.getSections()) {
			if (s.getTitle().equals(sectionTitle)) {
				model.addAttribute("section",s);
				break;
			}
		}
		return "newPage";
	}

	@RequestMapping("/tutorials/{tutorial}/newSection")
	public String newSection(Model model,@PathVariable("tutorial") long id) {
		Tutorial tutorial = tutorialDAO.getTutorial(id);
		model.addAttribute("tutorial",tutorial);
		return "newSection";
	}
}
