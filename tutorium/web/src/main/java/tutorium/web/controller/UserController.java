package tutorium.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import tutorium.data.dao.UserDAO;
import tutorium.data.entity.User;

@Controller
public class UserController {

	private final UserDAO userDAO;

	@Autowired
	public UserController(final UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	@RequestMapping("/createAccount")
	public String createAccount(Model model) {
		return "createAccount";
	}

	@RequestMapping("/login")
	public String login(Model model) {
		return "login";
	}

	@RequestMapping("/loginFailure")
	public String loginFailure() {
		return "loginFailure";
	}

	@RequestMapping("/profile")
	public String profile(Model model) {
		User user = null;
		Object principal = SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		if (principal instanceof User) {
			user = (User) principal;
		} else {
			String userName = null;
			if (principal instanceof UserDetails) {
				userName = ((UserDetails) principal).getUsername();
			} else {
				userName = principal.toString();
			}
			if (userName!=null) {
				user = userDAO.loadUserByUsername(userName);
			}
		}
		if (user != null) {
			model.addAttribute("user", user);
		} else {
			// must throw an exception
			throw new RuntimeException("Cannot find the user.");
		}
		return "profile";
	}
}
