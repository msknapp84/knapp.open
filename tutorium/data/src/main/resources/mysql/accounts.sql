create database tutorium;
# password hint: tutoriumapp
create user tutoriumApp identified by PASSWORD '*E875B4EE4824A46632596C8EF47B2EED4AAF59C5';
grant select,insert,update,delete,execute on tutorium.* to tutoriumApp@'%';
grant select,insert,update,delete,execute on tutorium.* to tutoriumApp@'localhost';

create database tutoriumTest;
# hint: unittestun
create user tutoriumTestApp identified by PASSWORD '*0888FDA2F319C8B22C11457A01E03EBAEA60B2BC';
grant select,insert,update,delete,execute,create,drop on tutoriumTest.* to tutoriumTestApp@'%';
grant select,insert,update,delete,execute,create,drop,alter on tutoriumTest.* to tutoriumTestApp@'localhost';

flush privileges;