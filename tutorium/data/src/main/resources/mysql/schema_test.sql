-- MySQL dump 10.13  Distrib 5.6.19, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: tutoriumTest
-- ------------------------------------------------------
-- Server version	5.6.19-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `tutoriumTest`
--

/*!40000 DROP DATABASE IF EXISTS `tutoriumTest`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `tutoriumTest` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `tutoriumTest`;

--
-- Table structure for table `COMMENT`
--

DROP TABLE IF EXISTS `COMMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `COMMENT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `rank` tinyint(4) DEFAULT NULL,
  `recommendAdding` tinyint(1) DEFAULT NULL,
  `recommendRetension` tinyint(1) DEFAULT NULL,
  `summary` varchar(500) DEFAULT NULL,
  `text` longtext,
  `title` varchar(120) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `paragraph_id` bigint(20) NOT NULL,
  `replyTo_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK63717A3F37A46470` (`paragraph_id`),
  KEY `FK63717A3FA683220A` (`replyTo_id`),
  CONSTRAINT `FK63717A3F37A46470` FOREIGN KEY (`paragraph_id`) REFERENCES `PARAGRAPH` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK63717A3FA683220A` FOREIGN KEY (`replyTo_id`) REFERENCES `COMMENT` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PAGE`
--

DROP TABLE IF EXISTS `PAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PAGE` (
  `id` bigint(20) NOT NULL,
  `name` varchar(120) DEFAULT NULL,
  `pages_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK255A8F362B3151` (`pages_id`),
  CONSTRAINT `FK255A8F362B3151` FOREIGN KEY (`pages_id`) REFERENCES `SECTION` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PAGE_SECTION`
--

DROP TABLE IF EXISTS `PAGE_SECTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PAGE_SECTION` (
  `id` bigint(20) NOT NULL,
  `sections_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK19300D1581307C25` (`sections_id`),
  CONSTRAINT `FK19300D1581307C25` FOREIGN KEY (`sections_id`) REFERENCES `PAGE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PARAGRAPH`
--

DROP TABLE IF EXISTS `PARAGRAPH`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PARAGRAPH` (
  `id` bigint(20) NOT NULL,
  `text` varchar(1024) DEFAULT NULL,
  `paragraphs_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1A47D94E33A3AC21` (`paragraphs_id`),
  CONSTRAINT `FK1A47D94E33A3AC21` FOREIGN KEY (`paragraphs_id`) REFERENCES `PAGE_SECTION` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SECTION`
--

DROP TABLE IF EXISTS `SECTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SECTION` (
  `id` bigint(20) NOT NULL,
  `title` varchar(120) DEFAULT NULL,
  `subSections_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKA03B0AC58FB90C67` (`subSections_id`),
  CONSTRAINT `FKA03B0AC58FB90C67` FOREIGN KEY (`subSections_id`) REFERENCES `SECTION` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TUTORIAL`
--

DROP TABLE IF EXISTS `TUTORIAL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TUTORIAL` (
  `id` bigint(20) NOT NULL,
  `author` varchar(60) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `name` varchar(120) DEFAULT NULL,
  `summary` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TUTORIAL_SECTION`
--

DROP TABLE IF EXISTS `TUTORIAL_SECTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TUTORIAL_SECTION` (
  `TUTORIAL_id` bigint(20) NOT NULL,
  `sections_id` bigint(20) NOT NULL,
  KEY `FKB4CB8EE4253432A7` (`sections_id`),
  KEY `FKB4CB8EE4CA93B4C4` (`TUTORIAL_id`),
  CONSTRAINT `FKB4CB8EE4253432A7` FOREIGN KEY (`sections_id`) REFERENCES `SECTION` (`id`),
  CONSTRAINT `FKB4CB8EE4CA93B4C4` FOREIGN KEY (`TUTORIAL_id`) REFERENCES `TUTORIAL` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-13 12:51:51
