package tutorium.data.query;

public enum CommentOrder {
	RANK,
	MOSTRECENT,
	BESTMATCH;
}
