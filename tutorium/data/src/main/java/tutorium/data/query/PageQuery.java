package tutorium.data.query;

import tutorium.data.entity.Page;
import tutorium.data.entity.PageSection;
import tutorium.data.entity.Paragraph;

public class PageQuery {
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean matches(Page p) {
		boolean found = false;
		for (PageSection ps : p.getSections()) {
			found = matches(ps);
			if (found) {
				break;
			}
		}
		return found;
	}

	private boolean matches(PageSection ps) {
		boolean found = false;
		for (Paragraph p: ps.getParagraphs()) {
			found = p.getText().contains(text);
			if (found) {
				break;
			}
		}
		return found;
	}
}