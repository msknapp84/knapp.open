package tutorium.data.query;

import tutorium.data.entity.Comment;
import tutorium.data.entity.CommentType;

public class CommentQuery {
	private Long paragraphId;
	private String text;
	private CommentType[] allowedTypes;
	private CommentOrder order;
	public Long getParagraphId() {
		return paragraphId;
	}
	public void setParagraphId(Long paragraphId) {
		this.paragraphId = paragraphId;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public CommentType[] getAllowedTypes() {
		return allowedTypes;
	}
	public void setAllowedTypes(CommentType[] allowedTypes) {
		this.allowedTypes = allowedTypes;
	}
	public CommentOrder getOrder() {
		return order;
	}
	public void setOrder(CommentOrder order) {
		this.order = order;
	}
	public boolean matches(Comment c) {
		if (paragraphId!=null && c.getParagraph().getId()!=this.paragraphId.longValue()) {
			return false;
		}
		if (allowedTypes!=null && allowedTypes.length>0) {
			boolean found = false;
			for (CommentType ct : allowedTypes) {
				if (ct == c.getType()) {
					found=true;
					break;
				}
			}
			if (!found) {
				return false;
			}
		}
		if (text!=null) {
			if (!c.getText().contains(text)) {
				return false;
			}
		}
		return true;
	}
	
}
