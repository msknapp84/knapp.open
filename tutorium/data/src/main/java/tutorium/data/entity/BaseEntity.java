package tutorium.data.entity;

public interface BaseEntity<T> {
	T getId();
	void setId(T t);
}
