package tutorium.data.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "page")
@Table(name = "PAGE")
public class Page {
	private long id;

	private String name;
	private List<PageSection> sections;

	@Column(length=120)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinColumn(nullable=true)
	public List<PageSection> getSections() {
		return sections;
	}

	public void setSections(List<PageSection> sections) {
		this.sections = sections;
	}

	@Id
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}