package tutorium.data.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

@Entity(name = "comment")
@Table(name = "COMMENT")
public class Comment {

	private long id;
	private Date created;
	private String title;
	private String summary;
	private String text;
	private byte rank;
	private Paragraph paragraph;
	private Comment replyTo;
	private boolean recommendRetension;
	private boolean recommendAdding;
	private CommentType type;

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "created",nullable=false)
	@Temporal(TemporalType.TIMESTAMP)
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Column(name = "title",length=120)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column(length=500)
	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	@Column
	@Type(type="java.sql.Clob")
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Column
	public byte getRank() {
		return rank;
	}

	public void setRank(byte rank) {
		this.rank = rank;
	}

	@Column
	public boolean isRecommendRetension() {
		return recommendRetension;
	}

	public void setRecommendRetension(boolean recommendRetension) {
		this.recommendRetension = recommendRetension;
	}

	@Column
	public boolean isRecommendAdding() {
		return recommendAdding;
	}

	public void setRecommendAdding(boolean recommendAdding) {
		this.recommendAdding = recommendAdding;
	}

	@Column(nullable=false)
	@Enumerated(EnumType.ORDINAL)
	public CommentType getType() {
		return type;
	}

	public void setType(CommentType type) {
		this.type = type;
	}

	@ManyToOne(cascade={CascadeType.REMOVE},fetch=FetchType.LAZY)
	@OnDelete(action=OnDeleteAction.CASCADE)
	@JoinColumn(nullable=false)
	public Paragraph getParagraph() {
		return paragraph;
	}

	public void setParagraph(Paragraph paragraph) {
		this.paragraph = paragraph;
	}

	@ManyToOne(cascade={CascadeType.REMOVE},fetch=FetchType.LAZY)
	@OnDelete(action=OnDeleteAction.CASCADE)
	public Comment getReplyTo() {
		return replyTo;
	}

	public void setReplyTo(Comment replyTo) {
		this.replyTo = replyTo;
	}
}