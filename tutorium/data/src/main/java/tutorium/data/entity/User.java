package tutorium.data.entity;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * This is the Tutorium user implementation.  Please note 
 * that there is no need to have a field for the salt value,
 * it is somehow contained in the password in a way that the
 * BCryptPasswordEncoder can determine that and use it 
 * in it's 'matches' function.  So do not add a 'salt' field.
 * @author mike
 *
 */
@Entity
public class User implements UserDetails {
	private static final long serialVersionUID = -6514055590068340670L;
	
	private long id;
	private String firstName;
	private String lastName;
	private String email;
	private String userName;
	private String password;
	private boolean expired;
	private boolean locked;
	private boolean enabled;
	private Set<String> auths;

	@Override
	@Transient
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> g = new HashSet<>();
		auths.forEach(p -> g.add(new SimpleGrantedAuthority(p)));
		return g;
	}

	/**
	 * This is the encrypted password as should be fed into the 
	 * BCryptPasswordEncoder.  Please note that the salt value 
	 * is somehow contained within this password, the BCryptPasswordEncoder
	 * does that automatically.  Hence there is no need to have a 
	 * separate field for the salt value.  It took me three hours
	 * to figure that out because Spring did not document this,
	 * AGAIN.  Thanks a lot Spring!
	 * <p>They also forgot to mention that the length is always 
	 * 60 characters, which is information I need to know so I 
	 * can create the correct database schema.</p>
	 */
	@Override
	@Column(length=60,nullable=false)
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return userName;
	}

	@Override
	@Transient
	public boolean isAccountNonExpired() {
		return !expired;
	}

	@Override
	@Transient
	public boolean isAccountNonLocked() {
		return !locked;
	}

	@Override
	@Transient
	public boolean isCredentialsNonExpired() {
		return !expired;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	@Column(length=24,nullable=false)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	@ElementCollection
	public Set<String> getAuths() {
		return auths;
	}

	public void setAuths(Set<String> authorities) {
		this.auths = authorities;
	}

	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(length=40)
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(length=40)
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(length=90)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}