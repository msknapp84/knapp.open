package tutorium.data.entity;

public enum CommentType {
	QUESTION,
	ANSWER,
	COMMENT,
	CRITICISM,
	DELETED,
	EDGECASE,
	SUMMARY;
}
