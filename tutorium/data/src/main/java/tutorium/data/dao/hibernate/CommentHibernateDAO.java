package tutorium.data.dao.hibernate;

import java.util.Collection;
import java.util.function.Consumer;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import tutorium.data.dao.CommentDAO;
import tutorium.data.entity.Comment;
import tutorium.data.query.CommentQuery;

@Repository("commentDAO")
@Profile("mysql")
public class CommentHibernateDAO implements CommentDAO {

	private final SessionFactory sessionFactory;
	
	@Autowired
	public CommentHibernateDAO(final SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public Comment get(long id) {
		return (Comment)sessionFactory.getCurrentSession().get(Comment.class, id);
	}

	@Transactional
	public void save(Comment comment) {
		sessionFactory.getCurrentSession().save(comment);
	}

	@Transactional
	public void delete(long id) {
		Comment p = (Comment)sessionFactory.getCurrentSession().get(Comment.class, id);
		sessionFactory.getCurrentSession().delete(p);
	}

	@Transactional
	public Collection<Comment> query(CommentQuery query) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public void with(long id, Consumer<Comment> consumer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public void with(CommentQuery query, Consumer<Comment> consumer) {
		// TODO Auto-generated method stub
		
	}

}
