package tutorium.data.dao.ram;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import tutorium.data.dao.CommentDAO;
import tutorium.data.entity.Comment;
import tutorium.data.query.CommentQuery;

@Repository("commentDAO")
@Profile("ram")
public class CommentRAMDAO implements CommentDAO {
	private long nextId = 1L;
	private final Map<Long,Comment> comments = new HashMap<>();

	@Override
	public Comment get(long id) {
		return comments.get(id);
	}

	@Override
	public void save(Comment comment) {
		if (comment.getId()<=0) {
			comment.setId(nextId++);
		}
		comments.put(comment.getId(), comment);
	}

	@Override
	public void delete(long id) {
		comments.remove(id);
	}

	@Override
	public Collection<Comment> query(CommentQuery query) {
		List<Comment> cs = new ArrayList<>();
		for (Comment c : comments.values()) {
			if (query.matches(c)) {
				cs.add(c);
			}
		}
		return cs;
	}

	@Override
	public void with(long id, Consumer<Comment> consumer) {
		consumer.accept(get(id));
	}

	@Override
	public void with(CommentQuery query, Consumer<Comment> consumer) {
		query(query).forEach(consumer);
	}

}
