package tutorium.data.dao.hibernate;


import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import tutorium.data.dao.UserDAO;
import tutorium.data.entity.User;

@Repository("userDAO")
@Profile("mysql")
public class UserHibernateDAO implements UserDAO {

	private final SessionFactory sessionFactory;
	
	@Autowired
	public UserHibernateDAO(final SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	@Transactional
	public User get(long id) {
		return (User)sessionFactory.getCurrentSession().get(User.class, id);
	}

	@Override
	@Transactional
	public User getByEmail(String email) {
		String hql = "from User where email = :email";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString("email", email);
		User user = (User) query.uniqueResult();
		return user;
	}

	@Override
	@Transactional
	public User loadUserByUsername(String userName)
			throws UsernameNotFoundException {
		String hql = "from User where userName = :userName";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setString("userName", userName);
		User user = (User) query.uniqueResult();
		return user;
	}

	@Override
	@Transactional
	public void save(User user) {
		sessionFactory.getCurrentSession().save(user);
	}
}