package tutorium.data.dao.ram;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import tutorium.data.dao.PageDAO;
import tutorium.data.entity.Page;
import tutorium.data.query.PageQuery;

@Repository("pageDAO")
@Profile("ram")
public class PageRAMDAO implements PageDAO {
	private long nextId = 1L;
	private Map<Long,Page> pages = new HashMap<>();

	@Override
	public Page get(long id) {
		return pages.get(id);
	}

	@Override
	public void save(Page page) {
		if (page.getId()<=0) {
			page.setId(nextId++);
		}
		pages.put(page.getId(), page);
	}

	@Override
	public void delete(long id) {
		pages.remove(id);
	}

	@Override
	public Collection<Page> query(PageQuery query) {
		List<Page> ps = new ArrayList<>();
		for (Page p : pages.values()) {
			if (query.matches(p)) {
				ps.add(p);
			}
		}
		return ps;
	}

	@Override
	public void with(long id, Consumer<Page> consumer) {
		consumer.accept(get(id));
	}

	@Override
	public void with(PageQuery query, Consumer<Page> consumer) {
		query(query).forEach(consumer);
	}
}