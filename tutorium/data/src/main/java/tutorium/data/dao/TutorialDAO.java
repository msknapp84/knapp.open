package tutorium.data.dao;

import java.util.List;
import java.util.function.Consumer;

import tutorium.data.entity.Tutorial;

public interface TutorialDAO {
	Tutorial getTutorial(long id);
	void saveTutorial(Tutorial tutorial);
	void deleteTutorial(long id);
	void with(long id,Consumer<Tutorial> consumer);
	List<Tutorial> getTutorialSummaries();
}