package tutorium.data.dao.hibernate;

import java.util.List;
import java.util.function.Consumer;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import tutorium.data.dao.TutorialDAO;
import tutorium.data.entity.Tutorial;

@Repository("tutorialDAO")
@Profile("mysql")
public class TutorialHibernateDAO implements TutorialDAO {

	private final SessionFactory sessionFactory;
	
	@Autowired
	public TutorialHibernateDAO(final SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public Tutorial getTutorial(long id) {
		Tutorial tutorial = (Tutorial) sessionFactory.getCurrentSession().load(Tutorial.class, id);
		return tutorial;
	}

	@Transactional
	public void saveTutorial(Tutorial tutorial) {
		sessionFactory.getCurrentSession().saveOrUpdate(tutorial);
	}

	@Transactional
	public void deleteTutorial(long id) {
		sessionFactory.getCurrentSession().delete(id);
	}

	@Override
	@Transactional
	public void with(long id, Consumer<Tutorial> consumer) {
		Tutorial tutorial = (Tutorial) sessionFactory.getCurrentSession().load(Tutorial.class,id);
		consumer.accept(tutorial);
	}

	@Override
	@Transactional
	public List<Tutorial> getTutorialSummaries() {
		Query query = sessionFactory.getCurrentSession().createQuery("from Tutorial");
		List<Tutorial> tutorials = query.list();
		return tutorials;
	}
}