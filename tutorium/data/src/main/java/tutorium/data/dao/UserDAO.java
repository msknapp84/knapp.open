package tutorium.data.dao;

import org.springframework.security.core.userdetails.UserDetailsService;

import tutorium.data.entity.User;

public interface UserDAO extends UserDetailsService {
	User get(long id);
	User getByEmail(String name);
	User loadUserByUsername(String userName);
	void save(User user);
}