package tutorium.data.dao;

import java.util.Collection;
import java.util.function.Consumer;

import tutorium.data.entity.Comment;
import tutorium.data.query.CommentQuery;

public interface CommentDAO {
	Comment get(long id);
	void save(Comment comment);
	void delete(long id);
	Collection<Comment> query(CommentQuery query);
	void with(long id,Consumer<Comment> consumer);
	void with(CommentQuery query,Consumer<Comment> consumer);
}