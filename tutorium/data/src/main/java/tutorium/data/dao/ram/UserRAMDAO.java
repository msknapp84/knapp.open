package tutorium.data.dao.ram;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import tutorium.data.dao.UserDAO;
import tutorium.data.entity.User;

@Repository("userDAO")
@Profile("ram")
public class UserRAMDAO implements UserDAO {
	private long nextId = 1L;
	private Map<Long,User> users = new HashMap<>();
	
	public UserRAMDAO() {
		if ("unittest".equals(System.getProperty("env", "unittest"))) {
			User admin = new User();
			admin.setFirstName("Tutorium");
			admin.setLastName("Administrator");
			admin.setEnabled(true);
			admin.setExpired(false);
			admin.setLocked(false);
			admin.setUserName("admin");
			// the password is admin during unit tests.
			// this is 'admin' after being encrypted by the BCryptPasswordEncrypter
			// with strength 11
			admin.setPassword("$2a$11$I91jwf8uCgHZqEhAqNPQF.keak8l92BzT6VlSMIaykxYhF.17EUya");
			admin.setAuths(new HashSet<>(Arrays.asList("ROLE_ADMIN","ROLE_USER")));
			save(admin);
		}
	}

	@Override
	public User get(long id) {
		return users.get(id);
	}

	@Override
	public User getByEmail(String name) {
		for (User user : users.values()) {
			if (user.getEmail().equals(name)) {
				return user;
			}
		}
		return null;
	}

	@Override
	public User loadUserByUsername(String userName) {
		for (User user : users.values()) {
			if (userName.equals(user.getUserName())) {
				return user;
			}
		}
		return null;
	}
	
	@Override
	public void save(User user){
		if (user.getId()<=0) {
			user.setId(nextId++);
		}
		users.put(user.getId(), user);
	}
}