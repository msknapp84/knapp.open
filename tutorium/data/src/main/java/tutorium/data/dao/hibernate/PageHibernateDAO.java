package tutorium.data.dao.hibernate;

import java.util.Collection;
import java.util.function.Consumer;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import tutorium.data.dao.PageDAO;
import tutorium.data.entity.Page;
import tutorium.data.query.PageQuery;

@Repository("pageDAO")
@Profile("mysql")
public class PageHibernateDAO implements PageDAO {

	private final SessionFactory sessionFactory;
	
	@Autowired
	public PageHibernateDAO(final SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Transactional
	public Page get(long id) {
		return (Page)sessionFactory.getCurrentSession().get(Page.class, id);
	}

	@Transactional
	public void save(Page page) {
		sessionFactory.getCurrentSession().save(page);
	}

	@Transactional
	public void delete(long id) {
		Page p = (Page)sessionFactory.getCurrentSession().get(Page.class, id);
		sessionFactory.getCurrentSession().delete(p);
	}

	@Transactional
	public Collection<Page> query(PageQuery query) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public void with(long id, Consumer<Page> consumer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional
	public void with(PageQuery query, Consumer<Page> consumer) {
		// TODO Auto-generated method stub
		
	}

}
