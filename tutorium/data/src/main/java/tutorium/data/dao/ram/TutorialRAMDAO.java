package tutorium.data.dao.ram;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import tutorium.data.dao.TutorialDAO;
import tutorium.data.entity.Tutorial;

@Repository("tutorialDAO")
@Profile("ram")
public class TutorialRAMDAO implements TutorialDAO {
	private long nextId = 1L;
	
	private Map<Long,Tutorial> tutorials = new HashMap<>();

	@Override
	public Tutorial getTutorial(long id) {
		return tutorials.get(id);
	}

	@Override
	public void saveTutorial(Tutorial tutorial) {
		if (tutorial.getId()<=0) {
			// set id.
			tutorial.setId(nextId++);
		}
		tutorials.put(tutorial.getId(),tutorial);
	}

	@Override
	public void deleteTutorial(long id) {
		tutorials.remove(id);
	}

	@Override
	public void with(long id, Consumer<Tutorial> consumer) {
		consumer.accept(getTutorial(id));
	}

	@Override
	public List<Tutorial> getTutorialSummaries() {
		return new ArrayList<>(tutorials.values());
	}

}
