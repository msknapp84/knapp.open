package tutorium.data.dao;

import java.util.Collection;
import java.util.function.Consumer;

import tutorium.data.entity.Page;
import tutorium.data.query.PageQuery;

public interface PageDAO {
	Page get(long id);
	void save(Page page);
	void delete(long id);
	Collection<Page> query(PageQuery query);
	void with(long id,Consumer<Page> consumer);
	void with(PageQuery query,Consumer<Page> consumer);
}
