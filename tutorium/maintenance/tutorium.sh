
ARG=$1

if [ "$ARG" = "deploy" ]; then
	echo "Tutorium home is $TUTORIUM_HOME"
	cp $TUTORIUM_HOME/web/target/tutorium.web.war $CATALINA_HOME/webapps
elif [ "$ARG" = "mci" ]; then
	cd $WORKSPACE/knapp.open
	mvn clean install
fi
