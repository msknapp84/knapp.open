Introduction
============

Tutorium is a free and open source website whose purpose is to help people write and improve tutorials and manuals.  
You can think of this as being half tutorial and half forum, hence the name "tutorium".  You might also think of it
as being half wikipedia and half StackOverflow.  The inspiration for this came when I noticed that there were a lot of really
bad tutorials in the programming domain, being either incomplete or confusing.  Tutorium's goal is to let the readers help
improve the tutorial they are reading.

You're probably thinking "we already have tutorials and forums, why do we need this?"  Tutorium is designed to put 
both alongside one another on the page.  Comments about each paragraph of a tutorial can be found right next to that
paragraph.

The basic use case is as follows, the user decides they want to read a tutorial about some cool new programming framework 
called Foobar.  They find a tutorium website that is about Foobar and start reading.  A few paragraphs later they read
something and are confused, it doesn't make sense, it's not clear.  The user has a question and could go post that 
question on stack overflow or some other forum but:

1. Now everybody on that forum will have to also go view that tutorial.
2. Other people reading the tutorial won't see the useful feedback from the answer to that question unless they also search 
for that information.

The goal of tutorium is to let people associate their question or comment with a specific paragraph from the tutorial. 
Also, the web page will be designed so that questions and comments will be displayed alongside the text of the 
tutorial.  So new readers will instantly see the questions and comments that previous users posted about specific 
portions of the text.

There could be many more comments about a paragraph than there is space to display them.  The web page will condense them
to the most relevant ones.  People will be able to vote on comments so the most useful ones will rise to the top.  People 
will also be able to control what kind of comments they see: questions, answers, comments, summaries, etc.

